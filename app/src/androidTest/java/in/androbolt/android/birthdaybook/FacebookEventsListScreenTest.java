package in.androbolt.android.birthdaybook;

import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import in.androbolt.android.birthdaybook.facebookeventslist.FacebookEventsListActivity;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okio.Buffer;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static in.androbolt.android.birthdaybook.matcher.CustomMatchers.*;


@RunWith(AndroidJUnit4.class)
public class FacebookEventsListScreenTest {

    private MockWebServer server;

    @Rule
    public ActivityTestRule<FacebookEventsListActivity> facebookEventsListActivityRule = new
            ActivityTestRule<>(FacebookEventsListActivity.class, true, false);

    @Before
    public void setUp() throws IOException {
        server = new MockWebServer();
        server.enqueue(new MockResponse().setBody(new Buffer().readFrom(getClass().getClassLoader()
                .getResourceAsStream("eventsforuitest.ics"))));
        server.enqueue(new MockResponse().setBody(""));
    }

    @Test
    public void shouldPopulateEventsList() throws InterruptedException {
        Context context = InstrumentationRegistry.getInstrumentation().getTargetContext();
        Intent intent = new Intent(context, FacebookEventsListActivity.class);
        intent.putExtra(FacebookEventsListActivity.EXTRAS_FBWEBCALURL,
                server.url("/ical/b.php?uid=1234&key=abc").toString());
        facebookEventsListActivityRule.launchActivity(intent);
        onView(withId(R.id.rvFacebookEventsList)).perform(actionOnItemAtPosition(1, click()));
    }

    @Test
    public void shouldDisplaySelectedCount() {
        Context context = InstrumentationRegistry.getInstrumentation().getTargetContext();
        Intent intent = new Intent(context, FacebookEventsListActivity.class);
        intent.putExtra(FacebookEventsListActivity.EXTRAS_FBWEBCALURL,
                server.url("/ical/b.php?uid=1234&key=abc").toString());
        facebookEventsListActivityRule.launchActivity(intent);
        onView(withId(R.id.tvToolbar)).check(matches(withText("20")));
        onView(withId(R.id.rvFacebookEventsList)).perform(actionOnItemAtPosition(1, click()));
        onView(withId(R.id.tvToolbar)).check(matches(withText("19")));
    }

    @Test
    public void shouldToggleSelectHaveSelectAll() {
        Context context = InstrumentationRegistry.getInstrumentation().getTargetContext();
        Intent intent = new Intent(context, FacebookEventsListActivity.class);
        intent.putExtra(FacebookEventsListActivity.EXTRAS_FBWEBCALURL,
                server.url("/ical/b.php?uid=1234&key=abc").toString());
        facebookEventsListActivityRule.launchActivity(intent);
        onView(withId(R.id.action_select))
                .check(matches(withContentDescription(R.string.select_none)));
        onView(withId(R.id.action_select))
                .check(matches(withMenuIcon(R.drawable.ic_check_box)));
    }

    @Test
    public void shouldToggleSelectHaveSelectNoneWithUnselectedItems() {
        Context context = InstrumentationRegistry.getInstrumentation().getTargetContext();
        Intent intent = new Intent(context, FacebookEventsListActivity.class);
        intent.putExtra(FacebookEventsListActivity.EXTRAS_FBWEBCALURL,
                server.url("/ical/b.php?uid=1234&key=abc").toString());
        facebookEventsListActivityRule.launchActivity(intent);
        onView(withId(R.id.rvFacebookEventsList)).perform(actionOnItemAtPosition(1, click()));
        onView(withId(R.id.action_select))
                .check(matches(withContentDescription(R.string.select_all)));
        onView(withId(R.id.action_select))
                .check(matches(withMenuIcon(R.drawable.ic_check_outline)));
    }

    @Test
    public void shouldToggleSelectHaveSelectAllWhenAllItemsSelected() {
        Context context = InstrumentationRegistry.getInstrumentation().getTargetContext();
        Intent intent = new Intent(context, FacebookEventsListActivity.class);
        intent.putExtra(FacebookEventsListActivity.EXTRAS_FBWEBCALURL,
                server.url("/ical/b.php?uid=1234&key=abc").toString());
        facebookEventsListActivityRule.launchActivity(intent);
        onView(withId(R.id.rvFacebookEventsList)).perform(actionOnItemAtPosition(1, click()));
        onView(withId(R.id.rvFacebookEventsList)).perform(actionOnItemAtPosition(1, click()));
        onView(withId(R.id.action_select))
                .check(matches(withContentDescription(R.string.select_none)));
        onView(withId(R.id.action_select))
                .check(matches(withMenuIcon(R.drawable.ic_check_box)));
    }

    @Test
    public void shouldToggleSelectUnSelectItems() {
        Context context = InstrumentationRegistry.getInstrumentation().getTargetContext();
        Intent intent = new Intent(context, FacebookEventsListActivity.class);
        intent.putExtra(FacebookEventsListActivity.EXTRAS_FBWEBCALURL,
                server.url("/ical/b.php?uid=1234&key=abc").toString());
        facebookEventsListActivityRule.launchActivity(intent);
        onView(withId(R.id.action_select)).perform(click());
        onView(withId(R.id.tvToolbar)).check(matches(withText("0")));
        onView(withId(R.id.action_select))
                .check(matches(withContentDescription(R.string.select_all)));
        onView(withId(R.id.action_select))
                .check(matches(withMenuIcon(R.drawable.ic_check_outline)));
    }

    @Test
    public void shouldToggleSelectSelectAllItemsWithSomeUnselectedItems() {
        Context context = InstrumentationRegistry.getInstrumentation().getTargetContext();
        Intent intent = new Intent(context, FacebookEventsListActivity.class);
        intent.putExtra(FacebookEventsListActivity.EXTRAS_FBWEBCALURL,
                server.url("/ical/b.php?uid=1234&key=abc").toString());
        facebookEventsListActivityRule.launchActivity(intent);
        onView(withId(R.id.rvFacebookEventsList)).perform(actionOnItemAtPosition(1, click()));
        onView(withId(R.id.action_select)).perform(click());
        onView(withId(R.id.tvToolbar)).check(matches(withText("20")));
        onView(withId(R.id.action_select))
                .check(matches(withContentDescription(R.string.select_none)));
        onView(withId(R.id.action_select))
                .check(matches(withMenuIcon(R.drawable.ic_check_box)));
    }

    @Test
    public void shouldToggleItemWithProperDrawables() {
        Context context = InstrumentationRegistry.getInstrumentation().getTargetContext();
        Intent intent = new Intent(context, FacebookEventsListActivity.class);
        intent.putExtra(FacebookEventsListActivity.EXTRAS_FBWEBCALURL,
                server.url("/ical/b.php?uid=1234&key=abc").toString());
        facebookEventsListActivityRule.launchActivity(intent);
        onView(withId(R.id.rvFacebookEventsList))
                .check(matches(atPosition(1, R.id.ivSelect,
                        withDrawable(R.drawable.ic_check_circle))));
        onView(withId(R.id.rvFacebookEventsList)).perform(actionOnItemAtPosition(1, click()));
        onView(withId(R.id.rvFacebookEventsList))
                .check(matches(atPosition(1, R.id.ivSelect,
                        withDrawable(R.drawable.ic_circle))));
    }

    @After
    public void tearDown() throws IOException {
        server.shutdown();
    }
}
