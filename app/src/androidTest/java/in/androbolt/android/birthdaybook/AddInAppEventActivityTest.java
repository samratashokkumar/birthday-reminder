package in.androbolt.android.birthdaybook;

import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.contrib.PickerActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;

import org.hamcrest.Matcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import in.androbolt.android.birthdaybook.common.datepicker.DatePicker;
import in.androbolt.android.birthdaybook.events.db.AppDBEventsHelper;
import in.androbolt.android.birthdaybook.inappevent.AddInAppEventActivity;

import static android.support.test.espresso.Espresso.*;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static android.support.test.espresso.action.ViewActions.*;
import static org.hamcrest.Matchers.allOf;
import static org.junit.Assert.assertEquals;

/**
 * Add In App Event Activity Test
 */

@RunWith(AndroidJUnit4.class)
public class AddInAppEventActivityTest {

    @Rule
    public ActivityTestRule<AddInAppEventActivity> activityTestRule = new ActivityTestRule<>
            (AddInAppEventActivity.class);

    @Test
    public void shouldAddEvent() {
        AppDBEventsHelper appDBEventsHelper = new AppDBEventsHelper(InstrumentationRegistry.getInstrumentation()
                .getTargetContext());
        int existingCount = appDBEventsHelper.getEvents().size();
        onView(withId(R.id.tietName)).perform(typeText("Test Name"));
        onView(withId(R.id.rbAnniversary)).perform(click());
        onView(withId(R.id.yearToggle)).perform(click());
        onView(withId(R.id.dpEventDate)).perform(updateDate(1990, 10, 30));
        onView(withId(R.id.action_done)).perform(click());
        int newCount = appDBEventsHelper.getEvents().size();
        assertEquals(existingCount + 1, newCount);
    }

    private ViewAction updateDate(final int year, final int month, final int date) {
        return new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return allOf(isAssignableFrom(DatePicker.class), isDisplayed());
            }

            @Override
            public String getDescription() {
                return "Set date";
            }

            @Override
            public void perform(UiController uiController, View view) {
                DatePicker datePicker = (DatePicker) view;
                datePicker.updateDate(year, month, date);
            }
        };
    }
}
