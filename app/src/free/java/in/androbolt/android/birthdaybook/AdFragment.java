package in.androbolt.android.birthdaybook;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

/**
 * Created by ashok on 14/1/15.
 */
public class AdFragment extends Fragment {

    private AdView mAdView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_ad, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mAdView = (AdView) getView().findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice("6A59EA23873F30012120963ED86AA96E").build();
        mAdView.loadAd(adRequest);
    }

    @Override
    public void onPause() {
        if(mAdView!=null)
            mAdView.pause();
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(mAdView!=null)
            mAdView.resume();
    }

    @Override
    public void onDestroy() {
        if(mAdView!=null)
            mAdView.destroy();
        super.onDestroy();
    }
}
