package in.androbolt.android.birthdaybook.test.adapters;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import in.androbolt.android.birthdaybook.adapters.NameFilter;
import in.androbolt.android.birthdaybook.adapters.RecyclerViewListAdapter;
import in.androbolt.android.birthdaybook.events.models.CustomEvent;
import in.androbolt.android.birthdaybook.events.models.Event;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
public class NameFilterTests {

    @Mock
    private RecyclerViewListAdapter recyclerViewListAdapter;

    private NameFilter nameFilter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(recyclerViewListAdapter.getItems()).thenReturn(new ArrayList<Event>(
                Arrays.asList(
                        new CustomEvent("Ashok Kumar M", "", 1),
                        new CustomEvent("Vignesh R", "", 1),
                        new CustomEvent("Tom Cruise", "", 1),
                        new CustomEvent("Leonardo Decaprio", "", 1),
                        new CustomEvent("Arun Kumar P", "", 1),
                        new CustomEvent("Sathyaraj.V", "", 1),
                        new CustomEvent("Anand.S", "", 1)
                )));
        nameFilter = new NameFilter(recyclerViewListAdapter);
    }

    @Test
    public void shouldGiveResults() {
        nameFilter.filter("kum");
        ArgumentCaptor<List<Event>> captor = ArgumentCaptor.forClass
                (List.class);
        verify(recyclerViewListAdapter).setItems(captor.capture());
        assertEquals(2, captor.getValue().size());
    }

    @Test
    public void shouldGiveEmptyResult() {
        nameFilter.filter("cap");
        ArgumentCaptor<List<Event>> captor = ArgumentCaptor.forClass
                (List.class);
        verify(recyclerViewListAdapter).setItems(captor.capture());
        assertEquals(0, captor.getValue().size());
    }

    @Test
    public void shouldEscapeRegEx() {
        nameFilter.filter(".*");
        ArgumentCaptor<List<Event>> captor = ArgumentCaptor.forClass
                (List.class);
        verify(recyclerViewListAdapter).setItems(captor.capture());
        assertEquals(0, captor.getValue().size());
    }

    @Test
    public void shouldFilterMultipleInputs() {
        ArgumentCaptor<List<Event>> captor = ArgumentCaptor.forClass
                (List.class);
        nameFilter.filter("Kumar");
        verify(recyclerViewListAdapter).setItems(captor.capture());
        assertEquals(2, captor.getValue().size());
        nameFilter.filter("Kumar M");
        verify(recyclerViewListAdapter, times(2)).setItems(captor.capture());
        assertEquals(1, captor.getValue().size());
    }
}
