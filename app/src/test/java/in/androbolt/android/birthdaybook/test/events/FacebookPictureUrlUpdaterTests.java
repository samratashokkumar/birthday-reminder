package in.androbolt.android.birthdaybook.test.events;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import in.androbolt.android.birthdaybook.common.models.Result;
import in.androbolt.android.birthdaybook.events.FacebookPictureUrlUpdater;
import in.androbolt.android.birthdaybook.events.models.Event;
import in.androbolt.android.birthdaybook.events.models.FacebookEvent;
import okhttp3.HttpUrl;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;

import static org.junit.Assert.assertEquals;

/**
 * Facebook Picture Updater Tests
 */
@RunWith(RobolectricTestRunner.class)
@Config(sdk = 23)
public class FacebookPictureUrlUpdaterTests {

    private MockWebServer server;

    @Before
    public void setUp() throws IOException {
        server = new MockWebServer();
        server.start();
    }

    @Test
    public void shouldUpdateInTwoBatches() throws IOException, InterruptedException {
        List<FacebookEvent> facebookEvents = new ArrayList<>();
        for (int i = 1; i <= 56; i++) {
            facebookEvents.add(new FacebookEvent("", "", Event.TYPE_BIRTHDAY, String.valueOf(i),
                    null));
        }
        int count = facebookEvents.size();
        buildResponse(count);

        FacebookPictureUrlUpdater facebookPictureUrlUpdater =
                new FacebookPictureUrlUpdater(server.url("/v2.5/picture").toString());
        Result<Void> result = facebookPictureUrlUpdater.updatePictureUrl(facebookEvents);
        assertEquals(Result.Type.SUCCESS, result.getType());
        assertEquals(2, server.getRequestCount());
        for (FacebookEvent facebookEvent: facebookEvents) {
            assertEquals(String.format("pic_url_%s", facebookEvent.getNativeId()),
                    facebookEvent.getPicturePath());
        }
    }

    @Test
    public void shouldUpdateInSingleBatch() throws IOException {
        List<FacebookEvent> facebookEvents = new ArrayList<>();
        for (int i = 1; i <= 50; i++) {
            facebookEvents.add(new FacebookEvent("", "", Event.TYPE_BIRTHDAY, String.valueOf(i),
                    null));
        }
        int count = facebookEvents.size();
        buildResponse(count);
        FacebookPictureUrlUpdater facebookPictureUrlUpdater =
                new FacebookPictureUrlUpdater(server.url("/v2.5/picture").toString());
        Result<Void> result = facebookPictureUrlUpdater.updatePictureUrl(facebookEvents);
        assertEquals(Result.Type.SUCCESS, result.getType());
        assertEquals(1, server.getRequestCount());
        for (FacebookEvent facebookEvent: facebookEvents) {
            assertEquals(String.format("pic_url_%s", facebookEvent.getNativeId()),
                    facebookEvent.getPicturePath());
        }
    }

    @Test
    public void shouldUpdateInSingleBatchLessThan50() throws IOException, InterruptedException {
        List<FacebookEvent> facebookEvents = new ArrayList<>();
        for (int i = 1; i <= 5; i++) {
            facebookEvents.add(new FacebookEvent("", "", Event.TYPE_BIRTHDAY, String.valueOf(i),
                    null));
        }
        int count = facebookEvents.size();
        buildResponse(count);
        FacebookPictureUrlUpdater facebookPictureUrlUpdater =
                new FacebookPictureUrlUpdater(server.url("/v2.5/picture").toString());
        Result<Void> result = facebookPictureUrlUpdater.updatePictureUrl(facebookEvents);
        assertEquals(Result.Type.SUCCESS, result.getType());
        assertEquals(1, server.getRequestCount());
        for (FacebookEvent facebookEvent: facebookEvents) {
            assertEquals(String.format("pic_url_%s", facebookEvent.getNativeId()),
                    facebookEvent.getPicturePath());
        }
    }

    private void buildResponse(int idCount) {
        StringBuilder response = new StringBuilder("{");
        for (int i = 1; i <= idCount; i++) {
            response.append(String.format("\"%d\":{\"data\":{\"is_silhouette\":false," +
                    "\"url\":\"pic_url_%d\"}},", i, i));
            if (i % 50 == 0) {
                response.deleteCharAt(response.lastIndexOf(",")).append("}");
                server.enqueue(new MockResponse().setBody(response.toString()));
                if (i < idCount) {
                    response = new StringBuilder("{");
                }
            }
        }
        if (!response.toString().endsWith("}")) {
            response.deleteCharAt(response.lastIndexOf(",")).append("}");
            server.enqueue(new MockResponse().setBody(response.toString()));
        }
    }

    @Test
    public void shouldRetryAfterDeletingUnavailableIds() throws IOException {
        String[] expectedUrls = new String[] {"pic_url_1", "pic_url_2", null, null};
        List<FacebookEvent> facebookEvents = new ArrayList<>();
        for (int i = 1; i <= 4; i++) {
            facebookEvents.add(new FacebookEvent("", "", Event.TYPE_BIRTHDAY, String.valueOf(i),
                    null));
        }
        server.enqueue(new MockResponse().setResponseCode(404).setBody
                ("{\"error\":{\"message\":\"(#803) Some of the " +
                "aliases you requested do not exist: 3,4\"," +
                "\"type\":\"OAuthException\",\"code\":803,\"fbtrace_id\":\"Eye16Jn7ey1\"}}"));
        server.enqueue(new MockResponse().setBody("{\"1\":{\"data\":{\"is_silhouette\":false," +
                "\"url\":\"pic_url_1\"}},\"2\":{\"data\":{\"is_silhouette\":false," +
                "\"url\":\"pic_url_2\"}}}"));
        FacebookPictureUrlUpdater facebookPictureUrlUpdater =
                new FacebookPictureUrlUpdater(server.url("/v2.5/picture").toString());
        Result<Void> result = facebookPictureUrlUpdater.updatePictureUrl(facebookEvents);
        assertEquals(Result.Type.SUCCESS, result.getType());
        for (int i = 0; i < 4; i++) {
            assertEquals(expectedUrls[i], facebookEvents.get(i).getPicturePath());
        }
    }

    @After
    public void tearDown() throws IOException {
        server.shutdown();
    }
}
