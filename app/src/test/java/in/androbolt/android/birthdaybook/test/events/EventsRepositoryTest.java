package in.androbolt.android.birthdaybook.test.events;

import android.net.Uri;
import android.provider.ContactsContract;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;

import java.util.ArrayList;
import java.util.Arrays;

import in.androbolt.android.birthdaybook.events.EventsRepository;
import in.androbolt.android.birthdaybook.events.contacts.ContactEventsHelper;
import in.androbolt.android.birthdaybook.events.db.AppDBEventsHelper;
import in.androbolt.android.birthdaybook.events.models.ContactEvent;
import in.androbolt.android.birthdaybook.events.models.CustomEvent;
import in.androbolt.android.birthdaybook.events.models.Event;
import in.androbolt.android.birthdaybook.events.models.FacebookEvent;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
public class EventsRepositoryTest {

    private EventsRepository eventsRepository;

    @Mock
    private ContactEventsHelper contactEventsHelper;
    @Mock
    private AppDBEventsHelper appDBEventsHelper;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        eventsRepository = new EventsRepository(contactEventsHelper, appDBEventsHelper);
    }

    @Test
    public void shouldAddEvent() {

        ContactEvent contactEvent = new ContactEvent(1, 1, 1, "");
        CustomEvent customEvent = new CustomEvent("", "", 1);
        when(contactEventsHelper.addEvent(contactEvent)).thenReturn(Uri.withAppendedPath
                (ContactsContract.Data.CONTENT_URI, "1"));
        when(appDBEventsHelper.addEvent(customEvent)).thenReturn(2L);
        assertEquals(1, eventsRepository.addEvent(contactEvent));
        assertEquals(2, eventsRepository.addEvent(customEvent));
    }

    @Test
    public void shouldGetAllEvents() {
        when(contactEventsHelper.getEvents()).thenReturn(new ArrayList<>(Arrays.<Event>asList(new
                ContactEvent(1, 1, 1, "Test Name", "1990-01-01", Event.TYPE_BIRTHDAY), new
                ContactEvent
                (2, 2, 2, "Test Name 1", "2010-01-05", Event.TYPE_ANNIVERSARY))));
        when(appDBEventsHelper.getEvents()).thenReturn(new ArrayList<>(Arrays.<Event>asList(new
                CustomEvent(1, 2, "Custom Name", "1978-09-09", Event.TYPE_ANNIVERSARY))));
        assertEquals(3, eventsRepository.getEvents().size());
    }

    @Test
    public void shouldGetSpecificSourceEvents() {
        when(contactEventsHelper.getEvents()).thenReturn(new ArrayList<>(Arrays.<Event>asList(new
                ContactEvent(1, 1, 1, "Test Name", "1990-01-01", Event.TYPE_BIRTHDAY), new
                ContactEvent(2, 2, 2, "Test Name 1", "2010-01-05", Event.TYPE_ANNIVERSARY))));
        when(appDBEventsHelper.getEvents(CustomEvent.SOURCE)).thenReturn(new ArrayList<>(Arrays
                .<Event>asList(new CustomEvent(1, 2, "Custom Name", "1978-09-09", Event
                        .TYPE_ANNIVERSARY))));
        when(appDBEventsHelper.getEvents(FacebookEvent.SOURCE)).thenReturn(new ArrayList<>
                (Arrays.<Event>asList(new FacebookEvent("Test Name 1", "1990-01-01", Event
                        .TYPE_BIRTHDAY, "112233", ""))));
        assertEquals(2, eventsRepository.getEvents(ContactEvent.SOURCE).size());
        assertEquals(1, eventsRepository.getEvents(CustomEvent.SOURCE).size());
        assertEquals(1, eventsRepository.getEvents(FacebookEvent.SOURCE).size());
    }

    @Test
    public void shouldGetEventDateNames() {
        when(contactEventsHelper.getNames(Event.TYPE_BIRTHDAY, "2016-01-01")).thenReturn(new
                ArrayList<>(Arrays.asList("1", "2")));
        when(appDBEventsHelper.getNames(Event.TYPE_BIRTHDAY, "2016-01-01")).thenReturn(new
                ArrayList<>(Arrays.asList("3", "4")));
        assertEquals(4, eventsRepository.getEventDateNames(Event.TYPE_BIRTHDAY, "2016-01-01")
                .size());
    }

    @Test
    public void shouldGetEventsForAPerson() {
        when(contactEventsHelper.getEvents(1)).thenReturn(Arrays.<Event>asList(new ContactEvent(1, 1, 1,
                "Test Name", "1990-01-01", Event.TYPE_BIRTHDAY), new ContactEvent(1, 1, 1, "Test " +
                "Name",
                "2010-01-05", Event.TYPE_ANNIVERSARY)));
        when(appDBEventsHelper.getEvents(2)).thenReturn(Arrays.<Event>asList(new CustomEvent(1, 2,
                "Custom Name", "1978-09-09", Event.TYPE_ANNIVERSARY)));
        assertEquals(2, eventsRepository.getEvents(1, ContactEvent.SOURCE).size());
        assertEquals(1, eventsRepository.getEvents(2, CustomEvent.SOURCE).size());
    }

    @Test
    public void shouldUpdateEvent() {
        ContactEvent contactEvent = new ContactEvent(1, 1, 1, "", "", 1);
        eventsRepository.update(contactEvent);
        verify(contactEventsHelper, times(1)).updateEventDate(contactEvent);
        CustomEvent customEvent = new CustomEvent(1, 1, "", "", 1);
        eventsRepository.update(customEvent);
        verify(appDBEventsHelper, times(1)).updateEvent(customEvent);
    }

    @Test
    public void shouldDeleteEvent() {
        when(contactEventsHelper.delete(1)).thenReturn(1);
        when(appDBEventsHelper.deleteEvent(2)).thenReturn(1);
        assertEquals(1, eventsRepository.delete(1, ContactEvent.SOURCE));
        assertEquals(1, eventsRepository.delete(2, CustomEvent.SOURCE));
    }
}
