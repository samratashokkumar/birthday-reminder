package in.androbolt.android.birthdaybook.test.events.contacts;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.BaseColumns;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;

/**
 * Mock Contact Content Provider
 */
public class MockContactContentProvider extends ContentProvider {

    Context context;
    SQLiteDatabase db;

    public MockContactContentProvider(Context context) {
        this.context = context;
    }

    @Override
    public boolean onCreate() {
        MockContactsSqliteHelper mockContactsSqliteHelper = new MockContactsSqliteHelper(context);
        db = mockContactsSqliteHelper.getWritableDatabase();
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri,
                        String[] projection,
                        String selection,
                        String[] selectionArgs,
                        String sortOrder) {
        if (ContactsContract.Data.CONTENT_URI.getAuthority().equals(uri.getAuthority())) {
            if (ContactsContract.Data.CONTENT_URI
                    .getLastPathSegment().equals(uri.getLastPathSegment())) {
                return
                        db.query("view_data",
                                projection, selection, selectionArgs, null, null, sortOrder);
            } else if (ContactsContract.Contacts.CONTENT_URI
                    .getLastPathSegment().equals(uri.getLastPathSegment())) {
                return
                        db.query("view_contacts",
                                projection, selection, selectionArgs, null, null, sortOrder);
            } else if (ContactsContract.RawContacts.CONTENT_URI
                    .getLastPathSegment().equals(uri.getLastPathSegment())){
                return
                        db.query("raw_contacts",
                                projection, selection, selectionArgs, null, null, sortOrder);
            }
        }
        return null;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        if (ContactsContract.Data.CONTENT_URI.getAuthority().equals(uri.getAuthority())) {
            if (ContactsContract.Contacts.CONTENT_URI
                    .getLastPathSegment().equals(uri.getLastPathSegment())) {
                long contactId = db.insert("contacts", null, values);
                ContentValues cvRawContacts = new ContentValues();
                cvRawContacts.put(ContactsContract.RawContacts.CONTACT_ID, contactId);
                long rawContactId = db.insert("raw_contacts", null, cvRawContacts);
                ContentValues cvVisibleContacts = new ContentValues();
                cvVisibleContacts.put(BaseColumns._ID, contactId);
                ContentValues cvContacts = new ContentValues();
                cvContacts.put(ContactsContract.Contacts.NAME_RAW_CONTACT_ID, rawContactId);
                db.update("contacts", cvContacts,
                        ContactsContract.Contacts._ID + "=?",
                        new String[] {String.valueOf(contactId)});
                db.insert("visible_contacts", null, cvVisibleContacts);
                return uri.withAppendedPath(uri, String.valueOf(contactId));
            } else if(ContactsContract.Data.CONTENT_URI
                    .getLastPathSegment().equals(uri.getLastPathSegment())) {
                long id = db.insert("data", null, values);
                String mimiType = values.getAsString(ContactsContract.Data.MIMETYPE);
                if (mimiType.equals(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)) {
                    ContentValues cvData = new ContentValues();
                    cvData.put(ContactsContract.Contacts.HAS_PHONE_NUMBER, 1);
                    db.update("contacts",
                            cvData,
                            ContactsContract.Contacts.NAME_RAW_CONTACT_ID + "=?",
                            new String[] {
                                    values.getAsString(ContactsContract.Data.RAW_CONTACT_ID)
                            });
                }
                return uri.withAppendedPath(uri, String.valueOf(id));
            }
        }
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        if (ContactsContract.Data.CONTENT_URI.getAuthority().equals(uri.getAuthority())) {
            if (ContactsContract.Data.CONTENT_URI
                    .getLastPathSegment().equals(uri.getLastPathSegment())) {
                return
                        db.delete("data", selection, selectionArgs);
            }
        }
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        if (ContactsContract.Data.CONTENT_URI.getAuthority().equals(uri.getAuthority())) {
            if (ContactsContract.Data.CONTENT_URI
                    .getLastPathSegment().equals(uri.getLastPathSegment())) {
                return db.update("data", values, selection, selectionArgs);
            }
        }
        return 0;
    }
}
