package in.androbolt.android.birthdaybook.test.notifications;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.net.Uri;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowApplication;
import org.robolectric.shadows.ShadowNotification;
import org.robolectric.shadows.ShadowNotificationManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import in.androbolt.android.birthdaybook.BuildConfig;
import in.androbolt.android.birthdaybook.events.EventsRepository;
import in.androbolt.android.birthdaybook.events.models.Event;
import in.androbolt.android.birthdaybook.notifications.EventsNotifier;
import in.androbolt.android.birthdaybook.notifications.FutureEventsNotificationReceiver;
import in.androbolt.android.birthdaybook.notifications.TodaysEventsNotificationReceiver;
import in.androbolt.android.birthdaybook.preferences.AppPreferences;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * Created by ashok on 21/2/16.
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 23)
public class NotificationTest {
    private Context context;
    private ShadowApplication application;

    @Mock
    AppPreferences appPreferences;

    @Mock
    EventsRepository eventsRepository;

    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);
        application = ShadowApplication.getInstance();
        context = RuntimeEnvironment.application;
    }

    @Test
    public void shouldHaveNotificationReceiverRegistered() {
        List<ShadowApplication.Wrapper> registeredReceivers =  this.application
                .getRegisteredReceivers();
        boolean isTodaysEventsRegistered = false;
        boolean isFutureEventsRegistered = false;
        for (ShadowApplication.Wrapper wrapper: registeredReceivers) {
            if (wrapper.getBroadcastReceiver().getClass().getName().equals
                    (TodaysEventsNotificationReceiver.class.getName())) {
                isTodaysEventsRegistered = true;
            }
            if (wrapper.getBroadcastReceiver().getClass().getName().equals
                    (FutureEventsNotificationReceiver.class.getName())) {
                isFutureEventsRegistered = true;
            }
        }
        assertTrue(isTodaysEventsRegistered);
        assertTrue(isFutureEventsRegistered);
    }

    @Test
    public void todaysBirthdaysLessThanTwoNames() {
        String eventDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance()
                .getTime());
        when(eventsRepository.getEventDateNames(Event.TYPE_BIRTHDAY, eventDate)).thenReturn
                (Arrays.asList("B", "A"));
        when(appPreferences.isStickyNotificationEnabled()).thenReturn(true);
        when(appPreferences.isNotificationSoundEnabled()).thenReturn(true);
        when(appPreferences.getNotificationTone()).thenReturn("");
        NotificationManager notificationManager = (NotificationManager) context.getSystemService
                (Context.NOTIFICATION_SERVICE);
        new EventsNotifier(context, appPreferences, eventsRepository)
                .showTodaysEventsNotification();
        ShadowNotificationManager shadowNotificationManager = Shadows.shadowOf(notificationManager);
        assertEquals(1, shadowNotificationManager.size());
        Notification notification = shadowNotificationManager.getNotification(11);
        ShadowNotification shadowNotification = Shadows.shadowOf(notification);
        assertEquals("A, B", shadowNotification.getContentText());
        assertEquals("Today’s Birthdays", shadowNotification.getContentTitle());
    }

    @Test
    public void todaysBirthdaysMoreThanTwoNames() {
        String eventDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance()
                .getTime());
        when(eventsRepository.getEventDateNames(Event.TYPE_BIRTHDAY, eventDate)).thenReturn
                (Arrays.asList("F", "D", "B", "C"));
        when(appPreferences.isStickyNotificationEnabled()).thenReturn(true);
        when(appPreferences.isNotificationSoundEnabled()).thenReturn(true);
        when(appPreferences.getNotificationTone()).thenReturn("");
        NotificationManager notificationManager = (NotificationManager) context.getSystemService
                (Context.NOTIFICATION_SERVICE);
        new EventsNotifier(context, appPreferences, eventsRepository)
                .showTodaysEventsNotification();
        ShadowNotificationManager shadowNotificationManager = Shadows.shadowOf(notificationManager);
        assertEquals(1, shadowNotificationManager.size());
        Notification notification = shadowNotificationManager.getNotification(11);
        ShadowNotification shadowNotification = Shadows.shadowOf(notification);
        assertEquals("B, C and 2 more", shadowNotification.getContentText());
    }

    @Test
    public void noEvents_shouldNotShowNotification() {
        String eventDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance()
                .getTime());
        when(eventsRepository.getEventDateNames(Event.TYPE_BIRTHDAY, eventDate)).thenReturn
                (new ArrayList<String>());
        NotificationManager notificationManager = (NotificationManager) context.getSystemService
                (Context.NOTIFICATION_SERVICE);
        new EventsNotifier(context, appPreferences, eventsRepository)
                .showTodaysEventsNotification();
        ShadowNotificationManager shadowNotificationManager = Shadows.shadowOf(notificationManager);
        assertEquals(0, shadowNotificationManager.size());
    }

    @Test
    public void shouldShowBirthdaysAndAnniversaries() {
        String eventDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance()
                .getTime());
        when(eventsRepository.getEventDateNames(Event.TYPE_BIRTHDAY, eventDate)).thenReturn
                (Arrays.asList("F", "D", "B", "C"));
        when(eventsRepository.getEventDateNames(Event.TYPE_ANNIVERSARY, eventDate)).thenReturn
                (Arrays.asList("I", "K", "J"));
        when(appPreferences.isStickyNotificationEnabled()).thenReturn(true);
        when(appPreferences.isNotificationSoundEnabled()).thenReturn(true);
        when(appPreferences.getNotificationTone()).thenReturn("");
        NotificationManager notificationManager = (NotificationManager) context.getSystemService
                (Context.NOTIFICATION_SERVICE);
        new EventsNotifier(context, appPreferences, eventsRepository)
                .showTodaysEventsNotification();
        ShadowNotificationManager shadowNotificationManager = Shadows.shadowOf(notificationManager);
        assertEquals(2, shadowNotificationManager.size());
        Notification birthdayNotification = shadowNotificationManager.getNotification(11);
        ShadowNotification shadowBirthdayNotification = Shadows.shadowOf(birthdayNotification);
        assertEquals("B, C and 2 more", shadowBirthdayNotification.getContentText());
        assertEquals("Today’s Birthdays", shadowBirthdayNotification.getContentTitle());
        Notification anniversaryNotification = shadowNotificationManager.getNotification(12);
        ShadowNotification shadowAnniversaryNotification = Shadows.shadowOf
                (anniversaryNotification);
        assertEquals("I, J and 1 more", shadowAnniversaryNotification.getContentText());
        assertEquals("Today’s Anniversaries", shadowAnniversaryNotification.getContentTitle());
    }

    @Test
    public void shouldShowOnlyAnniversary() {
        String eventDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance()
                .getTime());
        when(eventsRepository.getEventDateNames(Event.TYPE_ANNIVERSARY, eventDate)).thenReturn
                (Arrays.asList("I", "K", "J"));
        when(appPreferences.isStickyNotificationEnabled()).thenReturn(true);
        when(appPreferences.isNotificationSoundEnabled()).thenReturn(true);
        when(appPreferences.getNotificationTone()).thenReturn("");
        NotificationManager notificationManager = (NotificationManager) context.getSystemService
                (Context.NOTIFICATION_SERVICE);
        new EventsNotifier(context, appPreferences, eventsRepository)
                .showTodaysEventsNotification();
        ShadowNotificationManager shadowNotificationManager = Shadows.shadowOf(notificationManager);
        assertEquals(1, shadowNotificationManager.size());
        Notification anniversaryNotification = shadowNotificationManager.getNotification(12);
        ShadowNotification shadowAnniversaryNotification = Shadows.shadowOf
                (anniversaryNotification);
        assertEquals("I, J and 1 more", shadowAnniversaryNotification.getContentText());
        assertEquals("Today’s Anniversaries", shadowAnniversaryNotification.getContentTitle());
    }

    @Test
    public void shouldHaveNotificationSoundEnabled() {
        String eventDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance()
                .getTime());
        when(eventsRepository.getEventDateNames(Event.TYPE_BIRTHDAY, eventDate)).thenReturn
                (Arrays.asList("B", "A"));
        when(appPreferences.isStickyNotificationEnabled()).thenReturn(true);
        when(appPreferences.isNotificationSoundEnabled()).thenReturn(true);
        when(appPreferences.getNotificationTone()).thenReturn("file://test");
        NotificationManager notificationManager = (NotificationManager) context.getSystemService
                (Context.NOTIFICATION_SERVICE);
        new EventsNotifier(context, appPreferences, eventsRepository)
                .showTodaysEventsNotification();
        ShadowNotificationManager shadowNotificationManager = Shadows.shadowOf(notificationManager);
        Notification notification = shadowNotificationManager.getNotification(11);
        assertEquals(Uri.parse("file://test"), notification.sound);
    }

    @Test
    public void shouldShowTomorrowsEvents() {
        int notifyBefore = 1;
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, notifyBefore);
        String eventDate = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
        when(eventsRepository.getEventDateNames(Event.TYPE_BIRTHDAY, eventDate)).thenReturn
                (Arrays.asList("B", "A"));
        when(eventsRepository.getEventDateNames(Event.TYPE_ANNIVERSARY, eventDate)).thenReturn
                (Arrays.asList("I", "K", "J"));
        when(appPreferences.getNotifyBefore()).thenReturn(notifyBefore);
        when(appPreferences.isStickyNotificationEnabled()).thenReturn(true);
        when(appPreferences.isNotificationSoundEnabled()).thenReturn(true);
        when(appPreferences.getNotificationTone()).thenReturn("");
        NotificationManager notificationManager = (NotificationManager) context.getSystemService
                (Context.NOTIFICATION_SERVICE);
        new EventsNotifier(context, appPreferences, eventsRepository)
                .showFutureEventsNotification();
        ShadowNotificationManager shadowNotificationManager = Shadows.shadowOf(notificationManager);
        assertEquals(2, shadowNotificationManager.size());
        Notification birthdayNotification = shadowNotificationManager.getNotification(13);
        ShadowNotification shadowBirthdayNotification = Shadows.shadowOf(birthdayNotification);
        assertEquals("A, B", shadowBirthdayNotification.getContentText());
        assertEquals("Tomorrow’s Birthdays", shadowBirthdayNotification.getContentTitle());
        Notification anniversaryNotification = shadowNotificationManager.getNotification(14);
        ShadowNotification shadowAnniversaryNotification = Shadows.shadowOf
                (anniversaryNotification);
        assertEquals("I, J and 1 more", shadowAnniversaryNotification.getContentText());
        assertEquals("Tomorrow’s Anniversaries", shadowAnniversaryNotification.getContentTitle());
    }

    @Test
    public void shouldShowFutureEvents() {
        int notifyBefore = 2;
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, notifyBefore);
        String eventDate = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
        when(eventsRepository.getEventDateNames(Event.TYPE_BIRTHDAY, eventDate)).thenReturn
                (Arrays.asList("B", "A"));
        when(eventsRepository.getEventDateNames(Event.TYPE_ANNIVERSARY, eventDate)).thenReturn
                (Arrays.asList("I", "K", "J"));
        when(appPreferences.getNotifyBefore()).thenReturn(notifyBefore);
        when(appPreferences.isStickyNotificationEnabled()).thenReturn(true);
        when(appPreferences.isNotificationSoundEnabled()).thenReturn(true);
        when(appPreferences.getNotificationTone()).thenReturn("");
        NotificationManager notificationManager = (NotificationManager) context.getSystemService
                (Context.NOTIFICATION_SERVICE);
        new EventsNotifier(context, appPreferences, eventsRepository)
                .showFutureEventsNotification();
        ShadowNotificationManager shadowNotificationManager = Shadows.shadowOf(notificationManager);
        assertEquals(2, shadowNotificationManager.size());
        Notification birthdayNotification = shadowNotificationManager.getNotification(13);
        ShadowNotification shadowBirthdayNotification = Shadows.shadowOf(birthdayNotification);
        assertEquals("A, B", shadowBirthdayNotification.getContentText());
        assertEquals("Birthdays in two days", shadowBirthdayNotification.getContentTitle());
        Notification anniversaryNotification = shadowNotificationManager.getNotification(14);
        ShadowNotification shadowAnniversaryNotification = Shadows.shadowOf
                (anniversaryNotification);
        assertEquals("I, J and 1 more", shadowAnniversaryNotification.getContentText());
        assertEquals("Anniversaries in two days", shadowAnniversaryNotification.getContentTitle());
    }
}
