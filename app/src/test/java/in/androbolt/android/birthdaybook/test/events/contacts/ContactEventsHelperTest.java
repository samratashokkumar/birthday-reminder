package in.androbolt.android.birthdaybook.test.events.contacts;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.provider.ContactsContract;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.shadows.ShadowContentResolver;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import in.androbolt.android.birthdaybook.events.contacts.ContactEventsHelper;
import in.androbolt.android.birthdaybook.events.models.ContactEvent;
import in.androbolt.android.birthdaybook.events.models.Event;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;


/**
 * Tests For Contact Events
 */
@RunWith(RobolectricTestRunner.class)
public class ContactEventsHelperTest {

    private ContactEventsHelper contactEventsHelper;

    @Before
    public void setUp() {
        MockContactContentProvider mockContactContentProvider =
                new MockContactContentProvider(RuntimeEnvironment.application);
        mockContactContentProvider.onCreate();
        ShadowContentResolver.registerProvider(ContactsContract.Data.CONTENT_URI.getAuthority(),
                mockContactContentProvider);
        ContentResolver contentResolver = RuntimeEnvironment.application
                .getContentResolver();
        for (int i = 0; i < 10; i++) {
            ContentValues cv = new ContentValues();
            cv.put(ContactsContract.Contacts.DISPLAY_NAME, String.format("Test Name%d", i));
            contentResolver.insert(ContactsContract.Contacts.CONTENT_URI, cv);
        }

        //Phone Numbers
        ContentValues cv = new ContentValues();
        cv.put(ContactsContract.Data.MIMETYPE,
                ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE);
        cv.put(ContactsContract.CommonDataKinds.Phone.RAW_CONTACT_ID, 3);
        cv.put(ContactsContract.CommonDataKinds.Phone.NUMBER, "+11223344");
        contentResolver.insert(ContactsContract.Data.CONTENT_URI, cv);
        cv.put(ContactsContract.Data.MIMETYPE,
                ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE);
        cv.put(ContactsContract.CommonDataKinds.Phone.RAW_CONTACT_ID, 3);
        cv.put(ContactsContract.CommonDataKinds.Phone.NUMBER, "+33445566");
        cv.put(ContactsContract.CommonDataKinds.Phone.IS_SUPER_PRIMARY, 1);
        contentResolver.insert(ContactsContract.Data.CONTENT_URI, cv);
        cv.remove(ContactsContract.CommonDataKinds.Phone.IS_SUPER_PRIMARY);
        cv.put(ContactsContract.Data.MIMETYPE,
                ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE);
        cv.put(ContactsContract.CommonDataKinds.Phone.RAW_CONTACT_ID, 6);
        cv.put(ContactsContract.CommonDataKinds.Phone.NUMBER, "+55667788");
        contentResolver.insert(ContactsContract.Data.CONTENT_URI, cv);
        cv.clear();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();

        //Events
        cv.put(ContactsContract.Data.MIMETYPE,
                ContactsContract.CommonDataKinds.Event.CONTENT_ITEM_TYPE);
        cv.put(ContactsContract.CommonDataKinds.Event.RAW_CONTACT_ID, 3);
        cv.put(ContactsContract.CommonDataKinds.Event.TYPE,
                ContactsContract.CommonDataKinds.Event.TYPE_BIRTHDAY);
        cal.set(Calendar.YEAR, 2000);
        cv.put(ContactsContract.CommonDataKinds.Event.START_DATE, dateFormat.format(cal.getTime()));
        contentResolver.insert(ContactsContract.Data.CONTENT_URI, cv);
        cv.put(ContactsContract.Data.MIMETYPE,
                ContactsContract.CommonDataKinds.Event.CONTENT_ITEM_TYPE);
        cv.put(ContactsContract.CommonDataKinds.Event.RAW_CONTACT_ID, 5);
        cv.put(ContactsContract.CommonDataKinds.Event.TYPE,
                ContactsContract.CommonDataKinds.Event.TYPE_BIRTHDAY);
        cal.set(Calendar.YEAR, 1990);
        cv.put(ContactsContract.CommonDataKinds.Event.START_DATE, dateFormat.format(cal.getTime()));
        contentResolver.insert(ContactsContract.Data.CONTENT_URI, cv);
        cv.put(ContactsContract.Data.MIMETYPE,
                ContactsContract.CommonDataKinds.Event.CONTENT_ITEM_TYPE);
        cv.put(ContactsContract.CommonDataKinds.Event.RAW_CONTACT_ID, 5);
        cv.put(ContactsContract.CommonDataKinds.Event.TYPE,
                ContactsContract.CommonDataKinds.Event.TYPE_ANNIVERSARY);
        cal.add(Calendar.DATE, 1);
        cal.set(Calendar.YEAR, 2010);
        cv.put(ContactsContract.CommonDataKinds.Event.START_DATE, dateFormat.format(cal.getTime()));
        contentResolver.insert(ContactsContract.Data.CONTENT_URI, cv);

        contactEventsHelper = new ContactEventsHelper(RuntimeEnvironment.application);
    }

    @Test
    public void shouldAddEvent() {
        ContactEvent contactEvent = new ContactEvent(ContactEvent.ID_NA, 3, 3, "",
                "2014-05-05", ContactsContract.CommonDataKinds.Event.TYPE_ANNIVERSARY);
        contactEventsHelper.addEvent(contactEvent);
        List<Event> events = contactEventsHelper.getEvents(3);
        assertEquals(2, events.size());
    }

    @Test
    public void shouldGetListOfEvents () {
        List<Event> events = contactEventsHelper.getEvents();
        //Checking 3 events are available
        assertEquals(3, events.size());
    }

    @Test
    public void shouldGetSingleContactEvent() {
        List<Event> event = contactEventsHelper.getEvents(5);
        assertEquals("Test Name4", event.get(0).getDisplayName());
    }

    @Test
    public void shouldGetGivenEventDateNameList() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        List<String> nameList =
                contactEventsHelper
                        .getNames(ContactsContract.CommonDataKinds.Event.TYPE_BIRTHDAY,
                                dateFormat.format(cal.getTime()));
        assertArrayEquals(new String[]{"Test Name2", "Test Name4"}, nameList.toArray());
    }

    @Test
    public void shouldGetContactsWithEventsToBeAdded() {
        List<Event> contacts = contactEventsHelper.getEventsToBeAddedContacts();
        assertEquals(9, contacts.size());
    }

    @Test
    public void shouldGetContactPhoneNumber() {
        String phoneNumber = contactEventsHelper.getContactPhoneNumber(3);
        assertEquals("+33445566", phoneNumber);
        phoneNumber = contactEventsHelper.getContactPhoneNumber(6);
        assertEquals("+55667788", phoneNumber);
    }

    @Test
    public void shouldUpdateContactEventDate() {
        List<Event> events = contactEventsHelper.getEvents(3);
        Event event = events.get(0);
        event.setEventDate("2001-09-08");
        int rowsAffected = contactEventsHelper.updateEventDate((ContactEvent) event);
        assertEquals(1, rowsAffected);
        events = contactEventsHelper.getEvents(3);
        assertEquals("2001-09-08", events.get(0).getEventDate());
    }

    @Test
    public void shouldDeleteContactEvent() {
        List<Event> events = contactEventsHelper.getEvents(5);
        int rowsAffected = contactEventsHelper.delete(events.get(1).getId());
        assertEquals(1, rowsAffected);
        events = contactEventsHelper.getEvents(5);
        assertEquals(1, events.size());
        assertEquals(Event.TYPE_BIRTHDAY, events.get(0).getEventType());
    }


}
