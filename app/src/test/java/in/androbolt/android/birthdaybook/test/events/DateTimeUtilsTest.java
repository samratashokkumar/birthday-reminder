package in.androbolt.android.birthdaybook.test.events;

import static org.junit.Assert.*;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import in.androbolt.android.birthdaybook.utils.DateTimeUtils;

public class DateTimeUtilsTest {

    private static final SimpleDateFormat DATE_FORMAT =
            new SimpleDateFormat("yyyy-MM-dd", Locale.US);

    @Test
    public void shouldParseEventDatesWithYear() throws ParseException {
        String inputDateHyphen = "2000-05-30";
        String inputDateDot = "2000.05.30";
        String inputDateSlash = "2000/05/30";
        assertEquals("2000-05-30", DateTimeUtils.parseEventDate(inputDateHyphen).date);
        assertEquals("2000-05-30", DateTimeUtils.parseEventDate(inputDateDot).date);
        assertEquals("2000-05-30", DateTimeUtils.parseEventDate(inputDateSlash).date);
    }

    @Test
    public void shouldParseEventDateWithYearAndTime() throws ParseException {
        String inputDateHyphen = "2000-05-30T12:00:00Z";
        assertEquals("2000-05-30", DateTimeUtils.parseEventDate(inputDateHyphen).date);
    }

    @Test
    public void shouldParseFutureEventDatesWithoutYear() {
        Calendar tomorrow = Calendar.getInstance();
        tomorrow.add(Calendar.DATE, 1);

        assertEquals(DATE_FORMAT.format(tomorrow.getTime()),DateTimeUtils.parseEventDate(
                new SimpleDateFormat("MM-dd", Locale.US).format(tomorrow.getTime())).date);
        assertEquals(DATE_FORMAT.format(tomorrow.getTime()),DateTimeUtils.parseEventDate(
                new SimpleDateFormat("-MM-dd", Locale.US).format(tomorrow.getTime())).date);
        assertEquals(DATE_FORMAT.format(tomorrow.getTime()),DateTimeUtils.parseEventDate(
                new SimpleDateFormat("--MM-dd", Locale.US).format(tomorrow.getTime())).date);
        assertEquals(DATE_FORMAT.format(tomorrow.getTime()),DateTimeUtils.parseEventDate(
                new SimpleDateFormat("MM.dd", Locale.US).format(tomorrow.getTime())).date);
        assertEquals(DATE_FORMAT.format(tomorrow.getTime()),DateTimeUtils.parseEventDate(
                new SimpleDateFormat(".MM.dd", Locale.US).format(tomorrow.getTime())).date);
        assertEquals(DATE_FORMAT.format(tomorrow.getTime()),DateTimeUtils.parseEventDate(
                new SimpleDateFormat("MM/dd", Locale.US).format(tomorrow.getTime())).date);
        assertEquals(DATE_FORMAT.format(tomorrow.getTime()),DateTimeUtils.parseEventDate(
                new SimpleDateFormat("/MM/dd", Locale.US).format(tomorrow.getTime())).date);
    }

    @Test
    public void shouldParsePastEventDatesWithouYear() {
        Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DATE, -1);
        Calendar nextYearDate = Calendar.getInstance();
        nextYearDate.add(Calendar.YEAR, 1);
        nextYearDate.add(Calendar.DATE, -1);
        assertEquals(DATE_FORMAT.format(nextYearDate.getTime()),DateTimeUtils.parseEventDate(
                new SimpleDateFormat("MM-dd", Locale.US).format(yesterday.getTime())).date);
        assertEquals(DATE_FORMAT.format(nextYearDate.getTime()),DateTimeUtils.parseEventDate(
                new SimpleDateFormat("-MM-dd", Locale.US).format(yesterday.getTime())).date);
        assertEquals(DATE_FORMAT.format(nextYearDate.getTime()),DateTimeUtils.parseEventDate(
                new SimpleDateFormat("--MM-dd", Locale.US).format(yesterday.getTime())).date);
        assertEquals(DATE_FORMAT.format(nextYearDate.getTime()),DateTimeUtils.parseEventDate(
                new SimpleDateFormat("MM.dd", Locale.US).format(yesterday.getTime())).date);
        assertEquals(DATE_FORMAT.format(nextYearDate.getTime()),DateTimeUtils.parseEventDate(
                new SimpleDateFormat(".MM.dd", Locale.US).format(yesterday.getTime())).date);
        assertEquals(DATE_FORMAT.format(nextYearDate.getTime()),DateTimeUtils.parseEventDate(
                new SimpleDateFormat("MM/dd", Locale.US).format(yesterday.getTime())).date);
        assertEquals(DATE_FORMAT.format(nextYearDate.getTime()),DateTimeUtils.parseEventDate(
                new SimpleDateFormat("/MM/dd", Locale.US).format(yesterday.getTime())).date);
    }

    @Test
    public void shouldParseLongDate() {
        assertEquals("2000-05-30", DateTimeUtils.parseEventDate("959625000000").date);
    }

    @Test
    public void shouldAddCurrentYear() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, -10);
        Calendar calCurrent = Calendar.getInstance();
        assertEquals(DATE_FORMAT.format(calCurrent.getTime()), DateTimeUtils.addCurrentYear(
                new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(cal.getTime())));
        cal.add(Calendar.DATE, 1);
        calCurrent.add(Calendar.DATE, 1);
        assertEquals(DATE_FORMAT.format(calCurrent.getTime()), DateTimeUtils.addCurrentYear(
                new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(cal.getTime())));
    }

    @Test
    public void shouldAddNextYear() {
        Calendar calPast = Calendar.getInstance();
        calPast.add(Calendar.DATE, -1);
        calPast.add(Calendar.YEAR, -10);
        Calendar calNext = Calendar.getInstance();
        calNext.add(Calendar.DATE, -1);
        calNext.add(Calendar.YEAR, 1);
        assertEquals(DATE_FORMAT.format(calNext.getTime()), DateTimeUtils.addCurrentYear(
                new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(calPast.getTime())));
    }

}
