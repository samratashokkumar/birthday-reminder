package in.androbolt.android.birthdaybook.test.eventslist;

import android.telecom.Call;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import in.androbolt.android.birthdaybook.common.Callback;
import in.androbolt.android.birthdaybook.common.models.Result;
import in.androbolt.android.birthdaybook.events.EventsRepository;
import in.androbolt.android.birthdaybook.events.FacebookICalHelper;
import in.androbolt.android.birthdaybook.events.FacebookPictureUrlUpdater;
import in.androbolt.android.birthdaybook.events.models.AppEvent;
import in.androbolt.android.birthdaybook.events.models.Event;
import in.androbolt.android.birthdaybook.events.models.FacebookEvent;
import in.androbolt.android.birthdaybook.facebookeventslist.FacebookEventsDownloaderTask;
import in.androbolt.android.birthdaybook.facebookeventslist.FacebookEventsListActivity;
import in.androbolt.android.birthdaybook.facebookeventslist.FacebookEventsListContract;
import in.androbolt.android.birthdaybook.facebookeventslist.FacebookEventsListPresenter;
import in.androbolt.android.birthdaybook.facebookeventslist.FacebookPictureUrlUpdaterTask;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Facebook Events List Presenter Test
 */
public class FacebookEventsListPresenterTest {

    @Mock
    private EventsRepository eventsRepository;

    @Mock
    private FacebookEventsListContract.View view;

    @Mock
    private FacebookEventsDownloaderTask facebookEventsDownloaderTask;

    @Mock
    private FacebookPictureUrlUpdaterTask facebookPictureUrlUpdaterTask;

    @Captor
    private ArgumentCaptor<Callback<List<FacebookEvent>>> fbDownloaderArgumentCaptor;

    @Captor
    private ArgumentCaptor<Callback<Void>> fbPicutureUpdaterArgumentCaptor;

    private FacebookEventsListPresenter facebookEventsListPresenter;
    private final List<FacebookEvent> facebookEvents = Arrays.asList(
            new FacebookEvent("Name", "1990-01-01", Event.TYPE_BIRTHDAY, "111", ""),
            new FacebookEvent("Name 1", "1990-02-01", Event.TYPE_BIRTHDAY, "112", ""),
            new FacebookEvent("Name 2", "1990-03-01", Event.TYPE_BIRTHDAY, "113", ""),
            new FacebookEvent("Name 3", "1990-04-01", Event.TYPE_BIRTHDAY, "114", ""),
            new FacebookEvent("Name 4", "1990-05-01", Event.TYPE_BIRTHDAY, "115", "")
    );

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(facebookPictureUrlUpdaterTask
                .withFacebookEvents(ArgumentMatchers.<FacebookEvent>anyList()))
                .thenReturn(facebookPictureUrlUpdaterTask);
        facebookEventsListPresenter = new FacebookEventsListPresenter(
                facebookEventsDownloaderTask, facebookPictureUrlUpdaterTask, eventsRepository);
        facebookEventsListPresenter.onViewAttached(view);
        verify(facebookEventsDownloaderTask)
                .withCallback(fbDownloaderArgumentCaptor.capture());
    }

    @Test
    public void shouldShowAllFetchedEvents() {
        fbDownloaderArgumentCaptor.getValue()
                .onFinish(new Result<>(facebookEvents, "", Result.Type.SUCCESS));
        verify(view).showEvents(new ArrayList<Event>(facebookEvents));
    }

    @Test
    public void shouldShowNonExistingEvents() throws IOException {
        when(eventsRepository.getEvents(FacebookEvent.SOURCE)).thenReturn(Arrays.<Event>asList(
                new FacebookEvent(1, 1, "Name 1", "1990-02-01", Event.TYPE_BIRTHDAY, "112", ""),
                new FacebookEvent(1, 1, "Name 3", "1990-04-01", Event.TYPE_BIRTHDAY, "114", "")
        ));
        fbDownloaderArgumentCaptor.getValue()
                .onFinish(new Result<>(facebookEvents, "", Result.Type.SUCCESS));
        verify(view).showEvents(Arrays.<Event>asList(
                facebookEvents.get(0),
                facebookEvents.get(2),
                facebookEvents.get(4)
        ));
    }

    @Test
    public void shouldImportAllNonExistingEvents() throws IOException {
        //Add some existing event
        when(eventsRepository.getEvents(FacebookEvent.SOURCE)).thenReturn(Arrays.<Event>asList(
                new FacebookEvent(1, 1, "Name 1", "1990-02-01", Event.TYPE_BIRTHDAY, "112", ""),
                new FacebookEvent(1, 1, "Name 3", "1990-04-01", Event.TYPE_BIRTHDAY, "114", "")
        ));
        fbDownloaderArgumentCaptor.getValue()
                .onFinish(new Result<>(facebookEvents, "", Result.Type.SUCCESS));
        facebookEventsListPresenter.selectAll();
        facebookEventsListPresenter.importEvents();
        verify(eventsRepository).addEvents(Arrays.<AppEvent>asList(
                facebookEvents.get(0),
                facebookEvents.get(2),
                facebookEvents.get(4)
        ));
    }

    @Test
    public void shouldImportIgnoringUnSelectedEvent() {
        fbDownloaderArgumentCaptor.getValue()
                .onFinish(new Result<>(facebookEvents, "", Result.Type.SUCCESS));
        facebookEventsListPresenter.selectAll();
        facebookEventsListPresenter.toggleSelect(2);
        facebookEventsListPresenter.toggleSelect(3);
        facebookEventsListPresenter.importEvents();
        verify(eventsRepository).addEvents(Arrays.<AppEvent>asList(
                facebookEvents.get(0),
                facebookEvents.get(1),
                facebookEvents.get(4)
        ));
    }

    @Test
    public void shouldShowUnselectedEventsAfterImport() {
        fbDownloaderArgumentCaptor.getValue()
                .onFinish(new Result<>(facebookEvents, "", Result.Type.SUCCESS));
        facebookEventsListPresenter.selectNone();
        facebookEventsListPresenter.toggleSelect(2);
        facebookEventsListPresenter.toggleSelect(3);
        facebookEventsListPresenter.importEvents();
        verify(view).showEvents(Arrays.<Event>asList(
                facebookEvents.get(0),
                facebookEvents.get(1),
                facebookEvents.get(4)
        ));
    }

    @Test
    public void shouldClearSelectedEvents() {
        fbDownloaderArgumentCaptor.getValue()
                .onFinish(new Result<>(facebookEvents, "", Result.Type.SUCCESS));
        HashSet<String> selectedIds = new HashSet<>();
        selectedIds.addAll(Arrays.asList("111", "112", "113", "114", "115"));
        verify(view).updateSelected(selectedIds);
        facebookEventsListPresenter.selectNone();
        verify(view, times(2)).updateSelected(new HashSet<String>());
    }

    @Test
    public void shouldShowSelectEventMessageIfNoEventIsSelected() {
        fbDownloaderArgumentCaptor.getValue()
                .onFinish(new Result<>(facebookEvents, "", Result.Type.SUCCESS));
        facebookEventsListPresenter.selectNone();
        facebookEventsListPresenter.importEvents();
        verify(view).showMessage(FacebookEventsListActivity.MSG_SELECT_EVENTS, null);
    }

    @Test
    public void shouldShowErrorMessage() {
        fbDownloaderArgumentCaptor.getValue()
                .onFinish(new Result<List<FacebookEvent>>(null, "error message", Result.Type.FAILURE));
        verify(view).showMessage(FacebookEventsListActivity.MSG_ERROR, "error message");
    }

    @Test
    public void shouldShowAndHideProgress() {
        fbDownloaderArgumentCaptor.getValue()
                .onFinish(new Result<>(facebookEvents, "", Result.Type.SUCCESS));
        verify(facebookPictureUrlUpdaterTask)
                .withCallback(fbPicutureUpdaterArgumentCaptor.capture());
        fbPicutureUpdaterArgumentCaptor.getValue()
                .onFinish(new Result<Void>(null, "", Result.Type.SUCCESS));
        verify(view).showProgress(FacebookEventsListActivity.PRG_MSG_FETCH);
        verify(view).showProgress(FacebookEventsListActivity.PRG_MSG_UPDATE);
        verify(view, times(2)).hideProgress();
    }

    @Test
    public void shouldShowAndHideProgressWhenError() {
        fbDownloaderArgumentCaptor.getValue()
                .onFinish(new Result<List<FacebookEvent>>(null, "", Result.Type.FAILURE));
        verify(view).showProgress(FacebookEventsListActivity.PRG_MSG_FETCH);
        verify(view).hideProgress();
    }

    @Test
    public void shouldRefreshListAfterUpdatingPictures() {
        fbDownloaderArgumentCaptor.getValue()
                .onFinish(new Result<>(facebookEvents, "", Result.Type.SUCCESS));
        verify(facebookPictureUrlUpdaterTask)
                .withCallback(fbPicutureUpdaterArgumentCaptor.capture());
        fbPicutureUpdaterArgumentCaptor.getValue()
                .onFinish(new Result<Void>(null, "", Result.Type.SUCCESS));
        verify(view).refreshList();
    }

    @Test
    public void shouldShowErrorWhenErrorInPictureUpdate() {
        fbDownloaderArgumentCaptor.getValue()
                .onFinish(new Result<>(facebookEvents, "", Result.Type.SUCCESS));
        verify(facebookPictureUrlUpdaterTask)
                .withCallback(fbPicutureUpdaterArgumentCaptor.capture());
        fbPicutureUpdaterArgumentCaptor.getValue()
                .onFinish(new Result<Void>(null, "error message", Result.Type.FAILURE));
        verify(view, times(0)).refreshList();
        verify(view).showMessage(FacebookEventsListActivity.MSG_ERROR, "error message");
    }

    @Test
    public void shouldSelectNoneWhenAllSelected() {
        fbDownloaderArgumentCaptor.getValue()
                .onFinish(new Result<>(facebookEvents, "", Result.Type.SUCCESS));
        HashSet<String> selectedIds = new HashSet<>();
        selectedIds.addAll(Arrays.asList("111", "112", "113", "114", "115"));
        verify(view).updateSelected(selectedIds);
        facebookEventsListPresenter.toggleSelectAll();
        verify(view, times(2)).updateSelected(new HashSet<String>());
    }

    @Test
    public void shouldSelectAllWhenNoneSelected() {
        fbDownloaderArgumentCaptor.getValue()
                .onFinish(new Result<>(facebookEvents, "", Result.Type.SUCCESS));
        facebookEventsListPresenter.selectNone();
        facebookEventsListPresenter.toggleSelectAll();
        HashSet<String> selectedIds = new HashSet<>();
        selectedIds.addAll(Arrays.asList("111", "112", "113", "114", "115"));
        verify(view, times(3)).updateSelected(selectedIds);
    }

    @Test
    public void shouldSelectAllWhenSomeSelected() {
        fbDownloaderArgumentCaptor.getValue()
                .onFinish(new Result<>(facebookEvents, "", Result.Type.SUCCESS));
        facebookEventsListPresenter.toggleSelect(2);
        HashSet<String> selectedIds = new HashSet<>();
        selectedIds.addAll(Arrays.asList("111", "112", "113", "114", "115"));
        facebookEventsListPresenter.toggleSelectAll();
        verify(view, times(3)).updateSelected(selectedIds);
    }

    @Test
    public void shouldShowFetchProgressWhenViewReattached() throws InterruptedException, IOException {
        facebookEventsListPresenter.onViewDetached();
        facebookEventsListPresenter.onViewAttached(view);
        verify(view, times(2)).showProgress(FacebookEventsListActivity.PRG_MSG_FETCH);
        fbDownloaderArgumentCaptor.getValue()
                .onFinish(new Result<>(facebookEvents, "", Result.Type.SUCCESS));
        verify(view, times(2)).hideProgress();
    }

    @Test
    public void shouldShowUpdateProgressWhenViewReattached() {
        fbDownloaderArgumentCaptor.getValue()
                .onFinish(new Result<>(facebookEvents, "", Result.Type.SUCCESS));
        verify(facebookPictureUrlUpdaterTask)
                .withCallback(fbPicutureUpdaterArgumentCaptor.capture());
        facebookEventsListPresenter.onViewDetached();
        facebookEventsListPresenter.onViewAttached(view);
        verify(view, times(2)).showProgress(FacebookEventsListActivity.PRG_MSG_UPDATE);
        fbPicutureUpdaterArgumentCaptor.getValue()
                .onFinish(new Result<Void>(null, "", Result.Type.SUCCESS));
        verify(view, times(3)).hideProgress();
    }

    @Test
    public void shouldNotCallViewMethodsWhenViewDetached() {
        facebookEventsListPresenter.onViewDetached();
        fbDownloaderArgumentCaptor.getValue()
                .onFinish(new Result<>(facebookEvents, "", Result.Type.SUCCESS));
        verify(view, times(0)).showEvents(ArgumentMatchers.<Event>anyList());
    }

    @Test
    public void shouldShowEventsWhenViewReattached() {
        facebookEventsListPresenter.onViewDetached();
        fbDownloaderArgumentCaptor.getValue()
                .onFinish(new Result<>(facebookEvents, "", Result.Type.SUCCESS));
        facebookEventsListPresenter.onViewAttached(view);
        verify(view).showEvents(new ArrayList<Event>(facebookEvents));
    }
}
