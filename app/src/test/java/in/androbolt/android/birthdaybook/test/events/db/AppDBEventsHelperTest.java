package in.androbolt.android.birthdaybook.test.events.db;

import android.database.Cursor;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import in.androbolt.android.birthdaybook.events.db.AppDBEventsHelper;
import in.androbolt.android.birthdaybook.events.models.AppEvent;
import in.androbolt.android.birthdaybook.events.models.CustomEvent;
import in.androbolt.android.birthdaybook.events.models.Event;
import in.androbolt.android.birthdaybook.events.models.FacebookEvent;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(RobolectricTestRunner.class)
@Config(manifest = Config.NONE)
public class AppDBEventsHelperTest {

    private AppDBEventsHelper appDBEventsHelper;

    @Before
    public void setUp() {
        this.appDBEventsHelper = new AppDBEventsHelper(RuntimeEnvironment.application);
    }

    @Test
    public void shouldGetAllEventsForAPerson() {
        CustomEvent customEvent = new CustomEvent("Test Name", "1987-01-01", Event.TYPE_BIRTHDAY);
        appDBEventsHelper.addEvent(customEvent);
        customEvent = (CustomEvent) appDBEventsHelper.getEvents().get(0);
        customEvent.setEventDate("2009-10-20");
        customEvent.setEventType(Event.TYPE_ANNIVERSARY);
        appDBEventsHelper.addEvent(customEvent);
        List<Event> events = appDBEventsHelper.getEvents(customEvent.getPeopleId());
        assertEquals(2, events.size());
    }

    @Test
    public void shouldGetSpecificSourceEvent() {
        CustomEvent customEvent = new CustomEvent("Test Name", "1987-01-01", Event.TYPE_BIRTHDAY);
        FacebookEvent facebookEvent = new FacebookEvent("Test Name 1", "1990-01-01", Event
                .TYPE_BIRTHDAY, "112233", "");
        appDBEventsHelper.addEvent(customEvent);
        appDBEventsHelper.addEvent(facebookEvent);
        List<Event> events = appDBEventsHelper.getEvents(FacebookEvent.SOURCE);
        assertEquals(1, events.size());
        assertEquals("Test Name 1", events.get(0).getDisplayName());
        List<Event> customEvents = appDBEventsHelper.getEvents(CustomEvent.SOURCE);
        assertEquals(1, customEvents.size());
        assertEquals("Test Name", customEvents.get(0).getDisplayName());
    }

    @Test
    public void shouldAddAppEvent() {
        CustomEvent customEvent = new CustomEvent("Test",
                "1990-05-05",
                Event.TYPE_BIRTHDAY);
        long eventId = appDBEventsHelper.addEvent(customEvent);
        assertTrue(eventId > 0);
    }

    @Test
    public void shouldNotAddInvalidAppEvent() {
        CustomEvent customEvent = new CustomEvent("Test",
                "invalid_date",
                Event.TYPE_BIRTHDAY);
        long eventId = appDBEventsHelper.addEvent(customEvent);
        assertTrue(eventId == -1);
    }

    @Test
    public void shouldAddListOfAppEvents() {
        List<AppEvent> facebookEvents = prepareFacebookEvents();
        appDBEventsHelper.addEvents(facebookEvents);
        List<Event> appEvents = appDBEventsHelper.getEvents();
        assertEquals(2, appEvents.size());
    }

    @Test
    public void shouldUpdateAppEvent() {
        CustomEvent customEvent = new CustomEvent("Test",
                "1990-05-05",
                Event.TYPE_BIRTHDAY);
        long eventId = appDBEventsHelper.addEvent(customEvent);
        List<Event> appEvents = appDBEventsHelper.getEvents();
        customEvent = new CustomEvent(eventId, ((AppEvent)appEvents.get(0)).getPeopleId(), "New " +
                "Test Name", "1990-06-05", Event.TYPE_BIRTHDAY);
        appDBEventsHelper.updateEvent(customEvent);
        appEvents = appDBEventsHelper.getEvents();
        assertEquals("1990-06-05", appEvents.get(0).getEventDate());
        assertEquals("New Test Name", appEvents.get(0).getDisplayName());
    }

    @Test
    public void shouldDeleteInEventsAndPeopleTable() {
        CustomEvent customEvent = new CustomEvent("Test",
                "1990-05-05",
                Event.TYPE_BIRTHDAY);
        long eventId = appDBEventsHelper.addEvent(customEvent);
        int rowsAffected = appDBEventsHelper.deleteEvent(eventId);
        assertEquals(1, rowsAffected);
        Cursor cursor = appDBEventsHelper.getReadableDatabase().rawQuery("SELECT * FROM people", null);
        assertEquals(0, cursor.getCount());
        cursor.close();
        List<Event> appEvents = appDBEventsHelper.getEvents();
        assertEquals(0, appEvents.size());
    }

    @Test
    public void shouldDeleteOnlyInEventsTable() {
        CustomEvent customEvent = new CustomEvent("Test",
                "1990-05-05",
                Event.TYPE_BIRTHDAY);
        long eventId = appDBEventsHelper.addEvent(customEvent);
        customEvent.setId(eventId);
        customEvent.setEventDate("2010-09-09");
        customEvent.setEventType(Event.TYPE_ANNIVERSARY);
        long secondEventId = appDBEventsHelper.addEvent(customEvent);
        int rowsAffected = appDBEventsHelper.deleteEvent(secondEventId);
        assertEquals(1, rowsAffected);
        Cursor cursor = appDBEventsHelper.getReadableDatabase().rawQuery("SELECT * FROM people",
                null);
        assertEquals(1, cursor.getCount());
        cursor.close();
        List<Event> appEvents = appDBEventsHelper.getEvents();
        assertEquals(1, appEvents.size());
    }

    @Test
    public void shouldGetGivenDateEventsNameList() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1980);
        AppEvent event = new CustomEvent("Kumar", dateFormat.format(cal.getTime()), Event
                .TYPE_BIRTHDAY);
        cal = Calendar.getInstance();
        appDBEventsHelper.addEvent(event);
        List<String> names = appDBEventsHelper.getNames(Event.TYPE_BIRTHDAY, dateFormat.format(cal.getTime
                ()));
        assertEquals("Kumar", names.get(0));
    }

    private List<AppEvent> prepareFacebookEvents() {
        List<AppEvent> facebookEvents = new ArrayList<>();
        facebookEvents.
                add(
                        new FacebookEvent(
                                "Test A",
                                "1990-05-05",
                                Event.TYPE_BIRTHDAY,
                                "234",
                                "path"));
        facebookEvents.
                add(
                        new FacebookEvent(
                                "Test B",
                                "1990-05-05",
                                Event.TYPE_BIRTHDAY,
                                "235",
                                "path1"));
        return facebookEvents;
    }
}
