package in.androbolt.android.birthdaybook.test.events.contacts;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.RawContacts;
import android.provider.ContactsContract.Data;

/**
 * Created by ashok on 14/2/16.
 */
public class MockContactsSqliteHelper extends SQLiteOpenHelper {
    public MockContactsSqliteHelper(Context context) {
        super(context, "contacts.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createContactsTable = "CREATE TABLE contacts ("
                + Contacts._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + Contacts.NAME_RAW_CONTACT_ID + " INTEGER, "
                + Contacts.DISPLAY_NAME + " TEXT, "
                + Contacts.HAS_PHONE_NUMBER + " INTEGER)";
        String createRawContactsTable = "CREATE TABLE raw_contacts ("
                + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + RawContacts.CONTACT_ID + " INTEGER REFERENCES contacts( "
                + Contacts._ID + " ))";
        String createVisibleContactsTable = "CREATE TABLE visible_contacts ("
                + BaseColumns._ID + " INTEGER REFERENCES contacts ("
                + Contacts._ID + "))";
        String createDataTable = "CREATE TABLE data ("
                + Data._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + Data.RAW_CONTACT_ID + " INTEGER REFERENCES raw_contacts( "
                + RawContacts._ID + " ), "
                + Data.IS_SUPER_PRIMARY + " INTEGER, "
                + Data.DATA1 + " TEXT, "
                + Data.DATA2 + " TEXT, "
                + Data.MIMETYPE + " TEXT)";
        String createDataView = "CREATE VIEW view_data AS SELECT "
                + "data." + Data._ID + " AS " + Data._ID + ", "
                + Data.RAW_CONTACT_ID + ", "
                + "raw_contacts." + RawContacts.CONTACT_ID + " AS " + RawContacts.CONTACT_ID + ", "
                + Contacts.DISPLAY_NAME + ", "
                + Contacts.HAS_PHONE_NUMBER + ", "
                + Data.IS_SUPER_PRIMARY + ", "
                + Data.DATA1 + ", "
                + Data.DATA2 + ", "
                + Data.MIMETYPE
                + " FROM data JOIN raw_contacts ON " + Data.RAW_CONTACT_ID
                + " = raw_contacts." + RawContacts._ID + " JOIN contacts ON raw_contacts."
                + RawContacts.CONTACT_ID + " = contacts." + Contacts._ID;
        String createContactsView = "CREATE VIEW view_contacts AS SELECT "
                + "contacts." + Contacts._ID + " AS " + Contacts._ID + ", "
                + Contacts.NAME_RAW_CONTACT_ID + ", "
                + Contacts.DISPLAY_NAME + ", "
                + Contacts.HAS_PHONE_NUMBER +", "
                + "CAST(EXISTS (SELECT _id FROM visible_contacts "
                + "WHERE contacts._id=visible_contacts._id) AS INTEGER) AS in_visible_group "
                + " FROM contacts";
        db.execSQL(createContactsTable);
        db.execSQL(createRawContactsTable);
        db.execSQL(createVisibleContactsTable);
        db.execSQL(createDataTable);
        db.execSQL(createDataView);
        db.execSQL(createContactsView);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
