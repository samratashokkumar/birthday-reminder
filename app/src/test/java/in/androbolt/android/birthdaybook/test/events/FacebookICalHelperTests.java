package in.androbolt.android.birthdaybook.test.events;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import in.androbolt.android.birthdaybook.common.models.Result;
import in.androbolt.android.birthdaybook.events.FacebookICalHelper;
import in.androbolt.android.birthdaybook.events.models.AppEvent;
import in.androbolt.android.birthdaybook.events.models.Event;
import in.androbolt.android.birthdaybook.events.models.FacebookEvent;
import okhttp3.HttpUrl;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okio.Buffer;

import static org.junit.Assert.assertEquals;

/**
 * Facebook Event Importer Test
 */
public class FacebookICalHelperTests {

    private MockWebServer server;

    @Before
    public void setUp() throws IOException {
        server = new MockWebServer();
        server.enqueue(new MockResponse().setBody(new Buffer().readFrom(getClass().getClassLoader()
                .getResourceAsStream("events.ics"))));
    }

    @Test
    public void shouldGetListOfEvents() {
        Object[][] expectedData = new Object[][] {
                new Object[] {"Prasdfaser Laasdfasfs", "139", "-07-05", Event.TYPE_BIRTHDAY},
                new Object[] {"Pr La", "10135", "-01-08", Event.TYPE_BIRTHDAY},
                new Object[] {"Ga De", "104138", "-11-09", Event.TYPE_BIRTHDAY},
                new Object[] {"Ha Ha", "105014", "-03-11", Event.TYPE_BIRTHDAY},
                new Object[] {"Ka Ka", "10838", "-06-11", Event.TYPE_BIRTHDAY}
        };
        try {
            FacebookICalHelper facebookICalHelper =
                    new FacebookICalHelper(server.url("/ical/b.php?uid=1234&key=abc").toString());
            Result<List<FacebookEvent>> result = facebookICalHelper.fetchEvents();
            assertEquals(Result.Type.SUCCESS, result.getType());
            List<FacebookEvent> eventList = result.getData();
            assertEquals(5, eventList.size());
            for (int i = 0; i < eventList.size(); i++) {
                AppEvent appEvent = eventList.get(i);
                assertEquals(expectedData[i][0], appEvent.getDisplayName());
                assertEquals(expectedData[i][1], appEvent.getNativeId());
                assertEquals(expectedData[i][2], appEvent.getEventDate());
                assertEquals(expectedData[i][3], appEvent.getEventType());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void shouldReturnFailureResult() throws IOException {
        FacebookICalHelper facebookICalHelper =
                new FacebookICalHelper("http://asfdawerhkj.sfdsfsfd");
        Result<List<FacebookEvent>> result = facebookICalHelper.fetchEvents();
        assertEquals(Result.Type.FAILURE, result.getType());
    }

    @After
    public void tearDown() throws IOException {
        server.shutdown();
    }
}
