package in.androbolt.android.birthdaybook.test.contactevents;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import in.androbolt.android.birthdaybook.contactevent.AddEventContract;
import in.androbolt.android.birthdaybook.contactevent.AddEventPresenter;
import in.androbolt.android.birthdaybook.events.contacts.ContactEventsHelper;
import in.androbolt.android.birthdaybook.events.models.ContactEvent;
import in.androbolt.android.birthdaybook.events.models.Event;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Add Contact Event Presenter Test
 */
@RunWith(RobolectricTestRunner.class)
public class AddEventPresenterTest {

    @Mock
    ContactEventsHelper contactEventsHelper;

    @Mock
    AddEventContract.View view;

    AddEventPresenter addEventPresenter;

    private final List<Event> contactEvents = Arrays.asList(
            (Event) new ContactEvent(ContactEvent.ID_NA, 1, 1, "A"),
            new ContactEvent(ContactEvent.ID_NA, 1, 1, "B"),
            new ContactEvent(ContactEvent.ID_NA, 2, 2, "C"),
            new ContactEvent(ContactEvent.ID_NA, 3, 3, "D"),
            new ContactEvent(ContactEvent.ID_NA, 4, 4, "E")
    );

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(contactEventsHelper.getEventsToBeAddedContacts()).thenReturn(contactEvents);
        addEventPresenter = new AddEventPresenter(contactEventsHelper);
        addEventPresenter.onViewAttached(view);
    }

    @Test
    public void shouldAddBothBirthdayAndAnniversary() {
        when(contactEventsHelper.getEvents(1)).thenReturn(new ArrayList<Event>());
        addEventPresenter.onItemClick(0);
        verify(view).showAddContactEventDialog(contactEvents.get(0), 0, -1);
    }

    @Test
    public void shouldAddBirthday() {
        when(contactEventsHelper.getEvents(1))
                .thenReturn(Arrays.asList((Event)
                        new ContactEvent(1, 1, 1, "A", "1990-01-01", Event.TYPE_ANNIVERSARY)));
        addEventPresenter.onItemClick(0);
        verify(view).showAddContactEventDialog(contactEvents.get(0), 0, Event.TYPE_BIRTHDAY);
    }

    @Test
    public void shouldAddAnniversary() {
        when(contactEventsHelper.getEvents(1))
                .thenReturn(Arrays.asList((Event)
                        new ContactEvent(1, 1, 1, "A", "1990-01-01", Event.TYPE_BIRTHDAY)));
        addEventPresenter.onItemClick(0);
        verify(view).showAddContactEventDialog(contactEvents.get(0), 0, Event.TYPE_ANNIVERSARY);
    }
}
