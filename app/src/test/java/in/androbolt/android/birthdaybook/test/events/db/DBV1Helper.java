package in.androbolt.android.birthdaybook.test.events.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import in.androbolt.android.birthdaybook.events.models.Event;

/**
 * App Event DB V1 Helper for testing the DB upgrade
 */
public class DBV1Helper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "events.db";
    private static final int SCHEMA_VERSION = 1;

    private static final String TABLE_PEOPLE = "people";
    private static final String TABLE_EVENTS = "events";

    public static final String SOURCE_NC = "nc";
    public static final String SOURCE_FB = "fb";

    public static abstract class PeopleColumns {
        public static final String ID = "_id";
        public static final String FB_UID = "fb_uid";
        public static final String NAME = "name";
        public static final String PIC_URL = "pic_url";
    }

    public static abstract class EventsColumns {
        public static final String ID = "_id";
        public static final String PEOPLE_ID = "people_id";
        public static final String DATE = "eventdate";
        public static final String TYPE = "eventtype";
        public static final String SOURCE = "eventsource";
        public static final String SHOW = "show";
    }

    public DBV1Helper(Context context) {
        super(context, DATABASE_NAME, null, SCHEMA_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createPeopleTable = "CREATE TABLE " + TABLE_PEOPLE + " ("
                + PeopleColumns.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + PeopleColumns.FB_UID + " INTEGER UNIQUE, "
                + PeopleColumns.NAME + " TEXT, " + PeopleColumns.PIC_URL
                + " TEXT)";
        String createEventsTable = "CREATE TABLE " + TABLE_EVENTS + " ("
                + EventsColumns.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + EventsColumns.PEOPLE_ID + " INTEGER, " + EventsColumns.DATE
                + " TEXT, " + EventsColumns.TYPE + " INTEGER, "
                + EventsColumns.SOURCE + " TEXT, " + EventsColumns.SHOW
                + " INTEGER)";
        db.execSQL(createPeopleTable);
        db.execSQL(createEventsTable);
        addTestData(db);
    }

    private void addTestData(SQLiteDatabase db) {
        ContentValues peopleCV = new ContentValues();
        peopleCV.put(PeopleColumns.FB_UID, 1);
        peopleCV.put(PeopleColumns.NAME, "Test Name");
        peopleCV.put(PeopleColumns.PIC_URL, "url");
        long peopleId = db.insert(TABLE_PEOPLE, null, peopleCV);
        ContentValues eventCV = new ContentValues();
        eventCV.put(EventsColumns.PEOPLE_ID, peopleId);
        eventCV.put(EventsColumns.DATE, "1990-05-05");
        eventCV.put(EventsColumns.TYPE, Event.TYPE_BIRTHDAY);
        eventCV.put(EventsColumns.SOURCE, SOURCE_FB);
        eventCV.put(EventsColumns.SHOW, 1);
        db.insert(TABLE_EVENTS, null, eventCV);

        ContentValues peopleCVNonContact = new ContentValues();
        peopleCVNonContact.put(PeopleColumns.NAME, "Test Name A");
        long peopleIdNonContact = db.insert(TABLE_PEOPLE, null, peopleCVNonContact);
        ContentValues eventCVNonContact = new ContentValues();
        eventCVNonContact.put(EventsColumns.PEOPLE_ID, peopleIdNonContact);
        eventCVNonContact.put(EventsColumns.DATE, "1980-05-05");
        eventCVNonContact.put(EventsColumns.TYPE, Event.TYPE_BIRTHDAY);
        eventCVNonContact.put(EventsColumns.SOURCE, SOURCE_NC);
        eventCVNonContact.put(EventsColumns.SHOW, 1);
        db.insert(TABLE_EVENTS, null, eventCVNonContact);

        ContentValues eventCVNonContact1 = new ContentValues();
        eventCVNonContact1.put(EventsColumns.PEOPLE_ID, peopleIdNonContact);
        eventCVNonContact1.put(EventsColumns.DATE, "1995-09-05");
        eventCVNonContact1.put(EventsColumns.TYPE, Event.TYPE_ANNIVERSARY);
        eventCVNonContact1.put(EventsColumns.SOURCE, SOURCE_NC);
        eventCVNonContact1.put(EventsColumns.SHOW, 1);
        db.insert(TABLE_EVENTS, null, eventCVNonContact1);

        ContentValues peopleCVFBContact = new ContentValues();
        peopleCVFBContact.put(PeopleColumns.FB_UID, 2);
        peopleCVFBContact.put(PeopleColumns.NAME, "Test Name B");
        peopleCVFBContact.put(PeopleColumns.PIC_URL, "newUrl");
        long peopleIdFBContact = db.insert(TABLE_PEOPLE, null, peopleCVFBContact);
        ContentValues eventCVFBContact = new ContentValues();
        eventCVFBContact.put(EventsColumns.PEOPLE_ID, peopleIdFBContact);
        eventCVFBContact.put(EventsColumns.DATE, "1980-05-05");
        eventCVFBContact.put(EventsColumns.TYPE, Event.TYPE_BIRTHDAY);
        eventCVFBContact.put(EventsColumns.SOURCE, SOURCE_FB);
        eventCVFBContact.put(EventsColumns.SHOW, 1);
        db.insert(TABLE_EVENTS, null, eventCVFBContact);

        ContentValues eventCVFBContact1 = new ContentValues();
        eventCVFBContact1.put(EventsColumns.PEOPLE_ID, peopleIdFBContact);
        eventCVFBContact1.put(EventsColumns.DATE, "1995-09-05");
        eventCVFBContact1.put(EventsColumns.TYPE, Event.TYPE_ANNIVERSARY);
        eventCVFBContact1.put(EventsColumns.SOURCE, SOURCE_FB);
        eventCVFBContact1.put(EventsColumns.SHOW, 1);
        db.insert(TABLE_EVENTS, null, eventCVFBContact1);

        ContentValues peopleCVNonContactSingleEvent = new ContentValues();
        peopleCVNonContactSingleEvent.put(PeopleColumns.NAME, "Test Name C");
        long peopleIdNonContactSingleEvent = db.insert(TABLE_PEOPLE, null, peopleCVNonContactSingleEvent);
        ContentValues eventCVNonContactSingleEvent = new ContentValues();
        eventCVNonContactSingleEvent.put(EventsColumns.PEOPLE_ID, peopleIdNonContactSingleEvent);
        eventCVNonContactSingleEvent.put(EventsColumns.DATE, "1990-05-05");
        eventCVNonContactSingleEvent.put(EventsColumns.TYPE, Event.TYPE_BIRTHDAY);
        eventCVNonContactSingleEvent.put(EventsColumns.SOURCE, SOURCE_NC);
        eventCVNonContactSingleEvent.put(EventsColumns.SHOW, 1);
        db.insert(TABLE_EVENTS, null, eventCVNonContactSingleEvent);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
