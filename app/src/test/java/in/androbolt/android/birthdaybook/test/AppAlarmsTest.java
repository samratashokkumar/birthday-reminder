package in.androbolt.android.birthdaybook.test;

import android.app.AlarmManager;
import android.content.Context;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.shadows.ShadowAlarmManager;
import org.robolectric.shadows.ShadowPendingIntent;

import in.androbolt.android.birthdaybook.alarms.AppAlarms;
import in.androbolt.android.birthdaybook.appwidget.AppWidget;
import in.androbolt.android.birthdaybook.notifications.FutureEventsNotificationReceiver;
import in.androbolt.android.birthdaybook.notifications.TodaysEventsNotificationReceiver;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.robolectric.Shadows.shadowOf;

/**
 * App Alarms Test
 */
@RunWith(RobolectricTestRunner.class)
public class AppAlarmsTest {

    private AppAlarms appAlarms;
    private ShadowAlarmManager shadowAlarmManager;

    @Before
    public void setUp() {
        appAlarms = new AppAlarms(RuntimeEnvironment.application.getApplicationContext());
        AlarmManager alarmManager = (AlarmManager) RuntimeEnvironment.application
                .getSystemService(Context.ALARM_SERVICE);
        shadowAlarmManager = shadowOf(alarmManager);
    }

    @Test
    public void shouldScheduleTodaysEventsAlarm() {
        String notificationTime = "0700";
        long timeInMillis = AppAlarms.getAlarmTime(notificationTime).getTimeInMillis();
        appAlarms.setTodaysEventsNotificationAlarm(notificationTime);
        ShadowAlarmManager.ScheduledAlarm scheduledAlarm = shadowAlarmManager
                .getNextScheduledAlarm();
        ShadowPendingIntent shadowPendingIntent = shadowOf(scheduledAlarm.operation);
        assertEquals(TodaysEventsNotificationReceiver.INTENT_ACTION, shadowPendingIntent
                .getSavedIntent().getAction());
        assertEquals(timeInMillis, scheduledAlarm.triggerAtTime);
    }

    @Test
    public void shouldCancelTodaysEventsAlarm() {
        String notificationTime = "0700";
        long timeInMillis = AppAlarms.getAlarmTime(notificationTime).getTimeInMillis();
        appAlarms.setTodaysEventsNotificationAlarm(notificationTime);
        appAlarms.cancelTodaysEventsNotificationAlarm();
        assertNull(shadowAlarmManager.getNextScheduledAlarm());
    }

    @Test
    public void shouldScheduleFutureEventsAlarm() {
        String notificationTime = "0700";
        long timeInMillis = AppAlarms.getAlarmTime(notificationTime).getTimeInMillis();
        appAlarms.setFutureEventsNotificationAlarm(notificationTime);
        ShadowAlarmManager.ScheduledAlarm scheduledAlarm = shadowAlarmManager
                .getNextScheduledAlarm();
        ShadowPendingIntent shadowPendingIntent = shadowOf(scheduledAlarm.operation);
        assertEquals(FutureEventsNotificationReceiver.INTENT_ACTION, shadowPendingIntent
                .getSavedIntent().getAction());
        assertEquals(timeInMillis, scheduledAlarm.triggerAtTime);
    }

    @Test
    public void shouldCancelFutureEventsAlarm() {
        String notificationTime = "0700";
        long timeInMillis = AppAlarms.getAlarmTime(notificationTime).getTimeInMillis();
        appAlarms.setFutureEventsNotificationAlarm(notificationTime);
        appAlarms.cancelFutureEventsNotificationAlarm();
        ShadowAlarmManager.ScheduledAlarm scheduledAlarm = shadowAlarmManager
                .getNextScheduledAlarm();
        assertNull(scheduledAlarm);
    }

    @Test
    public void shouldScheduleAppWidgetUpdateAlarm() {
        appAlarms.setAppWidgetUpdateAlarm();
        ShadowAlarmManager.ScheduledAlarm scheduledAlarm = shadowAlarmManager
                .getNextScheduledAlarm();
        ShadowPendingIntent shadowPendingIntent = shadowOf(scheduledAlarm.operation);
        assertEquals(AppWidget.UPDATE_ACTION, shadowPendingIntent.getSavedIntent().getAction());
    }

    @Test
    public void shouldCancelAppWidgetAlarm() {
        appAlarms.setAppWidgetUpdateAlarm();
        appAlarms.cancelAppWidgetUpdateAlarm();
        assertNull(shadowAlarmManager.getNextScheduledAlarm());
    }
}
