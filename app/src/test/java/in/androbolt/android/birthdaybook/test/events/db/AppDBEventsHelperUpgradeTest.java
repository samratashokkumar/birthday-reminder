package in.androbolt.android.birthdaybook.test.events.db;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

import java.util.List;

import in.androbolt.android.birthdaybook.events.db.AppDBEventsHelper;
import in.androbolt.android.birthdaybook.events.models.AppEvent;
import in.androbolt.android.birthdaybook.events.models.CustomEvent;
import in.androbolt.android.birthdaybook.events.models.Event;
import in.androbolt.android.birthdaybook.events.models.FacebookEvent;

import static org.junit.Assert.assertEquals;

/**
 * Test for DB Upgrade
 */
@RunWith(RobolectricTestRunner.class)
public class AppDBEventsHelperUpgradeTest {

    @Test
    public void shouldUpgradeDB() {
        DBV1Helper DBV1Helper =
                new DBV1Helper(RuntimeEnvironment.application);
        DBV1Helper.getWritableDatabase();
        DBV1Helper.close();
        AppDBEventsHelper appDBEventsHelper = new AppDBEventsHelper(RuntimeEnvironment.application);
        appDBEventsHelper.getWritableDatabase();
        List<Event> appEvents = appDBEventsHelper.getEvents();
        assertEquals(6, appEvents.size());
        Object[][] expectedData = new Object[][] {
                new Object[] {"Test Name", FacebookEvent.SOURCE, "1", "url", "1990-05-05", Event.TYPE_BIRTHDAY},
                new Object[] {"Test Name A", CustomEvent.SOURCE, null, null, "1980-05-05", Event.TYPE_BIRTHDAY},
                new Object[] {"Test Name A", CustomEvent.SOURCE, null, null, "1995-09-05", Event.TYPE_ANNIVERSARY},
                new Object[] {"Test Name B", FacebookEvent.SOURCE, "2", "newUrl", "1980-05-05", Event.TYPE_BIRTHDAY},
                new Object[] {"Test Name B", FacebookEvent.SOURCE, "2", "newUrl", "1995-09-05", Event.TYPE_ANNIVERSARY},
                new Object[] {"Test Name C", CustomEvent.SOURCE, null, null, "1990-05-05", Event.TYPE_BIRTHDAY}
        };
        for (int i = 0; i < appEvents.size(); i++) {
            AppEvent appEvent = (AppEvent) appEvents.get(i);
            assertEquals(expectedData[i][0], appEvent.getDisplayName());
            assertEquals(expectedData[i][1], appEvent.getEventSource());
            assertEquals(expectedData[i][2], appEvent.getNativeId());
            assertEquals(expectedData[i][3], appEvent.getPicturePath());
            assertEquals(expectedData[i][4], appEvent.getEventDate());
            assertEquals(expectedData[i][5], appEvent.getEventType());
        }
    }

}
