package in.androbolt.android.birthdaybook;

import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class FacebookWebViewClient extends WebViewClient {

    @SuppressWarnings("unused")
    private static final String TAG = "FacebookWebViewClient";
    private WebCalURLListener webCalURLListener;

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        if(url != null && url.startsWith("webcal://")){
            if(webCalURLListener != null){
                webCalURLListener.foundWebCalURL(url);
            }
            return  true;
        }
        return false;
    }

    public void setWebCalURLListener(WebCalURLListener webCalURLListener) {
        this.webCalURLListener = webCalURLListener;
    }

    public interface WebCalURLListener{
        void foundWebCalURL(String url);
    }
}
