package in.androbolt.android.birthdaybook.facebookeventslist;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import in.androbolt.android.birthdaybook.common.Callback;
import in.androbolt.android.birthdaybook.common.models.Result;
import in.androbolt.android.birthdaybook.events.EventsRepository;
import in.androbolt.android.birthdaybook.events.models.AppEvent;
import in.androbolt.android.birthdaybook.events.models.Event;
import in.androbolt.android.birthdaybook.events.models.FacebookEvent;
import in.androbolt.android.birthdaybook.utils.NoOp;

/**
 * Facebook Events List Presenter
 */
public class FacebookEventsListPresenter implements FacebookEventsListContract.ActionListener {

    private static final Comparator<Event> NAME_COMPARATOR = new Comparator<Event>() {
        @Override
        public int compare(Event lhs, Event rhs) {
            return lhs.getDisplayName().compareTo(rhs.getDisplayName());
        }
    };
    private static final String TAG = FacebookEventsListPresenter.class.getSimpleName();

    private FacebookEventsDownloaderTask fbEventsDownloaderTask;
    private FacebookPictureUrlUpdaterTask fbPictureUrlUpdaterTask;

    private FacebookEventsListContract.View view;
    private EventsRepository eventsRepository;
    private List<FacebookEvent> events;
    private Set<String> selectedIds;
    private State state;

    public FacebookEventsListPresenter(FacebookEventsDownloaderTask fbEventsDownloaderTask,
                                       FacebookPictureUrlUpdaterTask fbPictureUrlUpdaterTask, EventsRepository eventsRepository) {
        this.fbEventsDownloaderTask = fbEventsDownloaderTask;
        this.fbPictureUrlUpdaterTask = fbPictureUrlUpdaterTask;
        this.eventsRepository = eventsRepository;
        this.selectedIds = new HashSet<>();
        this.state = State.IDLE;
        configureCallbacks();
    }

    private void configureCallbacks() {
        this.fbEventsDownloaderTask.withCallback(new Callback<List<FacebookEvent>>() {
            @Override
            public void onFinish(Result<List<FacebookEvent>> result) {
                updateEventsResult(result);
            }
        });
        this.fbPictureUrlUpdaterTask.withCallback(new Callback<Void>() {
            @Override
            public void onFinish(Result<Void> result) {
                updatePictureResult(result);
            }
        });
    }

    @Override
    public void importEvents() {
        ArrayList<AppEvent> appEvents = new ArrayList<>();
        for (FacebookEvent facebookEvent: events) {
            if (selectedIds.contains(facebookEvent.getNativeId())) {
                appEvents.add(facebookEvent);
            }
        }
        if (appEvents.size() > 0) {
            eventsRepository.addEvents(appEvents);
            events.removeAll(appEvents);
            int importCount = selectedIds.size();
            selectedIds.clear();
            view.showEvents(new ArrayList<Event>(events));
            view.showMessage(FacebookEventsListActivity.MSG_IMPORTED, String.valueOf(importCount));
        } else {
            view.showMessage(FacebookEventsListActivity.MSG_SELECT_EVENTS, null);
        }
    }

    @Override
    public void selectAll() {
        for (FacebookEvent facebookEvent: events) {
            selectedIds.add(facebookEvent.getNativeId());
        }
        view.updateSelected(selectedIds);
    }

    @Override
    public void selectNone() {
        selectedIds.clear();
        view.updateSelected(selectedIds);
    }

    @Override
    public void toggleSelectAll() {
        if (selectedIds.size() < events.size()) {
            selectAll();
        } else {
            selectNone();
        }
    }

    @Override
    public void toggleSelect(int position) {
        String nativeId = events.get(position).getNativeId();
        if (selectedIds.contains(nativeId)) {
            selectedIds.remove(nativeId);
        } else {
            selectedIds.add(nativeId);
        }
        view.updateSelected(selectedIds);
    }

    @Override
    public void onMenuCreated() {
        view.updateSelected(selectedIds);
    }

    private void updateEventsResult(Result<List<FacebookEvent>> result) {
        this.state = State.IDLE;
        this.view.hideProgress();
        if (result.getType() == Result.Type.SUCCESS) {
            List<Event> existingEvents = eventsRepository.getEvents(FacebookEvent.SOURCE);
            events = removeExistingEvents(result.getData(), existingEvents);
            Collections.sort(events, NAME_COMPARATOR);
            view.showEvents(new ArrayList<Event>(events));
            selectAll();
            if (events.size() > 0) {
                state = State.UPDATING;
                this.view.showProgress(FacebookEventsListActivity.PRG_MSG_UPDATE);
                fbPictureUrlUpdaterTask.withFacebookEvents(events).execute();
            }
        } else {
            view.showMessage(FacebookEventsListActivity.MSG_ERROR, result.getMessage());
        }
    }

    private void updatePictureResult(Result<Void> result) {
        this.state = State.IDLE;
        this.view.hideProgress();
        if (result.getType() == Result.Type.SUCCESS) {
            view.refreshList();
        } else {
            view.showMessage(FacebookEventsListActivity.MSG_ERROR, result.getMessage());
        }
    }

    private List<FacebookEvent> removeExistingEvents(List<FacebookEvent> fetchedEvents,
                                                     List<Event> existingEvents) {
        List<FacebookEvent> destinationEvents = new ArrayList<>();
        Set<String> existingIds = new HashSet<>();
        for (Event existingEvent: existingEvents) {
            existingIds.add(((AppEvent) existingEvent).getNativeId());
        }
        for (FacebookEvent event: fetchedEvents) {
            if (!existingIds.contains(event.getNativeId())) {
                destinationEvents.add(event);
            }
        }
        return destinationEvents;
    }

    @Override
    public void onViewAttached(FacebookEventsListContract.View view) {
        this.view = view;
        if (events != null) {
            this.view.showEvents(new ArrayList<Event>(events));
        } else if (state != State.FETCHING && state != State.UPDATING) {
            state = State.FETCHING;
            fbEventsDownloaderTask.execute();
        }
        switch (state) {
            case FETCHING:
                this.view.showProgress(FacebookEventsListActivity.PRG_MSG_FETCH);
                break;
            case UPDATING:
                this.view.showProgress(FacebookEventsListActivity.PRG_MSG_UPDATE);
                break;
        }
    }

    @Override
    public void onViewDetached() {
        this.view.hideProgress();
        this.view =
                (FacebookEventsListContract.View) NoOp.of(FacebookEventsListContract.View.class);
    }

    @Override
    public void onDestroyed() {

    }

    private enum State {
        FETCHING,
        UPDATING,
        IDLE
    }
}