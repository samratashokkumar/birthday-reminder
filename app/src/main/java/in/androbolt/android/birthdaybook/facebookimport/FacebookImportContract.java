package in.androbolt.android.birthdaybook.facebookimport;

/**
 * Created by ashok on 8/3/16.
 */
public interface FacebookImportContract {
    interface View {
        void showFacebookEventsList(String webCalUrl);
    }

    interface ActionListener {
        void openFacebookEventsList(String webCalUrl);
    }
}
