package in.androbolt.android.birthdaybook.facebookimport;

/**
 * Facebook Import Presenter
 */
public class FacebookImportPresenter implements FacebookImportContract.ActionListener {

    private FacebookImportContract.View view;

    public FacebookImportPresenter (FacebookImportContract.View view) {
        this.view = view;
    }

    @Override
    public void openFacebookEventsList(String webCalUrl) {
        this.view.showFacebookEventsList(webCalUrl);
    }
}
