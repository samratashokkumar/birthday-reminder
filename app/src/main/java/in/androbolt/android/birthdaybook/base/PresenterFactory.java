package in.androbolt.android.birthdaybook.base;

public interface PresenterFactory<P extends Presenter> {

    P create();

}
