package in.androbolt.android.birthdaybook.facebookimport;

import android.content.ClipDescription;
import android.content.ClipboardManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.regex.Pattern;

import in.androbolt.android.birthdaybook.FacebookWebViewClient;
import in.androbolt.android.birthdaybook.R;
import in.androbolt.android.birthdaybook.activities.FacebookWebCalURLSelectorActivity;
import in.androbolt.android.birthdaybook.facebookeventslist.FacebookEventsListActivity;
import in.androbolt.android.birthdaybook.preferences.AppPreferences;

/**
 * Facebook Events Import Activity
 */
public class FacebookImportActivity extends AppCompatActivity {

    @SuppressWarnings("unused")
    private static final String TAG = "FacebookImportActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facebookimport);
        Toolbar appToolbar = (Toolbar) findViewById(R.id.appToolbar);
        setSupportActionBar(appToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ClipboardManager clipboardManager = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        if (clipboardManager.hasPrimaryClip()
                && clipboardManager
                .getPrimaryClipDescription().hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN)) {
            Pattern fbWebCalPattern = Pattern
                    .compile("webcal:\\/\\/www\\.facebook\\.com\\/ical\\/b\\.php\\?uid=\\d+&key=\\w+");
            String content = clipboardManager.getPrimaryClip().getItemAt(0).getText().toString();
            if (fbWebCalPattern.matcher(content).find()) {
                AppPreferences appPreferences = new AppPreferences(getApplicationContext());
                appPreferences.setFbWebCalUrl(content);
                finish();
                Intent intent = new Intent(getApplicationContext(), FacebookEventsListActivity.class);
                intent.putExtra(FacebookEventsListActivity.EXTRAS_FBWEBCALURL, content);
                startActivity(intent);
            }
        }
    }
}
