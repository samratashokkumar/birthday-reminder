package in.androbolt.android.birthdaybook.events;

import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import biweekly.ICalendar;
import biweekly.component.VEvent;
import biweekly.io.text.ICalReader;
import in.androbolt.android.birthdaybook.common.models.Result;
import in.androbolt.android.birthdaybook.events.models.Event;
import in.androbolt.android.birthdaybook.events.models.FacebookEvent;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Facebook Events Importer
 */
public class FacebookICalHelper {

    private final SimpleDateFormat dateFormat;
    private OkHttpClient client = new OkHttpClient();
    private HttpUrl webCalUrl;

    public FacebookICalHelper(String webCalUrl) {
        this.webCalUrl = HttpUrl.parse(webCalUrl);
        dateFormat = new SimpleDateFormat("-MM-dd", Locale.getDefault());
    }

    public Result<List<FacebookEvent>> fetchEvents() throws IOException {
        Request request = new Request.Builder().url(webCalUrl).build();
        try {
            Response response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                List<FacebookEvent> facebookEvents = processEvents(response.body().byteStream());
                return new Result<>(facebookEvents, "", Result.Type.SUCCESS);
            } else {
                return new Result<>(null, response.message(), Result.Type.FAILURE);
            }
        } catch (UnknownHostException exc) {
            return new Result<>(null, exc.getMessage(), Result.Type.FAILURE);
        }
    }

    private List<FacebookEvent> processEvents(InputStream inputStream) throws IOException {
        List<FacebookEvent> facebookEvents = new ArrayList<>();
        ICalReader reader = new ICalReader(inputStream);
        ICalendar iCal;
        while ((iCal = reader.readNext()) != null) {
            for (VEvent event: iCal.getEvents()) {
                String displayName = event.getSummary().getValue();
                String eventDate = getFormattedDate(
                        event.getDateStart()
                                .getValue()
                                .getRawComponents()
                                .toDate());
                String nativeId = event.getUid().getValue();
                nativeId = nativeId.substring(1, nativeId.indexOf("@"));
                FacebookEvent facebookEvent = new FacebookEvent(displayName, eventDate, Event
                        .TYPE_BIRTHDAY, nativeId, null);
                facebookEvents.add(facebookEvent);
            }
        }
        inputStream.close();
        removeSuffix(facebookEvents);
        return facebookEvents;
    }

    private void removeSuffix(List<FacebookEvent> facebookEvents) {
        //Removes suffixes such as 's birthday from Name's birthday
        String suffix = identifySuffix(facebookEvents);
        for (Event event : facebookEvents) {
            event.setDisplayName(event.getDisplayName().replace(suffix, ""));
        }
    }

    private String identifySuffix(List<FacebookEvent> facebookEvents) {
        //TODO: Improve the algorithm
        StringBuilder suffix = new StringBuilder();
        if (facebookEvents.size() > 3) {
            int mid = facebookEvents.size() / 2;
            StringBuilder firstPersonName = new StringBuilder(facebookEvents.get(0)
                    .getDisplayName()).reverse();
            StringBuilder midPersonName = new StringBuilder(facebookEvents.get(mid)
                    .getDisplayName()).reverse();
            StringBuilder lastPersonName = new StringBuilder(facebookEvents.get(facebookEvents
                    .size() - 1).getDisplayName()).reverse();
            int minLength = Collections.min(Arrays.asList(firstPersonName.length(), midPersonName
                    .length(), lastPersonName.length()));
            for (int i = 0; i < minLength; i++) {
                if (firstPersonName.charAt(i) == midPersonName.charAt(i)
                        && midPersonName.charAt(i) == lastPersonName.charAt(i)
                        && lastPersonName.charAt(i) == firstPersonName.charAt(i)) {
                    suffix.append(firstPersonName.charAt(i));
                } else {
                    break;
                }
            }
            suffix.reverse();
        }
        return suffix.toString();
    }

    private String getFormattedDate(Date eventDate) {
        return dateFormat.format(eventDate);
    }
}
