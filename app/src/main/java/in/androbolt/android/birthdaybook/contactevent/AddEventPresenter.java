package in.androbolt.android.birthdaybook.contactevent;

import java.util.List;

import in.androbolt.android.birthdaybook.events.contacts.ContactEventsHelper;
import in.androbolt.android.birthdaybook.events.models.ContactEvent;
import in.androbolt.android.birthdaybook.events.models.Event;

/**
 * Add Event Presenter
 */
public class AddEventPresenter implements AddEventContract.ActionListener {


    private ContactEventsHelper contactEventsHelper;
    private AddEventContract.View view;
    private int selectedPosition;
    private List<Event> contactsEventList;

    public AddEventPresenter(ContactEventsHelper contactEventsHelper) {
        this.contactEventsHelper = contactEventsHelper;
        contactsEventList = contactEventsHelper.getEventsToBeAddedContacts();
    }

    @Override
    public void onItemClick(int position) {
        Event event = contactsEventList.get(position);
        selectedPosition = position;
        List<Event> eventList = contactEventsHelper.getEvents(((ContactEvent)event).getContactId());
        int eventToBeAdded = -1;
        if (eventList.size() > 0) {
            boolean birthdayAvailable = false;
            boolean anniversaryAvailable = false;
            for (Event e : eventList) {
                if (e.getEventType() == Event.TYPE_BIRTHDAY) {
                    birthdayAvailable = true;
                }
                if (e.getEventType() == Event.TYPE_ANNIVERSARY) {
                    anniversaryAvailable = true;
                }
            }
            if (!birthdayAvailable) {
                eventToBeAdded = Event.TYPE_BIRTHDAY;
            }
            if (!anniversaryAvailable) {
                eventToBeAdded = Event.TYPE_ANNIVERSARY;
            }
        }
        this.view.showAddContactEventDialog(event, position, eventToBeAdded);
    }

    @Override
    public void onDateSet(ContactEvent contactEvent, boolean isLastEvent) {
        contactEventsHelper.addEvent(contactEvent);
        if (isLastEvent) {
            view.removeFromList(selectedPosition);
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String searchString) {
        view.applyFilter(searchString);
        return true;
    }

    @Override
    public void onFilterComplete(int count) {
        contactsEventList = view.getItems();
    }

    @Override
    public void onViewAttached(AddEventContract.View view) {
        this.view = view;
        this.view.showContacts(contactsEventList);
    }

    @Override
    public void onViewDetached() {
        this.view = null;
    }

    @Override
    public void onDestroyed() {

    }
}
