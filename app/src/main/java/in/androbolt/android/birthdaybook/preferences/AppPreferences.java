package in.androbolt.android.birthdaybook.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.preference.PreferenceManager;
import android.text.format.DateFormat;

import java.text.SimpleDateFormat;

import in.androbolt.android.birthdaybook.R;
import in.androbolt.android.birthdaybook.alarms.AppAlarms;

/**
 * Application Preferences
 */
public class AppPreferences {

    public static final String NOTIFY = "notify";
    public static final String NOTIFICATIONTIME = "notificationtime";
    public static final String DATEFORMAT = "dateformat";
    public static final String ISFIRSTRUN = "isFirstRun";
    public static final String ADVANCENOTIFICATION = "advancenotification";
    public static final String NOTIFYBEFORE = "notifybefore";
    public static final String ENABLESOUND = "enablesound";
    public static final String NOTIFICATIONTONE = "notification_tone";
    public static final String SHOWAGE = "showage";
    public static final String NOTIFYONEVENTDATE = "notifyoneventdate";
    public static final String LAUNCHCOUNT = "launchcount";
    public static final String AGEFORMAT = "ageformat";
    public static final String STICKYNOTIFICATION = "stickynotification";
    public static final String DEFAULT = "default";
    public static final String FBWEBCALURL = "fbwebcalurl";

    private final SharedPreferences sharedPreferences;
    private final Context context;

    public AppPreferences(Context context) {
        this.context = context;
        this.sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public boolean isFirstRun() {
        boolean isFirstRun = sharedPreferences.getBoolean(ISFIRSTRUN, true);
        if (isFirstRun) {
            sharedPreferences.edit().putBoolean(ISFIRSTRUN, false).apply();
        }
        return isFirstRun;
    }

    public boolean isNotifyInAdvanceEnabled() {
        return sharedPreferences.getBoolean(ADVANCENOTIFICATION, false);
    }

    public boolean isNotifyOnEventDateEnabled() {
        return sharedPreferences.getBoolean(NOTIFYONEVENTDATE, false);
    }

    public Integer getNotifyBefore() {
        return Integer.valueOf(sharedPreferences.getString(NOTIFYBEFORE, "1"));
    }

    public String getNotificationTime() {
        String notificationTime = sharedPreferences.getString(NOTIFICATIONTIME, AppAlarms
                .DEFAULT_TIME);
        if(notificationTime.length() != 4) {//for older notification time format (i.e. just hour)
            notificationTime = String.format("%02d%02d", Integer.valueOf(notificationTime), 0);
        }
        return notificationTime;
    }

    public boolean isStickyNotificationEnabled() {
        return sharedPreferences.getBoolean(STICKYNOTIFICATION, false);
    }

    public boolean isNotificationSoundEnabled() {
        return sharedPreferences.getBoolean(ENABLESOUND, true);
    }

    public String getNotificationTone() {
        return sharedPreferences.getString(NOTIFICATIONTONE,
                RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION).toString());
    }

    public java.text.DateFormat getPreferredDateFormat() {
        String dateFormat = sharedPreferences.getString(DATEFORMAT,
                context.getResources().getStringArray(R.array.dateformat_list_entry_values)[1]);
        return dateFormat.equals(DEFAULT) ? DateFormat.getDateFormat(this.context) : new
                SimpleDateFormat(dateFormat);
    }

    public int getAgeFormat() {
        return Integer.valueOf(sharedPreferences.getString(AGEFORMAT, "1"));
    }

    public boolean isNotificationEnabled() {
        return sharedPreferences.getBoolean(NOTIFY, true);
    }

    public void setFbWebCalUrl(String webCalUrl) {
        sharedPreferences.edit().putString(FBWEBCALURL, webCalUrl).apply();
    }

    public String getFbWebCalUrl() {
        return sharedPreferences.getString(FBWEBCALURL, "");
    }
}
