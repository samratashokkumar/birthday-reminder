package in.androbolt.android.birthdaybook.notifications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import java.util.Calendar;

import in.androbolt.android.birthdaybook.alarms.AppAlarms;
import in.androbolt.android.birthdaybook.events.EventsRepository;
import in.androbolt.android.birthdaybook.preferences.AppPreferences;
import in.androbolt.android.birthdaybook.utils.DateTimeUtils;

/**
 * Todays Events Notification Broadcast Receiver
 */
public class TodaysEventsNotificationReceiver extends BroadcastReceiver {

    public static final String INTENT_ACTION = "in.androbolt.android.birthdaybook.NOTIFY_EVENT";

    @Override
    public void onReceive(final Context context, Intent intent) {
        final EventsRepository eventsRepository = new EventsRepository(context);
        final AppPreferences appPreferences = new AppPreferences(context);
        new EventsNotifier(context, appPreferences, eventsRepository)
                .showTodaysEventsNotification();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            new AppAlarms(context).setTodaysEventsNotificationAlarm(appPreferences
                    .getNotificationTime());
        }
    }
}
