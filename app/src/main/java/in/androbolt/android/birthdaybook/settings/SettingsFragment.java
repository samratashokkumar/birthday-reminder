package in.androbolt.android.birthdaybook.settings;

import android.content.Context;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.RingtonePreference;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Locale;

import in.androbolt.android.birthdaybook.R;
import in.androbolt.android.birthdaybook.alarms.AppAlarms;
import in.androbolt.android.birthdaybook.dialogs.NotificationTimePreference;
import in.androbolt.android.birthdaybook.preferences.AppPreferences;

/**
 * Settings Fragment
 */
public class SettingsFragment extends PreferenceFragment implements SettingsContract.View,
        Preference.OnPreferenceChangeListener {

    @SuppressWarnings("unused")
    private static final String TAG = "SettingsFragment";

    private CheckBoxPreference notifyInAdvance;
    private CheckBoxPreference notifyOnEventDate;

    private SettingsContract.ActionListener actionListener;
    private RingtonePreference ringtonePreference;
    private AppPreferences appPreferences;
    private NotificationTimePreference notificationTimePreference;
    private ListPreference dateFormatPref;
    private ListPreference notifyBeforePref;
    private ListPreference ageFormatPref;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        AppAlarms appAlarms = new AppAlarms(getActivity().getApplicationContext());
        appPreferences = new AppPreferences(getActivity().getApplicationContext());
        actionListener = new SettingsPresenter(this, appAlarms, appPreferences);
        setUp();
    }

    private void setUp() {
        CheckBoxPreference enableNotification = (CheckBoxPreference) findPreference
                (AppPreferences.NOTIFY);
        enableNotification.setOnPreferenceChangeListener(this);

        notifyInAdvance = (CheckBoxPreference) findPreference(AppPreferences.ADVANCENOTIFICATION);
        notifyInAdvance.setOnPreferenceChangeListener(this);
        notifyOnEventDate = (CheckBoxPreference) findPreference(AppPreferences.NOTIFYONEVENTDATE);
        notifyOnEventDate.setOnPreferenceChangeListener(this);
        setUpRingTone();
        setUpNotificationTime();
        setUpNotifyBefore();
        setUpDateFormat();
        setUpAgeFormat();
    }

    private void setUpAgeFormat() {
        ageFormatPref = (ListPreference) findPreference(AppPreferences.AGEFORMAT);
        ageFormatPref.setOnPreferenceChangeListener(this);
        updateAgeFormatSummary(appPreferences.getAgeFormat());
    }

    private void updateAgeFormatSummary(int ageFormat) {
        String[] entries = getActivity()
                .getResources().getStringArray(R.array.notification_age_format_list_entry);
        String[] entryValues = getActivity()
                .getResources().getStringArray(R.array.notification_age_format_list_entryvalues);
        int index = Arrays.asList(entryValues).indexOf(String.valueOf(ageFormat));
        ageFormatPref.setSummary(entries[index]);
    }

    private void setUpNotifyBefore() {
        notifyBeforePref = (ListPreference) findPreference(AppPreferences.NOTIFYBEFORE);
        notifyBeforePref.setOnPreferenceChangeListener(this);
        updateNotifyBeforeSummary(appPreferences.getNotifyBefore());
    }

    private void updateNotifyBeforeSummary(int notifyBeforeDays) {
        String[] entries = getActivity()
                .getResources().getStringArray(R.array.notifybefore_list_entry);
        String[] entryValues = getActivity()
                .getResources().getStringArray(R.array.notifybefore_list_entry_values);
        int index = Arrays.asList(entryValues)
                .indexOf(String.valueOf(notifyBeforeDays));
        notifyBeforePref.setSummary(entries[index]);
    }

    private void setUpNotificationTime() {
        notificationTimePreference = (NotificationTimePreference)
                findPreference(AppPreferences.NOTIFICATIONTIME);
        notificationTimePreference.setOnPreferenceChangeListener(this);
        updateNotificationTimeSummary(appPreferences.getNotificationTime());
    }

    private void updateNotificationTimeSummary(String notificationTime) {
        Calendar cal = AppAlarms.getAlarmTime(notificationTime);
        notificationTimePreference.setSummary(DateFormat.getTimeInstance(DateFormat.SHORT).format
                (cal.getTime()));
    }

    private void setUpRingTone() {
        ringtonePreference = (RingtonePreference) findPreference
                (AppPreferences.NOTIFICATIONTONE);
        ringtonePreference.setOnPreferenceChangeListener(this);
        updateNotificationToneSummary(appPreferences.getNotificationTone());
    }

    private void updateNotificationToneSummary(String notificationTone) {
        Context context = getActivity().getApplicationContext();
        Ringtone ringtone = RingtoneManager.getRingtone(context,
                Uri.parse(notificationTone));
        ringtonePreference.setSummary(ringtone.getTitle(context));
    }

    private void setUpDateFormat() {
        dateFormatPref = (ListPreference) findPreference(AppPreferences.DATEFORMAT);
        CharSequence[] dateFormatValues
                = getActivity()
                .getResources().getTextArray(R.array.dateformat_list_entry_values);
        CharSequence[] dateFormatEntries = new CharSequence[dateFormatValues.length];
        dateFormatEntries[0] = getActivity().getResources().getString(R.string.default_text);
        Calendar today = Calendar.getInstance();
        for (int i = 1; i < dateFormatValues.length; i++) {
            dateFormatEntries[i]
                    = new SimpleDateFormat(dateFormatValues[i].toString(), Locale.getDefault())
                    .format(today.getTime());
        }
        dateFormatPref.setEntries(dateFormatEntries);
        dateFormatPref.setEntryValues(dateFormatValues);
        dateFormatPref.setDefaultValue("dd MMM, yyyy");
        dateFormatPref.setOnPreferenceChangeListener(this);
        updateDateFormatSummary(appPreferences.getPreferredDateFormat());
    }

    private void updateDateFormatSummary(DateFormat preferredDateFormat) {
        Calendar cal = Calendar.getInstance();
        dateFormatPref.setSummary(preferredDateFormat.format(cal.getTime()));
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        switch (preference.getKey()) {
            case AppPreferences.NOTIFY:
                actionListener.toggleNotification((Boolean) newValue);
                break;
            case AppPreferences.NOTIFICATIONTIME:
                actionListener.updateNotificationTime((String) newValue);
                updateNotificationTimeSummary((String) newValue);
                break;
            case AppPreferences.ADVANCENOTIFICATION:
                actionListener.toggleAdvanceNotification((Boolean) newValue);
                break;
            case AppPreferences.NOTIFYONEVENTDATE:
                actionListener.toggleNotification((Boolean) newValue);
                break;
            case AppPreferences.NOTIFICATIONTONE:
                updateNotificationToneSummary((String) newValue);
                break;
            case AppPreferences.DATEFORMAT:
                DateFormat dateFormat = newValue.equals(AppPreferences.DEFAULT) ?
                        android.text.format.DateFormat.getDateFormat(getActivity()) :
                        new SimpleDateFormat((String) newValue, Locale.getDefault());
                updateDateFormatSummary(dateFormat);
                break;
            case AppPreferences.NOTIFYBEFORE:
                updateNotifyBeforeSummary(Integer.valueOf((String) newValue));
                break;
            case AppPreferences.AGEFORMAT:
                updateAgeFormatSummary(Integer.valueOf((String) newValue));
                break;
        }
        return true;
    }

    @Override
    public void disableNotifyInAdvance() {
        notifyInAdvance.setChecked(false);
        disableNotifyOnEventDate();
    }

    @Override
    public void disableNotifyOnEventDate() {
        notifyOnEventDate.setChecked(false);
    }

}
