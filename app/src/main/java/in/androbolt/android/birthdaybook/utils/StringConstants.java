package in.androbolt.android.birthdaybook.utils;

public class StringConstants {
	public static final String PROFILE_PIC_FOLDER = "profilepic";
    public static final String DEFAULT_DATEFORMAT = "dd MMM, yyyy";

	public static class ExtraConstants {
		public static final String EXTRA_CONTACTINDEX = "in.androbolt.android.birthdaybook.ContactIndex";
		public static final String EXTRA_NOOFEVENTS = "in.androbolt.android.birthdaybook.NoOfEvents";
		public static final String EXTRA_EVENTDATE = "in.androbolt.android.birthdaybook.Date";
		public static final String EXTRA_EVENTTYPE = "in.androbolt.android.birthdaybook.EventType";
		public static final String EXTRA_ADDEVENT = "in.androbolt.android.birthdaybook.AddEvent";
		public static final String EXTRA_EVENTSOURCE = "in.androbolt.android.birthdaybook.EventSource";
		public static final String EXTRA_UID = "in.androbolt.android.birthdaybook.UID";
		public static final String EXTRA_WIDGET_CLICK = "in.androbolt.android.birthdaybook.WidgetClick";
		public static final String EXTRA_DOWNLOADLIST = "in.androbolt.android.birthdaybook.DownloadList";
	}

	public static class IntentActions {
		// Intent Actions
		public static final String ACTION_NOTIFY_EVENT = "in.androbolt.android.birthdaybook.NOTIFY_EVENT";
		public static final String ACTION_WIDGET_UPDATE = "in.androbolt.android.birthdaybook.WIDGET_UPDATE";
		public static final String ACTION_ONEFOUR_WIDGET_UPDATE = "in.androbolt.android.birthdaybook.ONEFOUR_WIDGET_UPDATE";
	}

	public static class PreferenceKeys {
		// Notification Keys
		public static final String PREFERENCE_NOTIFY = "notify";
		public static final String PREFERENCE_NOTIFICATIONTIME = "notificationtime";
		public static final String PREFERENCE_DATEFORMATFIX = "dateformatfix";
		public static final String PREFERENCE_DATEFORMAT = "dateformat";
		public static final String PREFERENCE_WIDGETPOSITION = "widgetposition";
		public static final String PREFERENCE_ISFIRSTRUN = "isFirstRun";
		public static final String PREFERENCE_ADVANCENOTIFICATION = "advancenotification";
		public static final String PREFERENCE_NOTIFYBEFORE = "notifybefore";
		public static final String PREFERENCE_ENABLESOUND = "enablesound";
		public static final String PREFERENCE_NOTIFICATIONTONE = "notification_tone";
		public static final String PREFERENCE_SHOWAGE = "showage";
		public static final String PREFERENCE_LASTSYNC = "lastSync";
		public static final String PREFERENCE_CLEARDATA = "cleardata";
		public static final String PREFERENCE_NOTIFYONEVENTDATE = "notifyoneventdate";
		public static final String PREFERENCE_LAUNCHCOUNT = "launchcount";
		public static final String PREFERENCE_AGEFORMAT = "ageformat";
		public static final String PREFERENCE_STICKYNOTIFICATION = "stickynotification";
	}

	public static class StringLiterals {
		public static final String GRAPH_PICTURE_URL = "http://graph.facebook.com/v2.5/picture?width=%d&height=%d";
		public static final String PRO_VERSION_LINK = "market://details?id=in.androbolt.android.birthdaybookpro";
		public static final String APPLICATION_PACKAGE = "in.androbolt.android.birthdaybook";
	}

	public static class WidgetActions {
		public static final String UP_ACTION = "in.androbolt.android.birthdaybook.UP_ACTION";
		public static final String DOWN_ACTION = "in.androbolt.android.birthdaybook.DOWN_ACTION";
		public static final String SMALL_UP_ACTION = "in.androbolt.android.birthdaybook.SMALL_UP_ACTION";
		public static final String SMALL_DOWN_ACTION = "in.androbolt.android.birthdaybook.SMALL_DOWN_ACTION";
	}
}
