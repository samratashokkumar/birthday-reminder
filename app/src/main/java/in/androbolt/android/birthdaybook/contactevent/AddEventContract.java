package in.androbolt.android.birthdaybook.contactevent;

import android.support.v7.widget.SearchView;
import android.widget.Filter;

import java.util.List;

import in.androbolt.android.birthdaybook.base.Presenter;
import in.androbolt.android.birthdaybook.adapters.RecyclerViewListAdapter;
import in.androbolt.android.birthdaybook.dialogs.AddContactEventDialog;
import in.androbolt.android.birthdaybook.events.models.Event;

/**
 * Add Event Contract
 */
public interface AddEventContract {

    interface View {

        void showContacts(List<Event> contactsWithoutEventsList);

        void showAddContactEventDialog(Event event, int position, int eventToBeAdded);

        void removeFromList(int selectedPosition);

        void applyFilter(String searchString);

        List<Event> getItems();
    }

    interface ActionListener extends RecyclerViewListAdapter.OnItemClickListener,
            AddContactEventDialog.OnDateSetListener,
            SearchView.OnQueryTextListener, Filter.FilterListener, Presenter<View> {

    }
}
