package in.androbolt.android.birthdaybook.facebookeventslist;

import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.util.List;

import in.androbolt.android.birthdaybook.common.Callback;
import in.androbolt.android.birthdaybook.common.models.Result;
import in.androbolt.android.birthdaybook.events.FacebookPictureUrlUpdater;
import in.androbolt.android.birthdaybook.events.models.FacebookEvent;

public class FacebookPictureUrlUpdaterTask
        extends AsyncTask<Void, Void, Result<Void>> {

    private static final String TAG = "FBPictureUrlUpdaterTask";
    private FacebookPictureUrlUpdater fbPictureUrlUpdater;
    private List<FacebookEvent> facebookEvents;
    private Callback<Void> callback;

    public FacebookPictureUrlUpdaterTask(FacebookPictureUrlUpdater fbPictureUrlUpdater) {
        this.fbPictureUrlUpdater = fbPictureUrlUpdater;
    }

    public FacebookPictureUrlUpdaterTask withCallback(Callback<Void> callback) {
        this.callback = callback;
        return this;
    }

    public FacebookPictureUrlUpdaterTask withFacebookEvents(List<FacebookEvent> facebookEvents) {
        this.facebookEvents = facebookEvents;
        return this;
    }

    @Override
    protected Result<Void> doInBackground(Void... args) {
        try {
            Result<Void> result;
            if (facebookEvents != null) {
                result = fbPictureUrlUpdater.updatePictureUrl(facebookEvents);
            } else {
                result = new Result<>(null, "Events Null", Result.Type.FAILURE);
            }
            return result;
        } catch (IOException e) {
            Log.e(TAG, String.valueOf(e));
            return new Result<>(null, e.getMessage(), Result.Type.FAILURE);
        }
    }

    @Override
    protected void onPostExecute(Result<Void> result) {
        if (this.callback != null) {
            this.callback.onFinish(result);
        }
    }
}
