package in.androbolt.android.birthdaybook.events.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Facebook App Event
 */
public class FacebookEvent extends AppEvent {

    public static final String SOURCE = "facebook";

    public FacebookEvent(String displayName,
                         String eventDate,
                         int eventType,
                         String nativeId,
                         String picturePath) {
        super(displayName, eventDate, eventType, nativeId, picturePath);
    }

    public FacebookEvent(long id,
                         long peopleId,
                         String displayName,
                         String eventDate,
                         int eventType,
                         String nativeId,
                         String picturePath) {
        super(id, peopleId, displayName, eventDate, eventType, nativeId, picturePath);
    }

    @Override
    public String getEventSource() {
        return SOURCE;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(getId());
        dest.writeString(getDisplayName());
        dest.writeString(getEventDate());
        dest.writeInt(getEventType());
        dest.writeLong(getPeopleId());
        dest.writeString(getNativeId());
        dest.writeString(getPicturePath());
    }

    public static final Parcelable.Creator<FacebookEvent> CREATOR
            = new Parcelable.Creator<FacebookEvent>() {
        @Override
        public FacebookEvent createFromParcel(Parcel source) {
            long eventId = source.readLong();
            String displayName = source.readString();
            String eventDate = source.readString();
            int eventType = source.readInt();
            long peopleId = source.readLong();
            String nativeId = source.readString();
            String picturePath = source.readString();
            return new FacebookEvent(eventId,
                    peopleId,
                    displayName,
                    eventDate,
                    eventType,
                    nativeId,
                    picturePath);
        }

        @Override
        public FacebookEvent[] newArray(int size) {
            return new FacebookEvent[size];
        }
    };
}
