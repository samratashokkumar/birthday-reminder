package in.androbolt.android.birthdaybook.mainactivity;

/**
 * Created by ashok on 28/2/16.
 */
public interface MainContract {
    interface View {
        void showSettings();
    }
    interface ActionListener {
        void openSettings();
    }
}
