package in.androbolt.android.birthdaybook.base;

public interface Presenter<V> {

    void onViewAttached(V view);
    void onViewDetached();
    void onDestroyed();

}
