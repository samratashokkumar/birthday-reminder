package in.androbolt.android.birthdaybook.events.db;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.provider.BaseColumns;

/**
 * People table DAO
 */
class People {
    @SuppressWarnings("unused")
    private static final String TAG = "db.People";
    public static final String TABLE_PEOPLE = "people";

    private final SQLiteDatabase db;

    public People(SQLiteDatabase db) {
        this.db = db;
    }

    public void create() {
        String createQuery = "CREATE TABLE " + TABLE_PEOPLE + " ( "
                + Columns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + Columns.NAME + " TEXT , "
                + Columns.SOURCE + " TEXT, "
                + Columns.NATIVE_ID + " TEXT, "
                + Columns.PICTURE_PATH + " TEXT, "
                + "UNIQUE (" + Columns.SOURCE + ", " + Columns.NATIVE_ID + "))";
        db.execSQL(createQuery);
    }

    public long add(String displayName,
                    String eventSource) {
        return add(displayName, eventSource, null, null);
    }

    public long add(String displayName,
                    String eventSource,
                    String nativeId,
                    String picturePath) {
        ContentValues cv = new ContentValues();
        cv.put(Columns.NAME, displayName);
        cv.put(Columns.SOURCE, eventSource);
        cv.put(Columns.NATIVE_ID, nativeId);
        cv.put(Columns.PICTURE_PATH, picturePath);
        return db.insert(TABLE_PEOPLE, null, cv);
    }

    public SQLiteStatement getInsertStatement() {
        return db.compileStatement
                ("INSERT INTO " + TABLE_PEOPLE + " ("
                        + Columns.NAME + ", "
                        + Columns.SOURCE + ", "
                        + Columns.NATIVE_ID + ", "
                        + Columns.PICTURE_PATH + " ) "
                        + " VALUES (?, ?, ?, ?)");
    }

    public int delete(long peopleId) {
        return db.delete(TABLE_PEOPLE,
                Columns._ID + "=?",
                new String[] { String.valueOf(peopleId) });
    }

    public int updateName(long peopleId, String newName) {
        ContentValues cv = new ContentValues();
        cv.put(Columns.NAME, newName);
        return db.update(TABLE_PEOPLE, cv,
                Columns._ID + " =? ",
                new String[] { String.valueOf(peopleId) });
    }

    /**
     * People table columns
     */
    public interface Columns extends BaseColumns {
        String NAME = "name";
        String SOURCE = "source";
        String NATIVE_ID = "native_id";
        String PICTURE_PATH = "picture_path";
    }
}
