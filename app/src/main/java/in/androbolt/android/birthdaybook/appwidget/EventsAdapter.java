package in.androbolt.android.birthdaybook.appwidget;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import in.androbolt.android.birthdaybook.R;
import in.androbolt.android.birthdaybook.events.models.Event;

/**
 * Events Adapter for App Widget
 */
public class EventsAdapter extends ArrayAdapter<Event> {

    private Context context;
    private List<Event> events;

    public EventsAdapter(Context context, int resource, List<Event> events) {
        super(context, resource, events);
        this.context = context;
        this.events = events;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context
                    .LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.app_widget_list_item, parent, false);
            viewHolder.ivPhoto = (ImageView) convertView.findViewById(R.id.ivPhoto);
            viewHolder.tvDisplayName = (TextView) convertView.findViewById(R.id.tvDisplayName);
            viewHolder.tvEventDate = (TextView) convertView.findViewById(R.id.tvEventDate);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        Event event = events.get(position);
        viewHolder.tvDisplayName.setText(event.getDisplayName());
        viewHolder.tvEventDate.setText(event.getParsedDate());
        return convertView;
    }

    private static class ViewHolder {
        public ImageView ivPhoto;
        public TextView tvDisplayName;
        public TextView tvEventDate;
    }
}
