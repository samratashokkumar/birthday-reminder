package in.androbolt.android.birthdaybook.common;

import in.androbolt.android.birthdaybook.common.models.Result;

public interface Callback<T> {

    void onFinish(Result<T> result);

}
