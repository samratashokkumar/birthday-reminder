package in.androbolt.android.birthdaybook.events;

import android.content.Context;

import java.util.List;

import in.androbolt.android.birthdaybook.events.contacts.ContactEventsHelper;
import in.androbolt.android.birthdaybook.events.db.AppDBEventsHelper;
import in.androbolt.android.birthdaybook.events.models.AppEvent;
import in.androbolt.android.birthdaybook.events.models.ContactEvent;
import in.androbolt.android.birthdaybook.events.models.Event;

/**
 * Events Repository
 */
public class EventsRepository {

    ContactEventsHelper contactEventsHelper;
    AppDBEventsHelper appDBEventsHelper;

    public EventsRepository(Context context) {
        this.contactEventsHelper = new ContactEventsHelper(context);
        this.appDBEventsHelper = new AppDBEventsHelper(context);
    }

    public EventsRepository(ContactEventsHelper contactEventsHelper, AppDBEventsHelper
            appDBEventsHelper) {
        this.contactEventsHelper = contactEventsHelper;
        this.appDBEventsHelper = appDBEventsHelper;
    }

    public long addEvent(Event event) {
        long id;
        if (ContactEvent.SOURCE.equals(event.getEventSource())) {
            id = Long.parseLong(contactEventsHelper.addEvent((ContactEvent)event).getLastPathSegment());
        } else {
            id = appDBEventsHelper.addEvent((AppEvent)event);
        }
        return id;
    }

    public void addEvents(List<AppEvent> appEvents) {
        //TODO: Add Progress Listener Parameter
        appDBEventsHelper.addEvents(appEvents);
    }

    public List<Event> getEvents() {
        List<Event> events = contactEventsHelper.getEvents();
        events.addAll(appDBEventsHelper.getEvents());
        return events;
    }

    public List<Event> getEvents(String eventSource) {
        List<Event> events;
        if (ContactEvent.SOURCE.equals(eventSource)) {
            events = contactEventsHelper.getEvents();
        } else {
            events = appDBEventsHelper.getEvents(eventSource);
        }
        return events;
    }

    public List<Event> getEvents(long id, String eventSource) {
        List<Event> events;
        if (ContactEvent.SOURCE.equals(eventSource)) {
            events = contactEventsHelper.getEvents(id);
        } else {
            events = appDBEventsHelper.getEvents(id);
        }
        return events;
    }

    public List<String> getEventDateNames(int eventType, String eventDate) {
        List<String> names = contactEventsHelper.getNames(eventType, eventDate);
        names.addAll(appDBEventsHelper.getNames(eventType, eventDate));
        return  names;
    }

    //TODO: Remove this method from EventsRepository
    public String getContactPhoneNumber(long contactId) {
        return contactEventsHelper.getContactPhoneNumber(contactId);
    }

    public void update(Event event) {
        if (ContactEvent.SOURCE.equals(event.getEventSource())) {
            contactEventsHelper.updateEventDate((ContactEvent) event);
        } else {
            appDBEventsHelper.updateEvent((AppEvent) event);
        }
    }


    public int delete(int id, String eventSource) {
        int rowsAffected;
        if (ContactEvent.SOURCE.equals(eventSource)) {
            rowsAffected = contactEventsHelper.delete(id);
        } else {
            rowsAffected = appDBEventsHelper.deleteEvent(id);
        }
        return rowsAffected;
    }
}
