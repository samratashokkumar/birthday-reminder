package in.androbolt.android.birthdaybook.base;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;

public abstract class BaseActivity<P extends Presenter<V>, V> extends AppCompatActivity {

    @SuppressWarnings("unused")
    private static final String TAG = "BaseActivity";
    private static final int LOADER_ID = 101;
    private Presenter<V> presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportLoaderManager().initLoader(LOADER_ID, null, new LoaderManager.LoaderCallbacks<P>() {
            @Override
            public Loader<P> onCreateLoader(int id, Bundle args) {
                return new PresenterLoader<P>(BaseActivity.this, getPresenterFactory());
            }

            @Override
            public void onLoadFinished(Loader<P> loader, P presenter) {
                BaseActivity.this.presenter = presenter;
                onPresenterPrepared(presenter);
            }

            @Override
            public void onLoaderReset(Loader<P> loader) {
                BaseActivity.this.presenter = null;
                onPresenterDestroyed();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onViewAttached(getPresenterView());
    }

    @Override
    protected void onStop() {
        presenter.onViewDetached();
        super.onStop();
    }

    protected abstract PresenterFactory<P> getPresenterFactory();

    protected abstract void onPresenterPrepared(P presenter);

    protected void onPresenterDestroyed() {
    }

    protected V getPresenterView() {
        return (V) this;
    }
}
