package in.androbolt.android.birthdaybook.facebookeventslist;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import in.androbolt.android.birthdaybook.R;
import in.androbolt.android.birthdaybook.base.BaseActivity;
import in.androbolt.android.birthdaybook.base.PresenterFactory;
import in.androbolt.android.birthdaybook.events.EventsRepository;
import in.androbolt.android.birthdaybook.events.FacebookICalHelper;
import in.androbolt.android.birthdaybook.events.FacebookPictureUrlUpdater;
import in.androbolt.android.birthdaybook.events.models.Event;
import in.androbolt.android.birthdaybook.preferences.AppPreferences;
import in.androbolt.android.birthdaybook.utils.StringConstants;

public class FacebookEventsListActivity extends
        BaseActivity<FacebookEventsListContract.ActionListener, FacebookEventsListContract.View>
        implements FacebookEventsListContract.View, FacebookEventsListAdapter.OnItemClickListener {

    public static final String EXTRAS_FBWEBCALURL = "EXTRAS_FBWEBCALURL";
    public static final int MSG_SELECT_EVENTS = 1;
    public static final int MSG_ERROR = 2;
    public static final int MSG_IMPORTED = 3;
    public static final int PRG_MSG_FETCH = 1;
    public static final int PRG_MSG_UPDATE = 2;

    private static final String TAG = "FELActivity";
    private FacebookEventsListAdapter facebookEventsListAdapter;
    private FacebookEventsListContract.ActionListener actionListener;
    private TextView tvToolbar;
    private MenuItem actionSelect;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facebook_events_list);
        RecyclerView rvFacebookEventsList = (RecyclerView) findViewById(R.id.rvFacebookEventsList);
        tvToolbar = (TextView) findViewById(R.id.tvToolbar);
        tvToolbar.setVisibility(View.VISIBLE);
        Toolbar appToolbar = (Toolbar) findViewById(R.id.appToolbar);
        setSupportActionBar(appToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        progressDialog = new ProgressDialog(this);
        AppPreferences appPreferences = new AppPreferences(getApplicationContext());
        DateFormat dateFormat = appPreferences.getPreferredDateFormat();
        facebookEventsListAdapter =
                new FacebookEventsListAdapter(new ArrayList<Event>(0), dateFormat);
        facebookEventsListAdapter.setOnItemClickListener(this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        rvFacebookEventsList.setLayoutManager(mLayoutManager);
        rvFacebookEventsList.setAdapter(facebookEventsListAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_facebook_import, menu);
        actionSelect = menu.findItem(R.id.action_select);
        actionListener.onMenuCreated();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_select) {
            actionListener.toggleSelectAll();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showEvents(final List<Event> events) {
        facebookEventsListAdapter.setItems(events);
        tvToolbar.setText(String.valueOf(events.size()));
    }

    @Override
    public void refreshList() {
        facebookEventsListAdapter.refreshList();
    }

    @Override
    public void updateSelected(Set<String> selectedIds) {
        facebookEventsListAdapter.updateSelectedItems(selectedIds);
        tvToolbar.setText(String.valueOf(selectedIds.size()));
        if (selectedIds.size() == facebookEventsListAdapter.getItemCount()) {
            actionSelect.setIcon(R.drawable.ic_check_box);
            actionSelect.setTitle(R.string.select_none);
        } else {
            actionSelect.setIcon(R.drawable.ic_check_outline);
            actionSelect.setTitle(R.string.select_all);
        }
    }

    @Override
    public void showMessage(int messageId, String message) {
        if (messageId == MSG_SELECT_EVENTS) {
            Toast.makeText(this, R.string.select_to_import, Toast.LENGTH_LONG).show();
        }
        if (messageId == MSG_ERROR) {
            Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        }
        if (messageId == MSG_IMPORTED) {
            Toast.makeText(this, String.format(getString(R.string.imported), message), Toast.LENGTH_LONG).show();
        }
    }
    @Override
    public void onItemClick(int position) {
        actionListener.toggleSelect(position);
    }

    @Override
    public void showProgress(int messageId) {
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        String message;
        switch (messageId) {
            case PRG_MSG_FETCH:
                message = getString(R.string.fetching_events);
                break;
            case  PRG_MSG_UPDATE:
                message = getString(R.string.updating_pictures);
                break;
            default:
                message = "";
                break;
        }
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.dismiss();
    }

    public void onImportEventsListClick(View view) {
        actionListener.importEvents();
    }

    @Override
    protected PresenterFactory<FacebookEventsListContract.ActionListener> getPresenterFactory() {
        Bundle extras = getIntent().getExtras();
        String webCalUrl = extras.getString(EXTRAS_FBWEBCALURL);
        final FacebookICalHelper facebookICalHelper =
                new FacebookICalHelper(webCalUrl.replace("webcal://", "https://"));
        final EventsRepository eventsRepository = new EventsRepository(getApplicationContext());
        final FacebookPictureUrlUpdater fbPictureUrlUpdater =
                new FacebookPictureUrlUpdater(
                        String.format(Locale.US, StringConstants.StringLiterals.GRAPH_PICTURE_URL,
                                getResources().getInteger(R.integer.pic_width),
                                getResources().getInteger(R.integer.pic_width)));
        final FacebookEventsDownloaderTask fbEventsDownloaderTask =
                new FacebookEventsDownloaderTask(facebookICalHelper);
        final FacebookPictureUrlUpdaterTask fbPictureUrlUpdaterTask =
                new FacebookPictureUrlUpdaterTask(fbPictureUrlUpdater);
        return new PresenterFactory<FacebookEventsListContract.ActionListener>() {
            @Override
            public FacebookEventsListContract.ActionListener create() {
                return new FacebookEventsListPresenter(fbEventsDownloaderTask,
                        fbPictureUrlUpdaterTask, eventsRepository
                );
            }
        };
    }

    @Override
    protected void onPresenterPrepared(FacebookEventsListContract.ActionListener actionListener) {
        this.actionListener = actionListener;
    }

}
