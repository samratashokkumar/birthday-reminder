package in.androbolt.android.birthdaybook.notifications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import java.util.Calendar;

import in.androbolt.android.birthdaybook.alarms.AppAlarms;
import in.androbolt.android.birthdaybook.events.EventsRepository;
import in.androbolt.android.birthdaybook.preferences.AppPreferences;

public class FutureEventsNotificationReceiver extends BroadcastReceiver {

    public static final String INTENT_ACTION = "in.androbolt.android.birthdaybook" +
            ".NOTIFY_FUTURE_EVENTS";

    public FutureEventsNotificationReceiver() {
    }

    @Override
    public void onReceive(final Context context, Intent intent) {
        final EventsRepository eventsRepository = new EventsRepository(context);
        final AppPreferences appPreferences = new AppPreferences(context);
        new EventsNotifier(context, appPreferences, eventsRepository)
                .showFutureEventsNotification();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            new AppAlarms(context).setFutureEventsNotificationAlarm(appPreferences
                    .getNotificationTime());
        }
    }
}
