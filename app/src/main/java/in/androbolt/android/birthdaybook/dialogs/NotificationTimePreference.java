package in.androbolt.android.birthdaybook.dialogs;

import android.content.Context;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TimePicker;

import in.androbolt.android.birthdaybook.alarms.AppAlarms;

/**
 * Created by ashok on 5/3/15.
 */
public class NotificationTimePreference extends DialogPreference{

    @SuppressWarnings("unused")
    private static final String TAG = "NotificationTimePref";
    private TimePicker timePicker;

    public NotificationTimePreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected View onCreateDialogView() {
        timePicker = new TimePicker(getContext());
        return timePicker;
    }

    @Override
    protected void onBindDialogView(View view) {
        super.onBindDialogView(view);
        String currentValue = getPersistedString(AppAlarms.DEFAULT_TIME);
        if(currentValue.length() != 4) {//for older notification time format (i.e. just hour)
            currentValue = String.format("%02d%02d", Integer.valueOf(currentValue), 0);
        }
        timePicker.setCurrentHour(Integer.valueOf(currentValue.substring(0, 2)));
        timePicker.setCurrentMinute(Integer.valueOf(currentValue.substring(2)));
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);
        timePicker.clearFocus();
        if(positiveResult){
            String value = String.format("%02d%02d", timePicker.getCurrentHour(), timePicker.getCurrentMinute());
            if(callChangeListener(value)) {
                persistString(value);
            }
        }
    }
}
