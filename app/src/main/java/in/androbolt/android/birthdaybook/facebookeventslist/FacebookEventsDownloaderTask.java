package in.androbolt.android.birthdaybook.facebookeventslist;

import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.util.List;

import in.androbolt.android.birthdaybook.common.Callback;
import in.androbolt.android.birthdaybook.common.models.Result;
import in.androbolt.android.birthdaybook.events.FacebookICalHelper;
import in.androbolt.android.birthdaybook.events.models.FacebookEvent;

public class FacebookEventsDownloaderTask
        extends AsyncTask<Void, Void, Result<List<FacebookEvent>>> {

    private static final String TAG = "FBEventsDownloaderTask";
    private FacebookICalHelper facebookICalHelper;
    private Callback<List<FacebookEvent>> callback;

    public FacebookEventsDownloaderTask(FacebookICalHelper facebookICalHelper) {
        this.facebookICalHelper = facebookICalHelper;
    }

    public FacebookEventsDownloaderTask withCallback(Callback<List<FacebookEvent>> callback) {
        this.callback = callback;
        return this;
    }

    @Override
    protected Result<List<FacebookEvent>> doInBackground(Void... args) {
        Result<List<FacebookEvent>> result;
        try {
            result = this.facebookICalHelper.fetchEvents();
        } catch (IOException e) {
            Log.e(TAG, e.getMessage(), e);
            result = new Result<>(null, e.getMessage(), Result.Type.FAILURE);
        }
        return result;
    }

    @Override
    protected void onPostExecute(Result<List<FacebookEvent>> result) {
        if (this.callback != null) {
            this.callback.onFinish(result);
        }
    }
}
