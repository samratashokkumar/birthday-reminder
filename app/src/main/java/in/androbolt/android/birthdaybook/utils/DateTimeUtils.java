package in.androbolt.android.birthdaybook.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

import in.androbolt.android.birthdaybook.common.models.ParsedDate;

public class DateTimeUtils {

    private static final Pattern LONG_DATE = Pattern.compile("\\d+");
    private static final SimpleDateFormat ISO_FORMAT =
            new SimpleDateFormat("yyyy-MM-dd", Locale.US);
    private static final SimpleDateFormat ISO_FORMAT_DEFAULT_LOCALE =
            new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    private static final SimpleDateFormat[] NO_YEAR_DATE_FORMATS = {
            new SimpleDateFormat("MM-dd", Locale.US),
            new SimpleDateFormat("-MM-dd", Locale.US),
            new SimpleDateFormat("--MM-dd", Locale.US),
            new SimpleDateFormat("MM.dd", Locale.US),
            new SimpleDateFormat(".MM.dd", Locale.US),
            new SimpleDateFormat("MM/dd", Locale.US),
            new SimpleDateFormat("/MM/dd", Locale.US),
    };
    private static final SimpleDateFormat[] DATE_FORMATS = {
            new SimpleDateFormat("yyyy-MM-dd", Locale.US),
            new SimpleDateFormat("yyyy.MM.dd", Locale.US),
            new SimpleDateFormat("yyyy/MM/dd", Locale.US),
    };
    private static final Map<String, SimpleDateFormat> NO_YEAR_MAP = new HashMap<>();

    static {
        for (DateFormat dateFormat : NO_YEAR_DATE_FORMATS) {
            dateFormat.setLenient(false);
        }
        for (DateFormat dateFormat : DATE_FORMATS) {
            dateFormat.setLenient(false);
        }
        NO_YEAR_MAP.put("dd MMM, yyyy", new SimpleDateFormat("dd MMM", Locale.getDefault()));
        NO_YEAR_MAP.put("MMM dd, yyyy", new SimpleDateFormat("MMM dd", Locale.getDefault()));
        NO_YEAR_MAP.put("dd/MM/yyyy", new SimpleDateFormat("dd/MM", Locale.getDefault()));
        NO_YEAR_MAP.put("MM/dd/yyyy", new SimpleDateFormat("MM/dd", Locale.getDefault()));
        NO_YEAR_MAP.put("yyyy/MM/dd", new SimpleDateFormat("MM/dd", Locale.getDefault()));
    }

    private DateTimeUtils() {

    }

    public static ParsedDate parseEventDate(String eventDate) {
        if (LONG_DATE.matcher(eventDate).matches()) {
            return new ParsedDate(ISO_FORMAT.format(new Date(Long.parseLong(eventDate))), true);
        }
        String formattedDate = getFormattedDate(eventDate, DATE_FORMATS, false);
        if (formattedDate != null) {
            return new ParsedDate(formattedDate, true);
        }
        formattedDate = getFormattedDate(eventDate, NO_YEAR_DATE_FORMATS, true);
        if (formattedDate != null) {
            return new ParsedDate(formattedDate, false);
        }
        return new ParsedDate(eventDate, false);
    }

    private static String getFormattedDate(String eventDate,
                                           SimpleDateFormat[] formats,
                                           boolean isNoYearFormat) {
        for (DateFormat dateFormat: formats) {
            try {
                Date date = dateFormat.parse(eventDate);
                if (isNoYearFormat) {
                    date = updateYear(date);
                }
                return ISO_FORMAT.format(date);
            } catch (ParseException e) {
                // Ignore exception and try with another format
            }
        }
        return null;
    }

    private static Date updateYear(Date date) {
        Calendar currentCal = Calendar.getInstance();
        currentCal.set(Calendar.HOUR, 0);
        currentCal.set(Calendar.MINUTE, 0);
        currentCal.set(Calendar.SECOND, 0);
        currentCal.set(Calendar.MILLISECOND, 0);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.YEAR, currentCal.get(Calendar.YEAR));
        if (cal.compareTo(currentCal) < 0) {
            cal.add(Calendar.YEAR, 1);
        }
        return cal.getTime();
    }

    public static String addCurrentYear(String date) {
        try {
            Date eventDate = ISO_FORMAT.parse(date);
            return ISO_FORMAT.format(updateYear(eventDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String getFormattedDate(String date, DateFormat dateFormat) {
        try {
            Date eventDate = ISO_FORMAT_DEFAULT_LOCALE.parse(date);
            return dateFormat.format(eventDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String getISODate(Calendar cal) {
        return ISO_FORMAT.format(cal.getTime());
    }

    public static DateFormat getNoYearFormat(SimpleDateFormat dateFormat) {
        return NO_YEAR_MAP.get(dateFormat.toPattern());
    }
}
