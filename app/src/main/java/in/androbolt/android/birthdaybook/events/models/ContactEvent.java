package in.androbolt.android.birthdaybook.events.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Event which is taken from the phone contact
 */
public class ContactEvent extends Event {

    public static final String SOURCE = "contact";
    public static final long ID_NA = -1;

    private long contactId;
    private long rawContactId;

    public ContactEvent(long eventId, long contactId, long rawContactId, String displayName){
        super(eventId, displayName);
        this.contactId = contactId;
        this.rawContactId = rawContactId;
    }

    public ContactEvent(long eventId, long contactId, long rawContactId, String displayName, String
            eventDate, int eventType) {
        super(eventId, displayName, eventDate, eventType);
        this.contactId = contactId;
        this.rawContactId = rawContactId;
    }

    public long getContactId() {
        return contactId;
    }

    public long getRawContactId() {
        return rawContactId;
    }

    @Override
    public String getEventSource() {
        return SOURCE;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(getId());
        dest.writeString(getDisplayName());
        dest.writeString(getEventDate());
        dest.writeInt(getEventType());
        dest.writeLong(contactId);
        dest.writeLong(rawContactId);
    }

    public static final Parcelable.Creator<ContactEvent> CREATOR
            = new Parcelable.Creator<ContactEvent>() {
        @Override
        public ContactEvent createFromParcel(Parcel source) {
            long eventId = source.readLong();
            String displayName = source.readString();
            String eventDate = source.readString();
            int eventType = source.readInt();
            long contactId = source.readLong();
            long rawContactId = source.readLong();
            return new ContactEvent(eventId,
                    contactId,
                    rawContactId,
                    displayName,
                    eventDate,
                    eventType);
        }

        @Override
        public ContactEvent[] newArray(int size) {
            return new ContactEvent[size];
        }
    };
}
