package in.androbolt.android.birthdaybook.eventslist;

import android.view.View;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import in.androbolt.android.birthdaybook.R;
import in.androbolt.android.birthdaybook.common.Comparators;
import in.androbolt.android.birthdaybook.events.EventsRepository;
import in.androbolt.android.birthdaybook.events.models.Event;
import in.androbolt.android.birthdaybook.preferences.AppPreferences;
import in.androbolt.android.birthdaybook.utils.DateTimeUtils;

/**
 * Events List Presenter
 */
public class EventsListPresenter implements EventsListContract.ActionListener {

    private final AppPreferences appPreferences;
    private final EventsRepository eventsRepository;
    private final EventsListContract.View eventsListView;

    public  EventsListPresenter(AppPreferences appPreferences, EventsRepository eventsRepository,
                                EventsListContract.View eventsListView) {
        this.appPreferences = appPreferences;
        this.eventsRepository = eventsRepository;
        this.eventsListView = eventsListView;
    }

    @Override
    public void loadEvents() {
        DateFormat preferredDateFormat = appPreferences.getPreferredDateFormat();
        List<Event> events = eventsRepository.getEvents();
        Collections.sort(events, Comparators.DATE_COMPARATOR);
        eventsListView.showEvents(events, preferredDateFormat);
    }

    @Override
    public void openEventDetails(Event event) {

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        eventsListView.applyFilter(newText);
        return true;
    }
}
