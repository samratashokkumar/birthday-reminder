package in.androbolt.android.birthdaybook.common.models;

public class Result<T> {

    T data;
    String message;
    Type type;

    public Result(T data, String message, Type type) {
        this.data = data;
        this.message = message;
        this.type = type;
    }

    public T getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }

    public Type getType() {
        return type;
    }

    public enum Type {
        SUCCESS,
        FAILURE
    }
}
