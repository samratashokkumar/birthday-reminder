package in.androbolt.android.birthdaybook.alarms;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import java.util.Calendar;

import in.androbolt.android.birthdaybook.appwidget.AppWidget;
import in.androbolt.android.birthdaybook.notifications.FutureEventsNotificationReceiver;
import in.androbolt.android.birthdaybook.notifications.TodaysEventsNotificationReceiver;

/**
 * App Alarms
 */
public class AppAlarms {

    public static final String DEFAULT_TIME = "0600";
    private Context context;
    private AlarmManager alarmManager;

    public AppAlarms(Context context) {
        this.context = context;
        this.alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
    }

    public void setTodaysEventsNotificationAlarm(String notificationTime) {
        long timeInMillis = getAlarmTime(notificationTime).getTimeInMillis();
        Intent intent = new Intent(TodaysEventsNotificationReceiver.INTENT_ACTION);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            alarmManager.setExactAndAllowWhileIdle(
                    AlarmManager.RTC_WAKEUP, timeInMillis, pendingIntent);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, timeInMillis, pendingIntent);
        } else {
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, timeInMillis, AlarmManager
                    .INTERVAL_DAY, pendingIntent);
        }
    }

    public void cancelTodaysEventsNotificationAlarm() {
        Intent intent = new Intent(TodaysEventsNotificationReceiver.INTENT_ACTION);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
        alarmManager.cancel(pendingIntent);
    }

    public void setFutureEventsNotificationAlarm(String notificationTime) {
        long timeInMillis = getAlarmTime(notificationTime).getTimeInMillis();
        Intent intent = new Intent(FutureEventsNotificationReceiver.INTENT_ACTION);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            alarmManager.setExactAndAllowWhileIdle(
                    AlarmManager.RTC_WAKEUP, timeInMillis, pendingIntent);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, timeInMillis, pendingIntent);
        }else {
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, timeInMillis, AlarmManager
                    .INTERVAL_DAY, pendingIntent);
        }
    }

    public void cancelFutureEventsNotificationAlarm() {
        Intent intent = new Intent(FutureEventsNotificationReceiver.INTENT_ACTION);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
        alarmManager.cancel(pendingIntent);
    }

    public void setAppWidgetUpdateAlarm() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.add(Calendar.DATE, 1);

        Intent intent = new Intent(AppWidget.UPDATE_ACTION);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
        alarmManager.setInexactRepeating(AlarmManager.RTC, calendar.getTimeInMillis(),
                AlarmManager.INTERVAL_DAY, pendingIntent);
    }

    public void cancelAppWidgetUpdateAlarm() {
        Intent intent = new Intent(AppWidget.UPDATE_ACTION);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
        alarmManager.cancel(pendingIntent);
    }

    public static Calendar getAlarmTime(String notificationTime) {
        int hour = Integer.valueOf(notificationTime.substring(0, 2));
        int minute = Integer.valueOf(notificationTime.substring(2));
        Calendar alarmTime = Calendar.getInstance();
        alarmTime.set(Calendar.HOUR_OF_DAY, hour);
        alarmTime.set(Calendar.MINUTE, minute);
        alarmTime.set(Calendar.SECOND, 0);
        alarmTime.set(Calendar.MILLISECOND, 0);
        Calendar currentTime = Calendar.getInstance();
        alarmTime.add(Calendar.DATE, currentTime.compareTo(alarmTime) == -1 ? 0 : 1);
        return alarmTime;
    }
}
