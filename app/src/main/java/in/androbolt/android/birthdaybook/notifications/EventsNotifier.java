package in.androbolt.android.birthdaybook.notifications;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import in.androbolt.android.birthdaybook.R;
import in.androbolt.android.birthdaybook.mainactivity.MainActivity;
import in.androbolt.android.birthdaybook.events.EventsRepository;
import in.androbolt.android.birthdaybook.events.models.Event;
import in.androbolt.android.birthdaybook.preferences.AppPreferences;

/**
 * Events Notifier
 */
public class EventsNotifier {

    private static final Comparator<String> NAME_COMPARATOR = new Comparator<String>() {
        @Override
        public int compare(String lhs, String rhs) {
            return lhs.compareTo(rhs);
        }
    };
    private static final int MAX_NAME_COUNT = 2;

    private final Context context;
    private final AppPreferences appPreferences;
    private final EventsRepository eventsRepository;

    public EventsNotifier(Context context, AppPreferences appPreferences, EventsRepository
            eventsRepository) {
        this.context = context;
        this.appPreferences = appPreferences;
        this.eventsRepository = eventsRepository;
    }

    public void showTodaysEventsNotification() {
        String eventDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance()
                .getTime());
        CharSequence birthdayNotificationText = "";
        CharSequence anniversaryNotificationText = "";
        birthdayNotificationText = this.context.getText(R.string.todays_birthday);
        anniversaryNotificationText = this.context.getText(R.string.todays_anniversary);
        notifyEvent(birthdayNotificationText, Event.TYPE_BIRTHDAY, eventDate, 11);
        notifyEvent(anniversaryNotificationText, Event.TYPE_ANNIVERSARY, eventDate, 12);
    }

    public void showFutureEventsNotification() {
        CharSequence birthdayNotificationText = "";
        CharSequence anniversaryNotificationText = "";
        Calendar cal = Calendar.getInstance();
        int notifyBeforeDays = appPreferences.getNotifyBefore();
        cal.add(Calendar.DATE, notifyBeforeDays);
        String eventDate = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
        if (notifyBeforeDays == 1) {
            birthdayNotificationText = context.getText(R.string.tomorrows_birthday);
            anniversaryNotificationText = context.getText(R.string.tomorrows_anniversary);
        } else {
            String inDays = constructInDaysText(notifyBeforeDays);
            birthdayNotificationText = String.format(context.getText(R.string.birthdays_in)
                    .toString(), inDays);
            anniversaryNotificationText = String.format(context.getText(R.string
                    .anniversaries_in).toString(), inDays);
        }
        notifyEvent(birthdayNotificationText, Event.TYPE_BIRTHDAY, eventDate, 13);
        notifyEvent(anniversaryNotificationText, Event.TYPE_ANNIVERSARY, eventDate, 14);
    }

    private String constructInDaysText(int notifyBeforeDays) {
        String inDays = "";
        String[] notifyBeforeList
                = context.getResources().getStringArray(R.array.notifybefore_list_entry);
        String[] notifyBeforeValueList
                = context.getResources()
                .getStringArray(R.array.notifybefore_list_entry_values);
        for (int i = 0; i < notifyBeforeValueList.length ; i++) {
            if (notifyBeforeDays == Integer.valueOf(notifyBeforeValueList[i])) {
                inDays = notifyBeforeList[i].toLowerCase();
                break;
            }
        }
        return inDays;
    }

    private void notifyEvent(CharSequence notificationText, int eventType, String eventDate, int
            id) {
        List<String> names = eventsRepository.getEventDateNames(eventType, eventDate);
        Collections.sort(names, NAME_COMPARATOR);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService
                (Context.NOTIFICATION_SERVICE);
        if (names.size() > 0) {
            Notification notification = buildNotification(notificationText, eventType, names);
            notificationManager.notify(id, notification);
        } else {
            notificationManager.cancel(id);
        }
    }

    private Notification buildNotification(CharSequence notificationTitleText, int eventType,
                                           List<String> names) {
        int count = names.size();
        StringBuilder namesContent = buildNamesContent(names, count);
        boolean isStickyNotification = appPreferences.isStickyNotificationEnabled();
        int notificationIcon = eventType == Event.TYPE_BIRTHDAY ?
                R.drawable.ic_stat_notify_birthday : R.drawable.ic_stat_notify_anniversary;
        //TODO: Set intent flag for starting the activity
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, new Intent
                (context, MainActivity.class), 0);

        //TODO: Revisit sticky notification
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(notificationIcon)
                .setContentTitle(notificationTitleText)
                .setContentText(namesContent.toString().trim())
                .setNumber(count)
                .setAutoCancel(true)
                .setOngoing(isStickyNotification)
                .setContentIntent(pendingIntent);
        if (appPreferences.isNotificationSoundEnabled()) {
            String notificationTone = appPreferences.getNotificationTone();
            if (!notificationTone.isEmpty()) {
                notificationBuilder.setSound(Uri.parse(notificationTone));
            } else {
                notificationBuilder.setDefaults(Notification.DEFAULT_SOUND);
            }
        }
        return notificationBuilder.build();
    }

    private StringBuilder buildNamesContent(List<String> names, int count) {
        StringBuilder namesContent = new StringBuilder();
        for (int i = 0; i < count && i < MAX_NAME_COUNT; i++) {
            namesContent.append(names.get(i)).append(", ");
        }
        namesContent.delete(namesContent.length() - 2, namesContent.length() - 1);
        if (count > MAX_NAME_COUNT) {
            int remaining = count - MAX_NAME_COUNT;
            namesContent.append(String.format(context.getText(R.string.and_more)
                    .toString(), remaining));
        }
        return namesContent;
    }
}
