package in.androbolt.android.birthdaybook.common;

import java.util.Comparator;

import in.androbolt.android.birthdaybook.events.models.Event;
import in.androbolt.android.birthdaybook.utils.DateTimeUtils;

/**
 * Created by ashok on 29/3/16.
 */
public class Comparators {
    private Comparators() {

    }

    public static final Comparator<Event> DATE_COMPARATOR = new Comparator<Event>() {

        @Override
        public int compare(Event lhs, Event rhs) {
            if (lhs.isError()) {
                return -1;
            } else if (rhs.isError()) {
                return 1;
            }
            String currentEventDateLHS = DateTimeUtils.addCurrentYear(lhs.getParsedDate());
            String currentEventDateRHS = DateTimeUtils.addCurrentYear(rhs.getParsedDate());
            int dateComparedResult = currentEventDateLHS.compareTo(currentEventDateRHS);
            if (dateComparedResult == 0) {
                return lhs.getDisplayName().compareTo(rhs.getDisplayName());
            } else {
                return dateComparedResult;
            }
        }
    };
}
