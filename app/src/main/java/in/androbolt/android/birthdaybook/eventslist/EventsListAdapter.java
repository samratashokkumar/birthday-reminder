package in.androbolt.android.birthdaybook.eventslist;

import android.content.ContentUris;
import android.content.Context;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.androbolt.android.birthdaybook.R;
import in.androbolt.android.birthdaybook.adapters.RecyclerViewListAdapter;
import in.androbolt.android.birthdaybook.events.models.ContactEvent;
import in.androbolt.android.birthdaybook.events.models.Event;
import in.androbolt.android.birthdaybook.events.models.FacebookEvent;
import in.androbolt.android.birthdaybook.utils.DateTimeUtils;

public class EventsListAdapter extends RecyclerViewListAdapter {

    @SuppressWarnings("unused")
    private static final String TAG = "EventsListAdapter";
    public static final int TYPE_HEADER = 0;
    public static final int TYPE_ITEM = 1;

    private List<Event> events;
    private List<RowItem> rowItems;
    private Map<Integer, Pair<Integer, String>> headersMap;
    private java.text.DateFormat dateFormat;

    public EventsListAdapter(List<Event> events, java.text.DateFormat dateFormat) {
        this.events = events;
        this.dateFormat = dateFormat;
        this.rowItems = new ArrayList<>();
        this.headersMap = new HashMap<>();
        try {
            prepareRowItems();
        } catch (ParseException e) {
            Log.e(TAG, e.getMessage());
        }
    }

    @Override
    public List<Event> getItems() {
        return events;
    }

    @Override
    public void setItems(List<Event> events) {
        this.events = events;
        try {
            prepareRowItems();
        } catch (ParseException e) {
            Log.e(TAG, e.getMessage());
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return rowItems.get(position).type;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        if (viewType == TYPE_ITEM) {
            View v = layoutInflater.inflate(R.layout.event_list_item, parent, false);
            return new EventsListViewHolder(v);
        } else {
            View v = layoutInflater.inflate(R.layout.event_list_header, parent, false);
            return new EventsListHeaderViewHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (rowItems.get(position).type == TYPE_ITEM) {
            showItem((EventsListViewHolder) holder, position);
        } else {
            showHeader((EventsListHeaderViewHolder) holder, position);
        }
    }

    public Pair<Integer, String> getHeaderInfo(int position) {
        return headersMap.get(position);
    }

    private void showHeader(EventsListHeaderViewHolder holder, int position) {
        holder.tvHeader.setText(rowItems.get(position).header);
    }

    private void showItem(EventsListViewHolder holder, int position) {
        Event event = rowItems.get(position).event;
        EventsListViewHolder eventsListViewHolder = holder;
        eventsListViewHolder.tvName.setText(event.getDisplayName());
        ImageView ivPhoto = eventsListViewHolder.ivPhoto;
        loadPhoto(event, ivPhoto);
        if (!event.isError()) {
            eventsListViewHolder.tvEventDate.setText(DateTimeUtils
                    .getFormattedDate(event.getParsedDate(), dateFormat));
        } else {
            eventsListViewHolder.tvEventDate.setText(String.format("Parse Error: " +
                    "%s", event.getEventDate()));
        }
    }

    @Override
    public int getItemCount() {
        return rowItems.size();
    }

    public void setDateFormat(java.text.DateFormat dateFormat) {
        this.dateFormat = dateFormat;
        notifyDataSetChanged();
    }

    private void loadPhoto(Event event, ImageView ivPhoto) {
        if (event.getEventSource().equals(FacebookEvent.SOURCE)) {
            Picasso.with(ivPhoto.getContext()).load(((FacebookEvent) event).getPicturePath())
                    .into(ivPhoto);
        } else if (event.getEventSource().equals(ContactEvent.SOURCE)) {
            Picasso.with(ivPhoto.getContext())
                    .load(Uri.withAppendedPath(
                            ContentUris.withAppendedId(
                                    ContactsContract.Contacts.CONTENT_URI,
                                    ((ContactEvent) event)
                                            .getContactId()),
                            ContactsContract.Contacts.Photo.CONTENT_DIRECTORY))
                    .placeholder(R.drawable.ic_contact_list_picture)
                    .into(ivPhoto);
        }
    }

    private void prepareRowItems() throws ParseException {
        headersMap.clear();
        rowItems.clear();
        String currentMonth = "";
        for (Event event: events) {
            if (!event.isError()) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(event.getParsedDate()));
                String eventMonth = new SimpleDateFormat("MMMM").format(cal.getTime());
                if (!currentMonth.equals(eventMonth)) {
                    headersMap.put(rowItems.size(), new Pair<>(TYPE_HEADER, eventMonth));
                    rowItems.add(new RowItem(eventMonth));
                    currentMonth = eventMonth;
                }
                headersMap.put(rowItems.size(), new Pair<>(TYPE_ITEM, eventMonth));
                rowItems.add(new RowItem(event));
            }
        }
    }

    private static class EventsListViewHolder extends RecyclerView.ViewHolder {
        public ImageView ivPhoto;
        public TextView tvName;
        public TextView tvEventDate;

        public EventsListViewHolder(View vwListItem) {
            super(vwListItem);
            ivPhoto = (ImageView) vwListItem.findViewById(R.id.ivPhoto);
            tvName = (TextView) vwListItem.findViewById(R.id.tvName);
            tvEventDate = (TextView) vwListItem.findViewById(R.id.tvEventDate);
        }
    }

    private static class EventsListHeaderViewHolder extends RecyclerView.ViewHolder {

        public TextView tvHeader;

        public EventsListHeaderViewHolder(View itemView) {
            super(itemView);
            tvHeader = (TextView) itemView.findViewById(R.id.tvHeader);
        }
    }

    private static class RowItem {

        public Event event;
        public String header;
        public int type;

        public RowItem(Event event) {
            this.event = event;
            this.header = null;
            this.type = TYPE_ITEM;
        }

        public RowItem(String header) {
            this.event = null;
            this.header = header;
            this.type = TYPE_HEADER;
        }
    }
}