package in.androbolt.android.birthdaybook.dialogs;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.Locale;

import in.androbolt.android.birthdaybook.R;
import in.androbolt.android.birthdaybook.common.datepicker.DatePicker;
import in.androbolt.android.birthdaybook.events.models.ContactEvent;
import in.androbolt.android.birthdaybook.events.models.Event;

/**
 * Add Contact Event Dialog
 */
public class AddContactEventDialog extends DialogFragment {
    @SuppressWarnings("unused")
    private static final String TAG = "AddContactEventDialog";

    public static final String EVENT_TO_BE_ADDED = "event_to_be_added";
    public static final String CONTACT_EVENT = "contact_event";

    private OnDateSetListener onDateSetListener;
    private ContactEvent contactEvent;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        contactEvent = getArguments().getParcelable(CONTACT_EVENT);
        final int eventToBeAdded = getArguments().getInt(EVENT_TO_BE_ADDED);
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_select_event_date, null);
        TextView tvDisplayName = (TextView) view.findViewById(R.id.tvDisplayName);
        tvDisplayName.setText(contactEvent.getDisplayName());
        final RadioButton rbBirthday = (RadioButton) view.findViewById(R.id.rbBirthday);
        RadioButton rbAnniversary = (RadioButton) view.findViewById(R.id.rbAnniversary);
        final DatePicker datePicker = (DatePicker) view.findViewById(R.id.datePicker);
        if (eventToBeAdded == Event.TYPE_BIRTHDAY) {
            rbAnniversary.setEnabled(false);
        }
        if (eventToBeAdded == Event.TYPE_ANNIVERSARY) {
            rbBirthday.setEnabled(false);
            rbAnniversary.setChecked(true);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setView(view)
                .setPositiveButton(getActivity().getString(R.string.set), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (onDateSetListener != null) {
                            datePicker.clearFocus();
                            String eventDate = "";
                            int eventType =
                                    rbBirthday.isChecked() ?
                                            Event.TYPE_BIRTHDAY : Event.TYPE_ANNIVERSARY;
                            if (datePicker.getYear() != DatePicker.NO_YEAR) {
                                eventDate = String
                                        .format( Locale.getDefault(),
                                                "%04d-%02d-%02d",
                                                datePicker.getYear(),
                                                datePicker.getMonth() + 1,
                                                datePicker.getDayOfMonth());
                            } else {
                                eventDate = String
                                        .format(Locale.getDefault(), "%02d-%02d",
                                                datePicker.getMonth() + 1,
                                                datePicker.getDayOfMonth());
                            }
                            contactEvent.setEventDate(eventDate);
                            contactEvent.setEventType(eventType);
                            if (onDateSetListener != null) {
                                onDateSetListener
                                        .onDateSet(contactEvent, eventType == eventToBeAdded);
                            }
                        }
                    }
                })
                .setNegativeButton(getActivity().getString(R.string.cancel), null);
        return builder.create();
    }

    public void setOnDateSetListener(OnDateSetListener onDateSetListener) {
        this.onDateSetListener = onDateSetListener;
    }

    public interface OnDateSetListener {
        void onDateSet(ContactEvent contactEvent, boolean isLastEvent);
    }
}
