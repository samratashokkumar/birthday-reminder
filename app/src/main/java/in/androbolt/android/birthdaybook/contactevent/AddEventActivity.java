package in.androbolt.android.birthdaybook.contactevent;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.Filter;

import java.util.List;

import in.androbolt.android.birthdaybook.R;
import in.androbolt.android.birthdaybook.base.BaseActivity;
import in.androbolt.android.birthdaybook.base.PresenterFactory;
import in.androbolt.android.birthdaybook.events.contacts.ContactEventsHelper;
import in.androbolt.android.birthdaybook.dialogs.AddContactEventDialog;
import in.androbolt.android.birthdaybook.events.models.Event;

/**
 * Add Event Activity
 */
public class AddEventActivity extends BaseActivity<AddEventContract.ActionListener,
        AddEventContract.View> implements AddEventContract.View {
    @SuppressWarnings("unused")
    private static final String TAG = "AddEventActivity";

    private RecyclerView recyclerView;
    private ContactListAdapter contactListAdapter;
    private Filter filter;
    private AddEventContract.ActionListener actionListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addevent);
        Toolbar appToolbar = (Toolbar) findViewById(R.id.appToolbar);
        setSupportActionBar(appToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        recyclerView = (RecyclerView) findViewById(R.id.rvContactsList);
    }

    @Override
    public void showContacts(List<Event> contactsWithoutEventsList) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        contactListAdapter = new ContactListAdapter(contactsWithoutEventsList);
        contactListAdapter.setOnItemClickListener(actionListener);
        recyclerView.setAdapter(contactListAdapter);
        filter = contactListAdapter.getFilter();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_addevent, menu);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setQueryHint(getText(R.string.search_friend));
        searchView.setOnQueryTextListener(actionListener);
        searchView.setIconifiedByDefault(true);
        searchView.setMaxWidth(Integer.MAX_VALUE);
        return true;
    }

    @Override
    public void showAddContactEventDialog(Event event, int position, int eventToBeAdded) {
        AddContactEventDialog addContactEventDialog
                = new AddContactEventDialog();
        addContactEventDialog.setOnDateSetListener(actionListener);
        Bundle bundle = new Bundle();
        bundle.putParcelable(AddContactEventDialog.CONTACT_EVENT, event);
        bundle.putInt(AddContactEventDialog.EVENT_TO_BE_ADDED, eventToBeAdded);
        addContactEventDialog.setArguments(bundle);
        addContactEventDialog.show(getFragmentManager(), "dialog_select_event_date");
    }


    @Override
    public void removeFromList(int selectedPosition) {
        contactListAdapter.removeContact(selectedPosition);
    }

    @Override
    public void applyFilter(String searchString) {
        filter.filter(searchString, actionListener);
    }

    @Override
    public List<Event> getItems() {
        return contactListAdapter.getItems();
    }

    @Override
    protected PresenterFactory<AddEventContract.ActionListener> getPresenterFactory() {
        return new PresenterFactory<AddEventContract.ActionListener>() {
            @Override
            public AddEventContract.ActionListener create() {
                return new AddEventPresenter(new ContactEventsHelper(AddEventActivity.this));
            }
        };
    }

    @Override
    protected void onPresenterPrepared(AddEventContract.ActionListener actionListener) {
        this.actionListener = actionListener;
    }
}
