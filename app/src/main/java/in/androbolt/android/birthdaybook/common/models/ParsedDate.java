package in.androbolt.android.birthdaybook.common.models;

public class ParsedDate {
    public String date;
    public boolean hasYear;

    public ParsedDate(String date, boolean hasYear) {
        this.date = date;
        this.hasYear = hasYear;
    }
}
