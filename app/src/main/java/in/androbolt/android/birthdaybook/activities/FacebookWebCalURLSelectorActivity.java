package in.androbolt.android.birthdaybook.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;

import in.androbolt.android.birthdaybook.FacebookWebViewClient;
import in.androbolt.android.birthdaybook.R;

/**
 * Created by ashok on 2/6/15.
 */
public class FacebookWebCalURLSelectorActivity extends AppCompatActivity
        implements FacebookWebViewClient.WebCalURLListener {

    public static final String WEBCAL_URL = "webcal_url";
    private WebView wvFacebookBirthdays;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facebookwebcalurlselector);
        FacebookWebViewClient facebookWebViewClient = new FacebookWebViewClient();
        facebookWebViewClient.setWebCalURLListener(this);
        wvFacebookBirthdays = (WebView) findViewById(R.id.wvFacebookBirthdays);
        wvFacebookBirthdays.getSettings().setJavaScriptEnabled(true);
        wvFacebookBirthdays.getSettings()
                .setUserAgentString("Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 " +
                        "(KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36");
        wvFacebookBirthdays.setWebViewClient(facebookWebViewClient);
        wvFacebookBirthdays.loadUrl("https://www.facebook.com/events/birthdays");
    }

    @Override
    public void foundWebCalURL(String url) {
        setResult(RESULT_OK, new Intent().putExtra(WEBCAL_URL, url));
        finish();
    }
}
