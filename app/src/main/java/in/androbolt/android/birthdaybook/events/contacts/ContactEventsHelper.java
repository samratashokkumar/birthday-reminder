package in.androbolt.android.birthdaybook.events.contacts;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

import java.util.ArrayList;
import java.util.List;

import in.androbolt.android.birthdaybook.common.models.ParsedDate;
import in.androbolt.android.birthdaybook.events.models.ContactEvent;
import in.androbolt.android.birthdaybook.events.models.Event;
import in.androbolt.android.birthdaybook.utils.DateTimeUtils;

/**
 * Helper Class for Contact Events
 */
public class ContactEventsHelper {

    private ContentResolver contentResolver;

    public ContactEventsHelper(Context context) {
        this.contentResolver = context.getContentResolver();
    }

    public Uri addEvent(ContactEvent event) {
        ContentValues cv = new ContentValues();
        cv.put(ContactsContract.Data.MIMETYPE,
                ContactsContract.CommonDataKinds.Event.CONTENT_ITEM_TYPE);
        /**
         * TODO: Pass Raw Contact ID as parameter after getting confirmation
         * from the user in which account they want to save the event
         */
        cv.put(ContactsContract.CommonDataKinds.Event.RAW_CONTACT_ID,
                getRawContactID(event.getContactId()).get(0));
        cv.put(ContactsContract.CommonDataKinds.Event.START_DATE, event.getEventDate());
        cv.put(ContactsContract.CommonDataKinds.Event.TYPE, event.getEventType());
        return contentResolver.insert(ContactsContract.Data.CONTENT_URI, cv);
    }

    public List<Event> getEvents() {
        Cursor cursor = getEventsCursor();
        return getEventsList(cursor);
    }

    public List<Event> getEvents(long contactId){
        Cursor cursor = getEventsCursor(contactId);
        return getEventsList(cursor);
    }

    public List<String> getNames(int eventType, String eventDate) {
        List<String> names = new ArrayList<>();
        Cursor cursor = getEventsCursor(eventType);
        while (cursor.moveToNext()) {
            String displayName = cursor.getString(
                    cursor.getColumnIndex(
                            ContactsContract.CommonDataKinds.Event.DISPLAY_NAME));
            String cursorEventDate = cursor.getString(
                    cursor.getColumnIndex(
                            ContactsContract.CommonDataKinds.Event.START_DATE));
            ParsedDate parsedDate = DateTimeUtils.parseEventDate(cursorEventDate);
            cursorEventDate = parsedDate.date;
            if (cursorEventDate.matches(Event.DATE_PATTERN)
                    && eventDate.equals(DateTimeUtils.addCurrentYear(cursorEventDate))) {
                names.add(displayName);
            }
        }
        cursor.close();
        return names;
    }

    public List<Event> getEventsToBeAddedContacts() {
        List<Event> eventList = new ArrayList<>();
        Cursor allContacts = getContactsCursor();
        Cursor contactsWithEvents = getEventsCursor();

        contactsWithEvents.moveToFirst();

        while (allContacts.moveToNext()) {
            String displayName = allContacts.getString(allContacts
                    .getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
            long contactId = allContacts.getLong(allContacts
                    .getColumnIndex(ContactsContract.Contacts._ID));

            if (!contactsWithEvents.isAfterLast()) {
                long eventContactId = contactsWithEvents
                        .getLong(contactsWithEvents
                                .getColumnIndex(ContactsContract.Data.CONTACT_ID));
                int eventType = contactsWithEvents.getInt(contactsWithEvents
                        .getColumnIndex(ContactsContract.CommonDataKinds.Event.TYPE));
                boolean differentEvent = false;
                if (contactId == eventContactId) {
                    long nextEventContactId;
                    int nextEventType;
                    do {
                        contactsWithEvents.moveToNext();
                        if (contactsWithEvents.isAfterLast()) {
                            break;
                        }
                        nextEventContactId = contactsWithEvents
                                .getLong(contactsWithEvents
                                        .getColumnIndex(ContactsContract.Data.CONTACT_ID));
                        nextEventType = contactsWithEvents
                                .getInt(contactsWithEvents
                                        .getColumnIndex(
                                                ContactsContract.CommonDataKinds.Event.TYPE));
                        if ((eventContactId == nextEventContactId)
                                && !differentEvent) {
                            if (eventType != nextEventType
                                    && (eventType == Event.TYPE_BIRTHDAY ||
                                    eventType == Event.TYPE_ANNIVERSARY)
                                    && (nextEventType == Event.TYPE_BIRTHDAY ||
                                    nextEventType == Event.TYPE_ANNIVERSARY))
                                differentEvent = true;
                        }
                    } while (eventContactId == nextEventContactId);
                    if (differentEvent) {
                        continue;
                    }
                }
            }
            eventList.add(new ContactEvent(ContactEvent.ID_NA, contactId, ContactEvent.ID_NA,
                    displayName));
        }
        allContacts.close();
        contactsWithEvents.close();
        return eventList;
    }

    public String getContactPhoneNumber(long contactId) {
        String phone = null;
        Cursor phonesCursor = getContactPhoneNoCursor(contactId);
        if (phonesCursor == null || phonesCursor.getCount() == 0) {
            return "";
        } else if (phonesCursor.getCount() == 1) {
            phonesCursor.moveToFirst();
            phone = phonesCursor.getString(phonesCursor
                    .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
        } else {
            phonesCursor.moveToPosition(-1);
            while (phonesCursor.moveToNext()) {
                if (phonesCursor.getInt(phonesCursor
                        .getColumnIndex(
                                ContactsContract.CommonDataKinds.Phone.IS_SUPER_PRIMARY)) != 0) {
                    phone = phonesCursor.getString(phonesCursor
                            .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    break;
                }
            }
        }
        return phone;
    }

    public int updateEventDate(ContactEvent event) {
        ContentValues cv = new ContentValues();
        cv.put(ContactsContract.CommonDataKinds.Event.START_DATE, event.getEventDate());
        return contentResolver.update(ContactsContract.Data.CONTENT_URI, cv, ContactsContract
                .CommonDataKinds.Event._ID + "=? AND " +
                ContactsContract.CommonDataKinds.Event.TYPE
                + "=? AND " + ContactsContract.Data.MIMETYPE + "=?", new String[] {String.valueOf
                (event.getId()), String.valueOf(event.getEventType()), ContactsContract
                .CommonDataKinds.Event.CONTENT_ITEM_TYPE});
    }

    private List<Event> getEventsList(Cursor cursor) {
        List<Event> events = new ArrayList<>();
        while (cursor.moveToNext()) {
            String displayName = cursor.getString(
                    cursor.getColumnIndex(
                            ContactsContract.CommonDataKinds.Event.DISPLAY_NAME));
            String eventDate = cursor.getString(
                    cursor.getColumnIndex(
                            ContactsContract.CommonDataKinds.Event.START_DATE));
            int eventType = cursor.getInt(
                    cursor.getColumnIndex(
                            ContactsContract.CommonDataKinds.Event.TYPE));
            long eventId = cursor.getLong(cursor.getColumnIndex(ContactsContract.CommonDataKinds
                    .Event._ID));
            long contactId = cursor.getLong(
                    cursor.getColumnIndex(
                            ContactsContract.Data.CONTACT_ID));
            long rawContactId = cursor.getLong(cursor.getColumnIndex(ContactsContract
                    .CommonDataKinds.Event.RAW_CONTACT_ID));
            ContactEvent contactEvent = new ContactEvent(eventId,
                    contactId, rawContactId, displayName, eventDate, eventType);
            events.add(contactEvent);
        }
        cursor.close();
        return events;
    }

    private Cursor getContactsCursor() {
        return contentResolver.query(ContactsContract.Contacts.CONTENT_URI,
                new String[]{
                        ContactsContract.Contacts._ID,
                        ContactsContract.Contacts.DISPLAY_NAME,
                        ContactsContract.Contacts.HAS_PHONE_NUMBER},
                ContactsContract.Contacts.IN_VISIBLE_GROUP + "=?",
                new String[]{String.valueOf(1)},
                ContactsContract.Contacts.DISPLAY_NAME + " ASC");
    }

    private Cursor getContactsCursor(long contactID) {
        return contentResolver.query(
                ContactsContract.Contacts.CONTENT_URI,
                new String[]{
                        ContactsContract.Contacts._ID,
                        ContactsContract.Contacts.DISPLAY_NAME,
                        ContactsContract.Contacts.HAS_PHONE_NUMBER},
                ContactsContract.Contacts._ID + "=? ",
                new String[]{String.valueOf(contactID)}, null);
    }

    private Cursor getContactPhoneNoCursor(long contactID) {
        return contentResolver
                .query(ContactsContract.Data.CONTENT_URI,
                        new String[]{
                                ContactsContract.CommonDataKinds.Phone._ID,
                                ContactsContract.CommonDataKinds.Phone.NUMBER,
                                ContactsContract.CommonDataKinds.Phone.IS_SUPER_PRIMARY},
                        ContactsContract.Data.MIMETYPE + "=? AND "
                                + ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " =? ",
                        new String[]{
                                ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE,
                                String.valueOf(contactID)
                        }, null);
    }

    private List<Long> getRawContactID(long contactID) {
        List<Long> rawContactIdList = new ArrayList<>();
        Cursor c = contentResolver.query(ContactsContract.RawContacts.CONTENT_URI,
                new String[]{ContactsContract.RawContacts._ID},
                ContactsContract.RawContacts.CONTACT_ID + "=?",
                new String[]{String.valueOf(contactID)}, null);
        while (c.moveToNext()) {
            rawContactIdList.add(c.getLong(c.getColumnIndex(ContactsContract.RawContacts._ID)));
        }
        return rawContactIdList;
    }

    private Cursor getEventsCursor() {
        return contentResolver.query(
                ContactsContract.Data.CONTENT_URI,
                new String[]{
                        ContactsContract.CommonDataKinds.Event._ID,
                        ContactsContract.CommonDataKinds.Event.START_DATE,
                        ContactsContract.CommonDataKinds.Event.CONTACT_ID,
                        ContactsContract.CommonDataKinds.Event.RAW_CONTACT_ID,
                        ContactsContract.CommonDataKinds.Event.DISPLAY_NAME,
                        ContactsContract.CommonDataKinds.Event.TYPE},
                ContactsContract.Data.MIMETYPE + "=? AND (" +
                        ContactsContract.CommonDataKinds.Event.TYPE +
                        "=? OR " + ContactsContract.CommonDataKinds.Event.TYPE
                        + " =? )",
                new String[]{ContactsContract.CommonDataKinds.Event.CONTENT_ITEM_TYPE,
                        Integer.toString(ContactsContract.CommonDataKinds.Event.TYPE_BIRTHDAY),
                        Integer.toString(ContactsContract.CommonDataKinds.Event.TYPE_ANNIVERSARY)},
                ContactsContract.CommonDataKinds.Event.DISPLAY_NAME + " ASC");
    }

    private Cursor getEventsCursor(long contactID) {
        return contentResolver.query(
                ContactsContract.Data.CONTENT_URI,
                new String[]{
                        ContactsContract.CommonDataKinds.Event._ID,
                        ContactsContract.CommonDataKinds.Event.START_DATE,
                        ContactsContract.CommonDataKinds.Event.CONTACT_ID,
                        ContactsContract.CommonDataKinds.Event.RAW_CONTACT_ID,
                        ContactsContract.CommonDataKinds.Event.DISPLAY_NAME,
                        ContactsContract.CommonDataKinds.Event.TYPE},
                ContactsContract.Data.MIMETYPE + "=? AND ("
                        + ContactsContract.CommonDataKinds.Event.TYPE +
                        "=? OR " + ContactsContract.CommonDataKinds.Event.TYPE
                        + "=?) AND " + ContactsContract.CommonDataKinds.Event.CONTACT_ID + "=? ",
                new String[]{ContactsContract.CommonDataKinds.Event.CONTENT_ITEM_TYPE,
                        Integer.toString(ContactsContract.CommonDataKinds.Event.TYPE_BIRTHDAY),
                        Integer.toString(ContactsContract.CommonDataKinds.Event.TYPE_ANNIVERSARY),
                        String.valueOf(contactID)}, null);
    }

    private Cursor getEventsCursor(int eventType) {
        return contentResolver.query(
                ContactsContract.Data.CONTENT_URI,
                new String[] {
                        ContactsContract.CommonDataKinds.Event.START_DATE,
                        ContactsContract.CommonDataKinds.Event.DISPLAY_NAME },
                ContactsContract.Data.MIMETYPE + "=? AND "
                        + ContactsContract.CommonDataKinds.Event.TYPE + "=? ",
                new String[] { ContactsContract.CommonDataKinds.Event.CONTENT_ITEM_TYPE,
                        Integer.toString(eventType) },
                ContactsContract.CommonDataKinds.Event.DISPLAY_NAME + " ASC");
    }

    public int delete(long eventId) {
        return contentResolver.delete(ContactsContract.Data.CONTENT_URI, ContactsContract.Data
                ._ID + " =? ", new String[] { String.valueOf(eventId) });
    }
}