package in.androbolt.android.birthdaybook.settings;

import android.preference.Preference;

/**
 * Settings Contract
 */
public interface SettingsContract {
    interface View {

        void disableNotifyInAdvance();

        void disableNotifyOnEventDate();
    }

    interface ActionListener {

        void updateNotificationTime(String notificationTime);

        void toggleNotification(boolean isEnabled);

        void toggleAdvanceNotification(boolean isAdvanceNotificationEnabled);
    }
}
