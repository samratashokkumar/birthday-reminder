package in.androbolt.android.birthdaybook.events.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.provider.BaseColumns;

/**
 * Events table DAO
 */
class Events {

    @SuppressWarnings("unused")
    private static final String TAG = "db.Events";
    public static final String TABLE_EVENTS = "events";

    private final SQLiteDatabase db;

    public Events(SQLiteDatabase db) {
        this.db = db;
    }

    public void create() {
        String createQuery = "CREATE TABLE " + TABLE_EVENTS + " ( "
                + Columns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + Columns.PEOPLE_ID + " INTEGER , "
                + Columns.DATE + " TEXT, "
                + Columns.TYPE + " TEXT, "
                + "FOREIGN KEY (" + Columns.PEOPLE_ID
                + ") REFERENCES "+ People.TABLE_PEOPLE
                + "( " + People.Columns._ID +" ))";
        db.execSQL(createQuery);
    }

    public long add(long peopleId, String eventDate, int eventType) {
        ContentValues cv = new ContentValues();
        cv.put(Columns.PEOPLE_ID, peopleId);
        cv.put(Columns.DATE, eventDate);
        cv.put(Columns.TYPE, eventType);
        return db.insert(TABLE_EVENTS, null, cv);
    }

    public SQLiteStatement getInsertStatement() {
        return db.compileStatement
                ("INSERT INTO " + TABLE_EVENTS + "("
                        + Columns.PEOPLE_ID + ", "
                        + Columns.DATE + ", "
                        + Columns.TYPE + ") "
                        + " VALUES (?, ?, ?)");
    }

    public int updateDate(long eventId, String eventDate) {
        ContentValues cv = new ContentValues();
        cv.put(Columns.DATE, eventDate);
        return db.update(
                TABLE_EVENTS,
                cv,
                Columns._ID + " =? ",
                new String[]{
                        String.valueOf(eventId)
                });
    }

    public int delete(long eventId) {
        int rowsAffected = 0;
        try {
            db.beginTransaction();
            Cursor eventCursor =
                    db.rawQuery(
                            "SELECT " + Columns.PEOPLE_ID + " FROM "
                                    + TABLE_EVENTS
                                    + " WHERE "
                                    + Columns.PEOPLE_ID
                                    + " IN ( SELECT " + Columns.PEOPLE_ID
                                    + " FROM "
                                    + TABLE_EVENTS + " WHERE "
                                    + Columns._ID + " =? )",
                            new String[] { String.valueOf(eventId) });
            long peopleId = -1;
            // Checks is this the last event for a people row
            if (eventCursor.getCount() == 1) {
                eventCursor.moveToFirst();
                peopleId =
                        eventCursor.getLong(eventCursor.
                                getColumnIndex(Columns.PEOPLE_ID));
            }
            eventCursor.close();
            rowsAffected = db.delete(TABLE_EVENTS,
                    Columns._ID + " =? ",
                    new String[]{
                            String.valueOf(eventId)
                    });
            //If its the last event then delete the people row
            if (peopleId != -1) {
                People people = new People(db);
                people.delete(peopleId);
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
        return rowsAffected;
    }

    public long getPeopleId(long appEventId) {
        Cursor cursor = db.query(
                TABLE_EVENTS,
                new String[]{Columns.PEOPLE_ID},
                Columns._ID + " =? ",
                new String[]{String.valueOf(appEventId)},
                null, null, null);
        if (cursor.moveToFirst()) {
            long peopleId = cursor
                    .getLong(cursor.getColumnIndex(
                            Columns.PEOPLE_ID));
            cursor.close();
            return peopleId;
        }
        return -1;
    }

    /**
     * Events table columns
     */
    public interface Columns extends BaseColumns{
        String PEOPLE_ID = "people_id";
        String DATE = "event_date";
        String TYPE = "event_type";
    }
}
