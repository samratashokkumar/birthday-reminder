package in.androbolt.android.birthdaybook.inappevent;

import in.androbolt.android.birthdaybook.events.EventsRepository;
import in.androbolt.android.birthdaybook.events.models.CustomEvent;

/**
 * Created by ashok on 26/3/16.
 */
public class AddInAppEventPresenter implements AddInAppEventContract.ActionListener{

    private AddInAppEventContract.View view;
    private EventsRepository eventsRepository;

    public AddInAppEventPresenter(AddInAppEventContract.View view, EventsRepository eventsRepository) {
        this.view = view;
        this.eventsRepository = eventsRepository;
    }

    @Override
    public void saveEvent(CustomEvent customEvent) {
        if (validateEvent(customEvent)) {
            eventsRepository.addEvent(customEvent);
            view.saveSuccessful();
        } else {
            view.showNameMissing();
        }
    }

    private boolean validateEvent(CustomEvent customEvent) {
        if (!customEvent.getDisplayName().trim().isEmpty()) {
            return true;
        }
        return false;
    }
}
