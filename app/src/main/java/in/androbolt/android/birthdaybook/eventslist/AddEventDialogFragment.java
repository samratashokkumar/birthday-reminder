package in.androbolt.android.birthdaybook.eventslist;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import in.androbolt.android.birthdaybook.BuildConfig;
import in.androbolt.android.birthdaybook.R;
import in.androbolt.android.birthdaybook.contactevent.AddEventActivity;
import in.androbolt.android.birthdaybook.facebookeventslist.FacebookEventsListActivity;
import in.androbolt.android.birthdaybook.facebookimport.FacebookImportActivity;
import in.androbolt.android.birthdaybook.inappevent.AddInAppEventActivity;
import in.androbolt.android.birthdaybook.preferences.AppPreferences;

public class AddEventDialogFragment extends BottomSheetDialogFragment
        implements View.OnClickListener {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_add, container, false);
        view.findViewById(R.id.llAddContactEvent).setOnClickListener(this);
        view.findViewById(R.id.llAddFacebookEvent).setOnClickListener(this);
        view.findViewById(R.id.llInAppEvent).setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getTag().toString()) {
            case "contact":
                startActivity(new Intent(getContext(), AddEventActivity.class));
                break;
            case "facebook":
                AppPreferences appPreferences = new AppPreferences(this.getActivity());
                String fbWebCalUrl = appPreferences.getFbWebCalUrl();
                if (!fbWebCalUrl.isEmpty()) {
                    Intent intent = new Intent(getActivity(), FacebookImportActivity.class);
                    intent.putExtra(FacebookEventsListActivity.EXTRAS_FBWEBCALURL, fbWebCalUrl);
                    startActivity(intent);
                } else {
                    startActivity(new Intent(getContext(), FacebookImportActivity.class));
                }
                break;
            case "inapp":
                startActivity(new Intent(getContext(), AddInAppEventActivity.class));
                break;
        }
        dismiss();
    }
}
