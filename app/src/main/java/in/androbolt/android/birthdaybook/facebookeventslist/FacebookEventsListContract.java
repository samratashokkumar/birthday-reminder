package in.androbolt.android.birthdaybook.facebookeventslist;

import java.util.List;
import java.util.Set;

import in.androbolt.android.birthdaybook.base.Presenter;
import in.androbolt.android.birthdaybook.events.models.Event;

/**
 * Facebook Events Contract
 */
public interface FacebookEventsListContract {
    interface View {
        void showEvents(List<Event> events);

        void updateSelected(Set<String> selectedIds);

        void showMessage(int messageId, String message);

        void showProgress(int messageId);

        void hideProgress();

        void refreshList();
    }
    interface ActionListener extends Presenter<View> {

        void importEvents();

        void selectAll();

        void selectNone();

        void toggleSelect(int position);

        void toggleSelectAll();

        void onMenuCreated();
    }
}
