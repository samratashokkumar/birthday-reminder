package in.androbolt.android.birthdaybook.events;

import android.text.TextUtils;
import android.util.JsonReader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import in.androbolt.android.birthdaybook.common.models.Result;
import in.androbolt.android.birthdaybook.events.models.FacebookEvent;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Facebook Picture URL Updater - Updates the picture URL for the given list of FacebookEvent
 */
public class FacebookPictureUrlUpdater {

    private static final int ID_LIMIT = 50;
    private OkHttpClient client = new OkHttpClient();
    private HttpUrl graphPictureUrl;
    private Map<String, FacebookEvent> facebookEventMap;

    public FacebookPictureUrlUpdater(String graphPictureUrl) {
        this.graphPictureUrl = HttpUrl.parse(graphPictureUrl);
    }

    public Result<Void> updatePictureUrl(List<FacebookEvent> facebookEvents) throws IOException {
        facebookEventMap = new HashMap<>();
        StringBuilder ids = new StringBuilder();
        int count = 1;
        Result<Void> result = null;
        for (FacebookEvent facebookEvent: facebookEvents) {
            String nativeId = facebookEvent.getNativeId();
            ids.append(String.format("%s,", nativeId));
            facebookEventMap.put(nativeId, facebookEvent);
            if (count % ID_LIMIT == 0) {
                //TODO: Pass Id set instead of ids String
                //TODO: Update using ThreadPoolExecutor
                result = updatePictureUrlBatch(ids);
                if (result.getType() == Result.Type.FAILURE) {
                    return result;
                }
                facebookEventMap.clear();
                ids = new StringBuilder();
            }
            count++;
        }
        if (ids.length() > 0) {
            result = updatePictureUrlBatch(ids);
        }
        return result;
    }

    private Result<Void> updatePictureUrlBatch(StringBuilder ids) throws IOException {
        ids.deleteCharAt(ids.lastIndexOf(","));
        HttpUrl url = graphPictureUrl.newBuilder()
                .addQueryParameter("ids", ids.toString())
                .addQueryParameter("redirect", "false").build();
        Request request = new Request.Builder().url(url).build();
        Response response = client.newCall(request).execute();
        if (response.isSuccessful()) {
            JsonReader jsonReader = new JsonReader(new BufferedReader(new InputStreamReader
                    (response.body().byteStream())));
            processResponse(jsonReader);
            jsonReader.close();
        } else if (response.code() == 404) {
            JsonReader jsonReader = new JsonReader(new BufferedReader(new InputStreamReader
                    (response.body().byteStream())));
            String[] unavailableIds = processNotFoundResponse(jsonReader);
            jsonReader.close();
            if (unavailableIds.length > 0) {
                Set<String> newIds = new HashSet<>(Arrays.asList(ids.toString().split(",")));
                for (String unavailableId : unavailableIds) {
                    newIds.remove(unavailableId);
                }
                StringBuilder updatedIds = new StringBuilder();
                for (String id : newIds) {
                    updatedIds.append(String.format("%s,", id));
                }
                //Retry once more after removing unavailable ids
                updatePictureUrlBatch(updatedIds);
            }
        } else {
            return new Result<>(null, response.message(), Result.Type.FAILURE);
        }
        return new Result<>(null, "", Result.Type.SUCCESS);
    }

    // Sample Response:
    // {
    //  "1":{
    //          "data":{"is_silhouette":false,"url":"pic_url"}
    //      },
    //  "2":{
    //          "data":{"is_silhouette":false,"url":"pic_url"}
    //      }
    // }
    private void processResponse(JsonReader jsonReader) throws IOException {
        jsonReader.beginObject();
        while(jsonReader.hasNext()) {
            processId(jsonReader);
        }
        jsonReader.endObject();
    }

    private void processId(JsonReader jsonReader) throws IOException {
        String id = jsonReader.nextName();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            processData(jsonReader, id);
        }
        jsonReader.endObject();
    }

    private void processData(JsonReader jsonReader, String id) throws IOException {
        jsonReader.nextName(); //data property
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            processDataProperties(jsonReader, id);
        }
        jsonReader.endObject();
    }

    private void processDataProperties(JsonReader jsonReader, String id) throws IOException {
        String prop = jsonReader.nextName();
        if (prop.equals("url")) {
            String url = jsonReader.nextString();
            FacebookEvent facebookEvent = facebookEventMap.get(id);
            facebookEvent.setPicturePath(url);
        } else {
            jsonReader.skipValue();
        }
    }

    // Not Found Sample Response:
    // {
    //  "error":{
    //              "message":"(#803) Some of the aliases you requested do not exist: 1,3454",
    //              "type":"OAuthException","code":803,
    //              "fbtrace_id":"abc"
    //          }
    // }
    private String[] processNotFoundResponse(JsonReader jsonReader) throws IOException {
        String[] unavailableIds = new String[0];
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            jsonReader.nextName();//error property
            jsonReader.beginObject();
            while (jsonReader.hasNext()) {
                String prop = jsonReader.nextName();
                if (prop.equals("message")) {
                    String message = jsonReader.nextString();
                    String ids = message.substring(message.lastIndexOf(":") + 1).trim();
                    unavailableIds = ids.split(",");
                } else {
                    jsonReader.skipValue();
                }
            }
            jsonReader.endObject();
        }
        jsonReader.endObject();
        return unavailableIds;
    }
}
