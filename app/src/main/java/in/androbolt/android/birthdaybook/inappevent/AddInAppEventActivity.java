package in.androbolt.android.birthdaybook.inappevent;

import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import in.androbolt.android.birthdaybook.R;
import in.androbolt.android.birthdaybook.common.datepicker.DatePicker;
import in.androbolt.android.birthdaybook.events.EventsRepository;
import in.androbolt.android.birthdaybook.events.models.CustomEvent;
import in.androbolt.android.birthdaybook.events.models.Event;
import in.androbolt.android.birthdaybook.utils.DateTimeUtils;
import in.androbolt.android.birthdaybook.utils.DateUtils;

public class AddInAppEventActivity extends AppCompatActivity implements AddInAppEventContract.View{

    private AddInAppEventContract.ActionListener actionListener;
    private TextInputEditText tietName;
    private RadioGroup rgEventType;
    private DatePicker datePicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_in_app_event);
        Toolbar appToolbar = (Toolbar) findViewById(R.id.appToolbar);
        setSupportActionBar(appToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        actionListener = new AddInAppEventPresenter(this, new EventsRepository(this
                .getApplicationContext()));
        tietName = (TextInputEditText) findViewById(R.id.tietName);
        rgEventType = (RadioGroup) findViewById(R.id.rgEventType);
        datePicker = (DatePicker) findViewById(R.id.dpEventDate);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_in_app_event, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_done:
                saveInAppEvent();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void saveInAppEvent() {
        String displayName = tietName.getText().toString();
        int eventType = Integer.valueOf(((RadioButton) findViewById(rgEventType
                .getCheckedRadioButtonId())).getTag().toString());
        String eventDate = String.format("%d-%d-%d", datePicker.getYear(), datePicker.getMonth(),
                datePicker.getDayOfMonth());
        CustomEvent customEvent = new CustomEvent(displayName, eventDate, eventType);
        actionListener.saveEvent(customEvent);
    }

    @Override
    public void showNameMissing() {
        Toast.makeText(this, R.string.name_missing, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void saveSuccessful() {
        Toast.makeText(this, R.string.saved, Toast.LENGTH_SHORT).show();
        finish();
    }
}
