package in.androbolt.android.birthdaybook.events.models;

import android.os.Parcelable;

import in.androbolt.android.birthdaybook.common.models.ParsedDate;
import in.androbolt.android.birthdaybook.utils.DateTimeUtils;

/**
 * Base class for all types of events
 */
public abstract class Event implements Parcelable{
    public static final int TYPE_BIRTHDAY = 3;
    public static final int TYPE_ANNIVERSARY = 1;
    public static final String DATE_PATTERN = "\\d{4}-\\d{2}-\\d{2}";

    private long id;
    private String displayName;
    private String eventDate;
    private String parsedDate;
    private boolean hasYear;
    private int eventType;
    private boolean isError = false;

    Event(long id, String displayName){
        this.displayName = displayName;
        this.id = id;
    }

    Event(String displayName, String eventDate, int eventType) {
        this (-1, displayName, eventDate, eventType);
    }

    Event(long id, String displayName, String eventDate, int eventType) {
        this (id, displayName);
        this.eventDate = eventDate;
        ParsedDate parsedDate = DateTimeUtils.parseEventDate(eventDate);
        this.parsedDate = parsedDate.date;
        this.hasYear = parsedDate.hasYear;
        this.eventType = eventType;
        if (!this.parsedDate.matches(DATE_PATTERN)) {
            this.isError = true;
        }
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
        ParsedDate parsedDate = DateTimeUtils.parseEventDate(eventDate);
        this.parsedDate = parsedDate.date;
        this.hasYear = parsedDate.hasYear;
    }

    public String getParsedDate() {
        return parsedDate;
    }

    public boolean hasYear() {
        return hasYear;
    }

    public int getEventType() {
        return eventType;
    }

    public void setEventType(int eventType) {
        this.eventType = eventType;
    }

    public abstract String getEventSource();

    public boolean isError() {
        return isError;
    }

    @Override
    public String toString() {
        return "Event{" +
                "id=" + id +
                ", displayName='" + displayName + '\'' +
                ", eventDate='" + eventDate + '\'' +
                ", eventType=" + eventType +
                ", isError=" + isError +
                '}';
    }
}
