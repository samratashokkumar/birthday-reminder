package in.androbolt.android.birthdaybook.appwidget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.RemoteViews;
import android.widget.Toast;

import in.androbolt.android.birthdaybook.R;
import in.androbolt.android.birthdaybook.alarms.AppAlarms;
import in.androbolt.android.birthdaybook.mainactivity.MainActivity;

/**
 * Implementation of App Widget functionality.
 */
public class AppWidget extends AppWidgetProvider {

    public static final String UPDATE_ACTION = "in.androbolt.android.birthdaybook.APPWIDGET_UPDATE";
    private static final String DETAIL_ACTION = "in.androbolt.android.birthdayreminder" +
            ".DETAIL_ACTION";
    public static final String EXTRA_ID = "in.androbolt.android.birthdaybook.ID";
    public static final String EXTRA_SOURCE = "in.androbolt.android.birthdaybook.SOURCE";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(DETAIL_ACTION)) {
            long index = intent.getLongExtra(EXTRA_ID, 0);
            Toast.makeText(context, String.valueOf(index), Toast.LENGTH_LONG).show();
            Intent mainActivityIntent = new Intent(context, MainActivity.class);
            mainActivityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(mainActivityIntent);
        } else if (intent.getAction().equals(UPDATE_ACTION)) {
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
            ComponentName thisAppWidget = new ComponentName(context.getPackageName(), AppWidget
                    .class.getName());
            int[] appWidgetIds = appWidgetManager.getAppWidgetIds(thisAppWidget);
            onUpdate(context, appWidgetManager, appWidgetIds);
        }
        super.onReceive(context, intent);
    }

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {
        Intent intent = new Intent(context, AppWidgetService.class);
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        intent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.app_widget);
        views.setRemoteAdapter(R.id.lvEvents, intent);

        Intent detailsIntent = new Intent(context, AppWidget.class);
        detailsIntent.setAction(DETAIL_ACTION);
        detailsIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        detailsIntent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));

        PendingIntent detailsPendingIntent = PendingIntent.getBroadcast(context, 0,
                detailsIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        views.setPendingIntentTemplate(R.id.lvEvents, detailsPendingIntent);

        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
        AppAlarms appAlarms = new AppAlarms(context);
        appAlarms.setAppWidgetUpdateAlarm();
    }

    @Override
    public void onDisabled(Context context) {
        AppAlarms appAlarms = new AppAlarms(context);
        appAlarms.cancelAppWidgetUpdateAlarm();
    }
}

