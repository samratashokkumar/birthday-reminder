package in.androbolt.android.birthdaybook.inappevent;

import in.androbolt.android.birthdaybook.events.models.CustomEvent;

/**
 * Add In App Event Contract
 */
public interface AddInAppEventContract {

    interface View {
        void showNameMissing();
        void saveSuccessful();
    }

    interface ActionListener {
        void saveEvent(CustomEvent customEvent);
    }

}
