package in.androbolt.android.birthdaybook.events.db;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import in.androbolt.android.birthdaybook.events.models.AppEvent;
import in.androbolt.android.birthdaybook.events.models.CustomEvent;
import in.androbolt.android.birthdaybook.events.models.Event;
import in.androbolt.android.birthdaybook.events.models.FacebookEvent;
import in.androbolt.android.birthdaybook.utils.DateTimeUtils;

/**
 * Events View DAO
 */
class EventsView {

    private static final String VIEW_EVENTS = "view_events";
    private final SQLiteDatabase db;

    public EventsView(SQLiteDatabase db) {
        this.db = db;
    }

    public void create() {
        String createView = "CREATE VIEW " + VIEW_EVENTS + " AS " + "SELECT "
                + " E." + Events.Columns._ID + ", "
                + People.Columns.NAME + ", "
                + People.Columns.SOURCE + ", "
                + People.Columns.NATIVE_ID + ", "
                + People.Columns.PICTURE_PATH + ", "
                + Events.Columns.PEOPLE_ID + ", "
                + Events.Columns.DATE + ", "
                + Events.Columns.TYPE + " FROM "
                + People.TABLE_PEOPLE + " P INNER JOIN "
                + Events.TABLE_EVENTS + " E ON "
                + " P." + People.Columns._ID + " = "
                + " E." + Events.Columns.PEOPLE_ID;
        db.execSQL(createView);
    }

    public List<Event> getEvents() {
        return getAppEvents((Long) null);
    }

    public List<Event> getEvents(String eventSource) {
        return getAppEvents(eventSource);
    }

    public List<Event> getEvents(long peopleId) {
        return getAppEvents(peopleId);
    }

    @NonNull
    private List<Event> getAppEvents(Long peopleId) {
        List<Event> appEvents = new ArrayList<>();
        Cursor cursor;
        if (peopleId == null) {
            cursor = db.query(VIEW_EVENTS, new String[]{
                    Events.Columns._ID, People.Columns.NAME, People.Columns.SOURCE, People.Columns
                    .NATIVE_ID, People.Columns.PICTURE_PATH, Events.Columns.PEOPLE_ID, Events
                    .Columns.DATE, Events.Columns.TYPE
            }, null, null, null, null, null);
        } else {
            cursor = db.query(VIEW_EVENTS, new String[]{
                    Events.Columns._ID, People.Columns.NAME, People.Columns.SOURCE, People.Columns
                    .NATIVE_ID, People.Columns.PICTURE_PATH, Events.Columns.PEOPLE_ID, Events
                    .Columns.DATE, Events.Columns.TYPE
            }, Events.Columns.PEOPLE_ID + " =? ", new String[]{String.valueOf(peopleId)}, null, null, null);
        }
        while (cursor.moveToNext()) {
            long cursorEventId = cursor.getLong(
                    cursor.getColumnIndex(Events.Columns._ID));
            long cursorPeopleId = cursor.getLong(
                    cursor.getColumnIndex(Events.Columns.PEOPLE_ID));
            String name = cursor.getString(
                    cursor.getColumnIndex(People.Columns.NAME));
            String source = cursor.getString(
                    cursor.getColumnIndex(People.Columns.SOURCE));
            String nativeId = cursor.getString(
                    cursor.getColumnIndex(People.Columns.NATIVE_ID));
            String picturePath = cursor.getString(
                    cursor.getColumnIndex(People.Columns.PICTURE_PATH));
            String eventDate = cursor.getString(
                    cursor.getColumnIndex(Events.Columns.DATE));
            int eventType = cursor.getInt(
                    cursor.getColumnIndex(Events.Columns.TYPE));
            AppEvent appEvent = null;
            if (source.equals(FacebookEvent.SOURCE)) {
                appEvent = new FacebookEvent(cursorEventId, cursorPeopleId, name, eventDate,
                        eventType, nativeId, picturePath);
            } else if (source.equals(CustomEvent.SOURCE)) {
                appEvent = new CustomEvent(cursorEventId, cursorPeopleId, name, eventDate,
                        eventType);
            }
            appEvents.add(appEvent);
        }
        cursor.close();
        return appEvents;
    }



    private List<Event> getAppEvents(String eventSource) {
        List<Event> appEvents = new ArrayList<>();
        Cursor cursor = cursor = db.query(VIEW_EVENTS, new String[]{ Events.Columns._ID, People
                .Columns.NAME, People.Columns.SOURCE, People.Columns.NATIVE_ID, People.Columns
                .PICTURE_PATH, Events.Columns.PEOPLE_ID, Events.Columns.DATE, Events.Columns
                        .TYPE}, People.Columns.SOURCE + " =? ", new String[]{String.valueOf
                (eventSource)}, null, null, null);
        while (cursor.moveToNext()) {
            long cursorEventId = cursor.getLong(
                    cursor.getColumnIndex(Events.Columns._ID));
            long cursorPeopleId = cursor.getLong(
                    cursor.getColumnIndex(Events.Columns.PEOPLE_ID));
            String name = cursor.getString(
                    cursor.getColumnIndex(People.Columns.NAME));
            String source = cursor.getString(
                    cursor.getColumnIndex(People.Columns.SOURCE));
            String nativeId = cursor.getString(
                    cursor.getColumnIndex(People.Columns.NATIVE_ID));
            String picturePath = cursor.getString(
                    cursor.getColumnIndex(People.Columns.PICTURE_PATH));
            String eventDate = cursor.getString(
                    cursor.getColumnIndex(Events.Columns.DATE));
            int eventType = cursor.getInt(
                    cursor.getColumnIndex(Events.Columns.TYPE));
            AppEvent appEvent = null;
            if (source.equals(FacebookEvent.SOURCE)) {
                appEvent = new FacebookEvent(cursorEventId, cursorPeopleId, name, eventDate,
                        eventType, nativeId, picturePath);
            } else if (source.equals(CustomEvent.SOURCE)) {
                appEvent = new CustomEvent(cursorEventId, cursorPeopleId, name, eventDate,
                        eventType);
            }
            appEvents.add(appEvent);
        }
        cursor.close();
        return appEvents;
    }

    public List<String> getNames(int eventType, String eventDate) {
        List<String> nameList = new ArrayList<>();
        Cursor cursor = this.db.query(VIEW_EVENTS, new String[]{People.Columns.NAME, Events
                .Columns.DATE}, Events.Columns.TYPE + " =? ", new String[]{String.valueOf
                (eventType)}, null, null, People.Columns.NAME + " ASC");
        while (cursor.moveToNext()) {
            String name = cursor.getString(cursor.getColumnIndex(People.Columns.NAME));
            String cursorEventDate = cursor.getString(cursor.getColumnIndex(Events.Columns.DATE));
            if (eventDate.equals(DateTimeUtils.addCurrentYear(cursorEventDate))) {
                nameList.add(name);
            }
        }
        return nameList;
    }
}
