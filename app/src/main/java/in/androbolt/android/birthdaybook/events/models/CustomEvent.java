package in.androbolt.android.birthdaybook.events.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Custom App Event
 */
public class CustomEvent extends AppEvent {

    public static final String SOURCE = "custom";

    public CustomEvent(String displayName,
                       String eventDate,
                       int eventType) {
        super(displayName, eventDate, eventType, null, null);
    }

    public CustomEvent(long id,
                       long peopleId,
                       String displayName,
                       String eventDate,
                       int eventType) {
        super(id, peopleId, displayName, eventDate, eventType, null, null);
    }

    @Override
    public String getEventSource() {
        return SOURCE;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(getId());
        dest.writeString(getDisplayName());
        dest.writeString(getEventDate());
        dest.writeInt(getEventType());
        dest.writeLong(getPeopleId());
    }

    public static final Parcelable.Creator<CustomEvent> CREATOR
            = new Parcelable.Creator<CustomEvent>() {
        @Override
        public CustomEvent createFromParcel(Parcel source) {
            long eventId = source.readLong();
            String displayName = source.readString();
            String eventDate = source.readString();
            int eventType = source.readInt();
            long peopleId = source.readLong();
            return new CustomEvent(eventId,
                    peopleId,
                    displayName,
                    eventDate,
                    eventType);
        }

        @Override
        public CustomEvent[] newArray(int size) {
            return new CustomEvent[size];
        }
    };
}
