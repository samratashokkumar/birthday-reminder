package in.androbolt.android.birthdaybook.eventslist;


import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;

import in.androbolt.android.birthdaybook.R;
import in.androbolt.android.birthdaybook.events.EventsRepository;
import in.androbolt.android.birthdaybook.events.models.Event;
import in.androbolt.android.birthdaybook.preferences.AppPreferences;


/**
 * Events List Fragment
 */
public class EventsListFragment extends Fragment implements EventsListContract.View, View.OnClickListener{

    @SuppressWarnings("unused")
    private static final String TAG = "EventsListFragment";
    private static final int READ_CONTACTS_PERMISSION_REQUEST = 1;

    private RecyclerView recyclerView;
    private EventsListAdapter eventsListAdapter;
    private EventsListContract.ActionListener actionListener;
    private Filter filter;

    public EventsListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        eventsListAdapter = new EventsListAdapter(new ArrayList<Event>(0), android.text.format
                .DateFormat.getDateFormat(getActivity().getApplicationContext()));
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        actionListener = new EventsListPresenter(new AppPreferences(getActivity()
                .getApplicationContext()), new EventsRepository(getActivity()
                .getApplicationContext()), this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_events_list, container, false);
        setupRecyclerView(view);
        FloatingActionButton fabAdd = (FloatingActionButton) view.findViewById(R.id.fabAdd);
        fabAdd.setOnClickListener(this);
        setHasOptionsMenu(true);
        return view;
    }

    private void setupRecyclerView(View view) {
        final View eventListHeader = view.findViewById(R.id.event_list_header);
        final TextView tvHeader = (TextView) eventListHeader.findViewById(R.id.tvHeader);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView = (RecyclerView) view.findViewById(R.id.rvEventsList);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(eventsListAdapter);

        //Shows sticky header
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            private int currentFirstVisiblePosition = -1;

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                        .getLayoutManager();
                int firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();
                int firstCompletelyVisibleItemPosition = linearLayoutManager
                        .findFirstCompletelyVisibleItemPosition();
                if (firstCompletelyVisibleItemPosition == 0) {
                    eventListHeader.setVisibility(View.GONE);
                } else {
                    eventListHeader.setVisibility(View.VISIBLE);
                }
                if (currentFirstVisiblePosition != firstVisibleItemPosition) {
                    Pair<Integer, String> headerInfo = eventsListAdapter
                            .getHeaderInfo(firstVisibleItemPosition);
                    if (headerInfo != null) {
                        tvHeader.setText(headerInfo.second);
                    } else {
                        eventListHeader.setVisibility(View.GONE);
                    }
                    currentFirstVisiblePosition = firstVisibleItemPosition;
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[]
            grantResults) {
        switch (requestCode) {
            case READ_CONTACTS_PERMISSION_REQUEST:
                if (permissions.length > 0 && grantResults[0] == PackageManager
                        .PERMISSION_GRANTED) {
                    actionListener.loadEvents();
                } else {
                    Log.d(TAG, "Permission Denied");
                }
                break;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setQueryHint(getText(R.string.search_friend));
        searchView.setOnQueryTextListener(actionListener);
        searchView.setIconifiedByDefault(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[] {Manifest.permission
                    .READ_CONTACTS}, READ_CONTACTS_PERMISSION_REQUEST);
        } else {
            actionListener.loadEvents();
        }
    }

    @Override
    public void showEvents(List<Event> events, DateFormat dateFormat) {
        eventsListAdapter.setItems(events);
        filter = eventsListAdapter.getFilter();
    }

    @Override
    public void showEventDetails(Event event) {

    }

    @Override
    public void applyFilter(String newText) {
        filter.filter(newText);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.fabAdd) {
            AddEventDialogFragment addEventDialogFragment = new
                    AddEventDialogFragment();
            addEventDialogFragment.show(getFragmentManager(), "dialog_add");
        }
    }
}
