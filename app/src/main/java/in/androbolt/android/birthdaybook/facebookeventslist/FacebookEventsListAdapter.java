package in.androbolt.android.birthdaybook.facebookeventslist;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Set;

import in.androbolt.android.birthdaybook.R;
import in.androbolt.android.birthdaybook.adapters.RecyclerViewListAdapter;
import in.androbolt.android.birthdaybook.events.models.Event;
import in.androbolt.android.birthdaybook.events.models.FacebookEvent;
import in.androbolt.android.birthdaybook.utils.DateTimeUtils;

/**
 * Facebook Events List Adapter
 */
public class FacebookEventsListAdapter extends RecyclerViewListAdapter {

    @SuppressWarnings("unused")
    private static final String TAG = FacebookEventsListAdapter.class.getSimpleName();
    private List<Event> events;
    private Set<String> selectedItems;
    private OnItemClickListener onItemClickListener;
    private DateFormat dateFormat;

    public FacebookEventsListAdapter(List<Event> events, DateFormat dateFormat) {
        this.events = events;
        this.dateFormat = dateFormat;
    }

    @Override
    public List<Event> getItems() {
        return events;
    }

    @Override
    public void setItems(List<Event> events) {
        this.events = events;
        notifyDataSetChanged();
    }

    public void refreshList() {
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void updateSelectedItems(Set<String> selectedItems) {
        this.selectedItems = selectedItems;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View v = layoutInflater.inflate(R.layout.facebook_event_list, parent, false);
        FacebookEventsListViewHolder facebookEventsListViewHolder = new
                FacebookEventsListViewHolder(v);
        facebookEventsListViewHolder.setOnItemClickListener(onItemClickListener);
        return facebookEventsListViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        FacebookEventsListViewHolder facebookEventsListViewHolder =
                (FacebookEventsListViewHolder) holder;
        FacebookEvent facebookEvent = (FacebookEvent) events.get(position);
        ImageView ivPhoto = facebookEventsListViewHolder.ivPhoto;
        Picasso.with(ivPhoto.getContext())
                .load(facebookEvent.getPicturePath())
                .placeholder(R.drawable.ic_contact_list_picture)
                .into(ivPhoto);
        facebookEventsListViewHolder.tvName.setText(facebookEvent.getDisplayName());
        DateFormat dateFormat = this.dateFormat;
        if (!facebookEvent.hasYear()) {
            DateFormat noYearFormat = DateTimeUtils.getNoYearFormat((SimpleDateFormat) dateFormat);
            if (noYearFormat != null) {
                dateFormat = noYearFormat;
            }
        }
        facebookEventsListViewHolder.tvEventDate.setText(
                DateTimeUtils.getFormattedDate(facebookEvent.getParsedDate(), dateFormat));
        if (selectedItems != null && selectedItems.contains(facebookEvent.getNativeId())) {
            facebookEventsListViewHolder.ivSelect.setImageResource(R.drawable.ic_check_circle);
            facebookEventsListViewHolder.background.setSelected(true);
        } else {
            facebookEventsListViewHolder.ivSelect.setImageResource(R.drawable.ic_circle);
            facebookEventsListViewHolder.background.setSelected(false);
        }
    }

    @Override
    public int getItemCount() {
        return events.size();
    }

    private static class FacebookEventsListViewHolder extends RecyclerView.ViewHolder implements
            View.OnClickListener {
        public View background;
        public ImageView ivPhoto;
        public TextView tvName;
        public TextView tvEventDate;
        public ImageView ivSelect;
        private OnItemClickListener onItemClickListener;

        public FacebookEventsListViewHolder(View vwListItem) {
            super(vwListItem);
            background = vwListItem;
            ivPhoto = (ImageView) vwListItem.findViewById(R.id.ivPhoto);
            tvName = (TextView) vwListItem.findViewById(R.id.tvName);
            tvEventDate = (TextView) vwListItem.findViewById(R.id.tvEventDate);
            ivSelect = (ImageView) vwListItem.findViewById(R.id.ivSelect);
            vwListItem.setOnClickListener(this);
        }

        public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
            this.onItemClickListener = onItemClickListener;
        }

        @Override
        public void onClick(View v) {
            if (onItemClickListener != null) {
                onItemClickListener.onItemClick(getLayoutPosition());
            }
        }
    }
}
