package in.androbolt.android.birthdaybook.eventslist;

import android.support.v7.widget.SearchView;
import android.widget.Filter;

import java.text.DateFormat;
import java.util.List;

import in.androbolt.android.birthdaybook.adapters.RecyclerViewListAdapter;
import in.androbolt.android.birthdaybook.events.models.Event;

/**
 * Events List Contract
 */
public interface EventsListContract {

    interface View {

        void showEvents(List<Event> events, DateFormat dateFormat);

        void showEventDetails(Event event);

        void applyFilter(String newText);

    }

    interface ActionListener extends SearchView
            .OnQueryTextListener {

        void loadEvents();

        void openEventDetails(Event event);
    }
}
