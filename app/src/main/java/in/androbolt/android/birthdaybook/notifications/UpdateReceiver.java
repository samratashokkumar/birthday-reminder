package in.androbolt.android.birthdaybook.notifications;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.Calendar;

import in.androbolt.android.birthdaybook.alarms.AppAlarms;
import in.androbolt.android.birthdaybook.preferences.AppPreferences;
import in.androbolt.android.birthdaybook.utils.DateTimeUtils;
import in.androbolt.android.birthdaybook.utils.StringConstants;

/**
 * Update Receiver - Enables the
 */
public class UpdateReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        AppPreferences appPreferences = new AppPreferences(context);
        if(appPreferences.isNotificationEnabled()){
            AppAlarms appAlarms = new AppAlarms(context);
            appAlarms.setTodaysEventsNotificationAlarm(appPreferences.getNotificationTime());
            if(appPreferences.isNotifyInAdvanceEnabled()){
                appAlarms.setFutureEventsNotificationAlarm(appPreferences.getNotificationTime());
                if (!appPreferences.isNotifyOnEventDateEnabled()) {
                    appAlarms.cancelTodaysEventsNotificationAlarm();
                }
            }
        }
    }
}
