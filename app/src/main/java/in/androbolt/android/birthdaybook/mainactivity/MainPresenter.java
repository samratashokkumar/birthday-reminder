package in.androbolt.android.birthdaybook.mainactivity;

/**
 * Created by ashok on 29/2/16.
 */
public class MainPresenter implements MainContract.ActionListener {

    private MainContract.View mainView;

    public MainPresenter(MainContract.View mainView) {
        this.mainView = mainView;
    }

    @Override
    public void openSettings() {
        mainView.showSettings();
    }
}
