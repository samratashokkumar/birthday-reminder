package in.androbolt.android.birthdaybook.contactevent;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import in.androbolt.android.birthdaybook.R;
import in.androbolt.android.birthdaybook.adapters.RecyclerViewListAdapter;
import in.androbolt.android.birthdaybook.events.models.ContactEvent;
import in.androbolt.android.birthdaybook.events.models.Event;

/*
 * Contact List Adapter
 */
public class ContactListAdapter
        extends RecyclerViewListAdapter {

    private static final String TAG = "ContactListAdapter";
    private OnItemClickListener onItemClickListener;

    private List<Event> events;

    public ContactListAdapter(List<Event> events) {
        this.events = events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public List<Event> getItems() {
        return this.events;
    }

    @Override
    public void setItems(List<Event> events) {
        this.events = events;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.contact_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        viewHolder.setOnItemClickListener(onItemClickListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Event event = events.get(position);
        ((ViewHolder) holder).tvName.setText(event.getDisplayName());
        CircleImageView ivPhoto = ((ViewHolder) holder).ivPhoto;
        Picasso.with(ivPhoto.getContext())
                .load(Uri.withAppendedPath(
                        ContentUris.withAppendedId(
                                ContactsContract.Contacts.CONTENT_URI,
                                ((ContactEvent) event)
                                        .getContactId()),
                        ContactsContract.Contacts.Photo.CONTENT_DIRECTORY))
                .placeholder(R.drawable.ic_contact_list_picture)
                .into(ivPhoto);
    }

    public Event removeContact(int position) {
        Event event = events.remove(position);
        notifyItemRemoved(position);
        return event;
    }

    @Override
    public int getItemCount() {
        return events.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public CircleImageView ivPhoto;
        public TextView tvName;
        private OnItemClickListener onItemClickListener;

        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            ivPhoto = (CircleImageView) itemView.findViewById(R.id.ivPhoto);
        }

        public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
            this.onItemClickListener = onItemClickListener;
        }

        @Override
        public void onClick(View v) {
            if (onItemClickListener != null) {
                onItemClickListener.onItemClick(getLayoutPosition());
            }
        }

    }
}
