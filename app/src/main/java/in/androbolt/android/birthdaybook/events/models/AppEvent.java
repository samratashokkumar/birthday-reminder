package in.androbolt.android.birthdaybook.events.models;

/**
 * Event which will be stored within our app
 */
public abstract class AppEvent extends Event {

    private long peopleId;
    private String nativeId;
    private String picturePath;

    AppEvent(String displayName,
             String eventDate,
             int eventType,
             String nativeId,
             String picturePath) {
        super(displayName, eventDate, eventType);
        this.nativeId = nativeId;
        this.picturePath = picturePath;
    }

    AppEvent(long id,
             long peopleId,
             String displayName,
             String eventDate,
             int eventType,
             String nativeId,
             String picturePath) {
        super(id, displayName, eventDate, eventType);
        this.peopleId = peopleId;
        this.nativeId = nativeId;
        this.picturePath = picturePath;
    }

    public long getPeopleId() {
        return peopleId;
    }

    public String getNativeId() {
        return nativeId;
    }

    public String getPicturePath() {
        return picturePath;
    }

    public void setPicturePath(String picturePath) {
        this.picturePath = picturePath;
    }
}
