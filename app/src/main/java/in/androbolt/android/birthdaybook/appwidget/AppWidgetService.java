package in.androbolt.android.birthdaybook.appwidget;

import android.content.Intent;
import android.os.Bundle;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import in.androbolt.android.birthdaybook.R;
import in.androbolt.android.birthdaybook.common.Comparators;
import in.androbolt.android.birthdaybook.events.EventsRepository;
import in.androbolt.android.birthdaybook.events.models.Event;
import in.androbolt.android.birthdaybook.utils.DateTimeUtils;

/**
 * App Widget Service
 */
public class AppWidgetService extends RemoteViewsService {

    private static final String TAG = "AppWidgetService";

    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        return new AppWidgetRemoteViewsFactory();
    }

    private class AppWidgetRemoteViewsFactory implements RemoteViewsFactory {

        public static final int MAX_COUNT = 5;
        private List<Event> widgetEvents;

        @Override
        public void onCreate() {

        }

        @Override
        public void onDataSetChanged() {
            List<Event> events = new EventsRepository(getApplicationContext()).getEvents();
            Collections.sort(events, Comparators.DATE_COMPARATOR);
            widgetEvents = getUpcomingEvents(events);
        }

        @Override
        public void onDestroy() {

        }

        @Override
        public int getCount() {
            return widgetEvents.size();
        }

        @Override
        public RemoteViews getViewAt(int position) {
            RemoteViews rv = new RemoteViews(getApplicationContext().getPackageName(), R.layout
                    .app_widget_list_item);
            Event event = widgetEvents.get(position);
            rv.setTextViewText(R.id.tvDisplayName, event.getDisplayName());
            rv.setTextViewText(R.id.tvEventDate, event.getParsedDate());

            Bundle extras = new Bundle();
            extras.putLong(AppWidget.EXTRA_ID, event.getId());
            extras.putString(AppWidget.EXTRA_SOURCE, event.getEventSource());
            Intent fillingIntent = new Intent();
            fillingIntent.putExtras(extras);
            rv.setOnClickFillInIntent(R.id.app_widget_list_item, fillingIntent);
            return rv;
        }

        @Override
        public RemoteViews getLoadingView() {
            return null;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        private List<Event> getUpcomingEvents(List<Event> events) {
            List<Event> eventList = new ArrayList<>();
            if (events.size() > MAX_COUNT) {
                //Add next 5 days' events
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, 5);
                String dateLimit = DateTimeUtils.getISODate(cal);
                for (Event event : events) {
                    if (!event.isError()) {
                        if (event.getParsedDate().compareTo(dateLimit) < 0) {
                            eventList.add(event);
                        } else {
                            break;
                        }
                    }
                }
                //Check at least MAX_COUNT events available, if not add first MAX_COUNT events
                if (eventList.size() < MAX_COUNT) {
                    eventList.clear();
                    for (int i = 0; i < MAX_COUNT; i ++) {
                        eventList.add(events.get(i));
                    }
                }
            } else {
                eventList = events;
            }
            return eventList;
        }
    }
}
