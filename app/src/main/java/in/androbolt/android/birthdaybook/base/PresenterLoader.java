package in.androbolt.android.birthdaybook.base;

import android.content.Context;
import android.support.v4.content.Loader;

public class PresenterLoader<P extends Presenter> extends Loader<P> {

    private final PresenterFactory<P> factory;
    private P presenter;

    public PresenterLoader(Context context, PresenterFactory<P> factory) {
        super(context);
        this.factory = factory;
    }

    @Override
    protected void onStartLoading() {
        if (presenter != null) {
            deliverResult(presenter);
            return;
        }
        forceLoad();
    }

    @Override
    protected void onForceLoad() {
        this.presenter = factory.create();
        deliverResult(presenter);
    }

    @Override
    protected void onReset() {
        presenter.onDestroyed();
        presenter = null;
    }
}
