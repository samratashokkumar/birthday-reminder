package in.androbolt.android.birthdaybook.utils;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * Created by ashok on 25/1/17.
 */

public class NoOp {
    public static Object of(Class<?> clazz) {
        return Proxy
                .newProxyInstance(NoOp.class.getClassLoader(),
                        new Class[]{clazz},
                        new InvocationHandler() {
                            @Override
                            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                                return null;
                            }
                        });
    }
}
