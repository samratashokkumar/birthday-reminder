package in.androbolt.android.birthdaybook.mainactivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import in.androbolt.android.birthdaybook.R;
import in.androbolt.android.birthdaybook.settings.SettingsActivity;
import in.androbolt.android.birthdaybook.alarms.AppAlarms;
import in.androbolt.android.birthdaybook.preferences.AppPreferences;


public class MainActivity extends AppCompatActivity implements MainContract.View{

    @SuppressWarnings("unused")
    private static final String TAG = "MainActivity";
    public static final int ADD_FROM_CONTACT = 100;

    private AppPreferences appPreferences;
    private MainContract.ActionListener actionListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        actionListener = new MainPresenter(this);
        appPreferences = new AppPreferences(getApplicationContext());
        setupAlarms();
        Toolbar appToolbar = (Toolbar) findViewById(R.id.appToolbar);
        setSupportActionBar(appToolbar);
    }

    private void setupAlarms() {
        if (appPreferences.isFirstRun()) {
            new AppAlarms(getApplicationContext()).setTodaysEventsNotificationAlarm
                    (appPreferences.getNotificationTime());
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            actionListener.openSettings();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showAdd(View v){

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case ADD_FROM_CONTACT:
                Log.d(TAG, "Finished...");
                if(resultCode==RESULT_OK) {
//                    EventsListFragment eventsListFragment
//                            = (EventsListFragment) getFragmentManager()
//                            .findFragmentById(R.id.frgEventsList);
                    Log.d(TAG, "Updating...");
                }
                break;
        }
    }

    @Override
    public void showSettings() {
        startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
    }
}
