package in.androbolt.android.birthdaybook.adapters;

import android.support.v7.widget.RecyclerView;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.List;

import in.androbolt.android.birthdaybook.events.models.Event;

public abstract class RecyclerViewListAdapter extends RecyclerView.Adapter<RecyclerView
        .ViewHolder> implements Filterable{

    public abstract List<Event> getItems();

    public abstract void setItems(List<Event> events);

    @Override
    public Filter getFilter() {
        return new NameFilter(this);
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }
}
