package in.androbolt.android.birthdaybook.settings;

import in.androbolt.android.birthdaybook.alarms.AppAlarms;
import in.androbolt.android.birthdaybook.preferences.AppPreferences;

/**
 * Settings Presenter
 */
public class SettingsPresenter implements SettingsContract.ActionListener {

    private SettingsContract.View view;
    private AppAlarms appAlarms;
    private AppPreferences appPreferences;

    public SettingsPresenter(SettingsContract.View view, AppAlarms appAlarms, AppPreferences
            appPreferences) {
        this.view = view;
        this.appAlarms = appAlarms;
        this.appPreferences = appPreferences;
    }

    @Override
    public void updateNotificationTime(String notificationTime) {
        appAlarms.setTodaysEventsNotificationAlarm(notificationTime);
    }

    @Override
    public void toggleNotification(boolean isEnabled) {
        if (isEnabled) {
            appAlarms.setTodaysEventsNotificationAlarm(appPreferences.getNotificationTime());
        } else {
            view.disableNotifyInAdvance();
            appAlarms.cancelTodaysEventsNotificationAlarm();
            appAlarms.cancelFutureEventsNotificationAlarm();
        }
    }

    @Override
    public void toggleAdvanceNotification(boolean isAdvanceNotificationEnabled) {
        if (isAdvanceNotificationEnabled) {
            appAlarms.setFutureEventsNotificationAlarm(appPreferences.getNotificationTime());
        } else {
            view.disableNotifyOnEventDate();
            appAlarms.cancelFutureEventsNotificationAlarm();
        }
    }
}
