package in.androbolt.android.birthdaybook.adapters;

import android.widget.Filter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import in.androbolt.android.birthdaybook.events.models.Event;

/**
 * Name Filter
 */
public class NameFilter extends Filter {

    private RecyclerViewListAdapter recyclerViewListAdapter;
    private List<Event> allEvents;
    private List<Event> filteredList;

    public NameFilter(RecyclerViewListAdapter recyclerViewListAdapter) {
        this.recyclerViewListAdapter = recyclerViewListAdapter;
        this.allEvents = Collections.unmodifiableList(this.recyclerViewListAdapter.getItems());
        this.filteredList = new ArrayList<>();
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        filteredList.clear();
        //Matches every word starts with the constraint
        Pattern pattern = Pattern.compile(String.format("\\b%s", Pattern.quote(constraint
                .toString())), Pattern.CASE_INSENSITIVE);
        //TODO: Filter from the filtered list
        if (constraint.toString().trim().isEmpty()) {
            filteredList.addAll(this.allEvents);
        } else {
            for (Event event : this.allEvents) {
                Matcher matcher = pattern.matcher(event.getDisplayName());
                if (matcher.find()) {
                    filteredList.add(event);
                }
            }
        }
        FilterResults filterResults = new FilterResults();
        filterResults.values = filteredList;
        filterResults.count = filteredList.size();
        return filterResults;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        recyclerViewListAdapter.setItems((List<Event>) results.values);
    }
}
