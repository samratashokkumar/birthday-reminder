package in.androbolt.android.birthdaybook.events.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.provider.BaseColumns;

import java.util.List;

import in.androbolt.android.birthdaybook.events.models.AppEvent;
import in.androbolt.android.birthdaybook.events.models.CustomEvent;
import in.androbolt.android.birthdaybook.events.models.Event;
import in.androbolt.android.birthdaybook.events.models.FacebookEvent;

/**
 * Sqlite Helper class for events to be stored within app sqlite database
 */
public class AppDBEventsHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "events.db";
    private static final int SCHEMA_VERSION = 2;

    public AppDBEventsHelper(Context context) {
        //TODO: Make AppDBEventsHelper access from a singleton
        super(context, DATABASE_NAME, null, SCHEMA_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        People people = new People(db);
        people.create();
        Events events = new Events(db);
        events.create();
        EventsView eventsView = new EventsView(db);
        eventsView.create();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion == 1 && newVersion == 2) {
            upgradeToV2(db);
        }
    }

    private void upgradeToV2(SQLiteDatabase db) {
        try {
            db.beginTransaction();
            db.execSQL("ALTER TABLE people RENAME TO people_v1");
            db.execSQL("ALTER TABLE events RENAME TO events_v1");
            People people = new People(db);
            people.create();
            Events events = new Events(db);
            events.create();
            EventsView eventsView = new EventsView(db);
            eventsView.create();
            migrateData(db);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    private void migrateData(SQLiteDatabase db) {
        People people = new People(db);
        Events events = new Events(db);
        Cursor cursor =
                db.rawQuery("SELECT E._id, fb_uid, name, pic_url, people_id, " +
                                " eventdate, eventtype, eventsource FROM people_v1 P " +
                                " INNER JOIN events_v1 E ON P._id = people_id " +
                                "WHERE show =? ORDER BY people_id",
                        new String[]{String.valueOf(1)});
        SQLiteStatement insertPeopleStatement = people.getInsertStatement();
        SQLiteStatement insertEventStatement = events.getInsertStatement();
        long previousPeopleIdV1 = -1;
        long peopleIdV2 = -1;
        while (cursor.moveToNext()) {
            long peopleIdV1 = cursor.getLong(
                    cursor.getColumnIndex("people_id"));
            String name = cursor.getString(
                    cursor.getColumnIndex("name"));
            String source = cursor.getString(
                    cursor.getColumnIndex("eventsource"));
            String nativeId = cursor.getString(
                    cursor.getColumnIndex("fb_uid"));
            String picturePath = cursor.getString(
                    cursor.getColumnIndex("pic_url"));
            String eventDate = cursor.getString(
                    cursor.getColumnIndex("eventdate"));
            int eventType = cursor.getInt(
                    cursor.getColumnIndex("eventtype"));

            switch (source) {
                case "fb":
                    source = FacebookEvent.SOURCE;
                    break;
                case "nc":
                    source = CustomEvent.SOURCE;
                    break;
            }

            if (peopleIdV1 != previousPeopleIdV1) {
                insertPeopleStatement.bindString(1, name);
                insertPeopleStatement.bindString(2, source);
                if (nativeId != null) {
                    insertPeopleStatement.bindString(3, nativeId);
                } else {
                    insertPeopleStatement.bindNull(3);
                }
                if (picturePath != null) {
                    insertPeopleStatement.bindString(4, picturePath);
                } else {
                    insertPeopleStatement.bindNull(4);
                }
                peopleIdV2 = insertPeopleStatement.executeInsert();
            }
            insertEventStatement.bindAllArgsAsStrings(new String[]{
                    String.valueOf(peopleIdV2),
                    eventDate,
                    String.valueOf(eventType)
            });
            insertEventStatement.executeInsert();
            previousPeopleIdV1 = peopleIdV1;
        }
        cursor.close();
        db.execSQL("DROP TABLE people_v1");
        db.execSQL("DROP TABLE events_v1");
    }

    public long addEvent(AppEvent appEvent) {
        if (!appEvent.isError()) {
            SQLiteDatabase db = getWritableDatabase();
            People people = new People(db);
            Events events = new Events(db);
            long peopleId;
            if (appEvent.getId() == -1) {
                peopleId = people.
                        add(appEvent.getDisplayName(),
                                appEvent.getEventSource(),
                                appEvent.getNativeId(),
                                appEvent.getPicturePath());
            } else {
                peopleId = appEvent.getPeopleId();
            }
            return events
                    .add(peopleId,
                            appEvent.getEventDate(),
                            appEvent.getEventType());
        } else {
            return -1;
        }
    }

    public void addEvents(List<AppEvent> appEventList) {
        SQLiteDatabase db = getWritableDatabase();
        People people = new People(db);
        Events events = new Events(db);
        SQLiteStatement insertPeopleStatement = people.getInsertStatement();
        SQLiteStatement insertEventStatement = events.getInsertStatement();
        db.beginTransaction();
        try {
            for (AppEvent appEvent : appEventList) {
                insertPeopleStatement.bindAllArgsAsStrings(new String[]{
                        appEvent.getDisplayName(),
                        appEvent.getEventSource(),
                        appEvent.getNativeId(),
                        appEvent.getPicturePath()
                });
                long peopleId = insertPeopleStatement.executeInsert();
                insertEventStatement.bindAllArgsAsStrings(new String[]{
                        String.valueOf(peopleId),
                        appEvent.getEventDate(),
                        String.valueOf(appEvent.getEventType())
                });
                insertEventStatement.executeInsert();
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    public List<Event> getEvents(long peopleId) {
        EventsView eventsView = new EventsView(getReadableDatabase());
        return eventsView.getEvents(peopleId);
    }

    public List<Event> getEvents(String eventSource) {
        EventsView eventsView = new EventsView(getReadableDatabase());
        return eventsView.getEvents(eventSource);
    }

    public List<Event> getEvents() {
        EventsView eventsView = new EventsView(getReadableDatabase());
        return eventsView.getEvents();
    }

    public int deleteEvent(long eventId) {
        Events events = new Events((getWritableDatabase()));
        return events.delete(eventId);
    }

    public List<String> getNames(int eventType, String eventDate) {
        EventsView eventsView = new EventsView(getReadableDatabase());
        return eventsView.getNames(eventType, eventDate);
    }

    public void updateEvent(AppEvent event) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cvPeople = new ContentValues();
        cvPeople.put(People.Columns.NAME, event.getDisplayName());
        db.update(People.TABLE_PEOPLE, cvPeople, People.Columns._ID + " =? ", new String[] {
                String.valueOf(event.getPeopleId())});
        ContentValues cvEvent = new ContentValues();
        cvEvent.put(Events.Columns.DATE, event.getEventDate());
        db.update(Events.TABLE_EVENTS, cvEvent, Events.Columns._ID + " =?", new String[] { String
                .valueOf(event.getId())});
    }
}
